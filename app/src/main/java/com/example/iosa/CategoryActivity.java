package com.example.iosa;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.example.iosa.Adapter.CategoryAdapter;
import com.example.iosa.Retrofitapi.APIInterface;
import com.example.iosa.Retrofitapi.ApiClient;
import com.example.iosa.VollyApi.NoConnectivityException;
import com.example.iosa.bean.CategoryApiResponse;
import com.example.iosa.bean.CategoryItemList;
import com.example.iosa.utils.CommanUtilsApp;
import com.example.iosa.utils.UtilsStatusbar;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.iosa.utils.ApiConstants.BASE_URL_OLd;

public class CategoryActivity extends BaseActivity {

    @BindView(R.id.rvCategoryList)
    RecyclerView rvCategoryList;
    CategoryAdapter aboutAdapter;
    CommanUtilsApp commanUtils;
    List<CategoryItemList> categoryItemLists;
    String instaImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new UtilsStatusbar(CategoryActivity.this).setStatuBar();
        setContentView(R.layout.activity_category);
        ButterKnife.bind(this);

        commanUtils = new CommanUtilsApp(this);
        commanUtils.createProgressDialog(getResources().getString(R.string.loading));

        instaImage = getIntent().getExtras().getString("image");

        getAboutList();

    }

    private void getAboutList() {
        commanUtils.showProgressDialog();
        Call<CategoryApiResponse> call = ApiClient.getClient(BASE_URL_OLd, getApplicationContext()).create(APIInterface.class)
                .getcategoryList("qa4sc6f2028dd6b29f91e0e656790c8c", "1");
        call.enqueue(new Callback<CategoryApiResponse>() {
            @Override
            public void onResponse(Call<CategoryApiResponse> call, Response<CategoryApiResponse> response) {
                if (response.body().getSuccess().equalsIgnoreCase("1")) {
                    categoryItemLists = response.body().getResponse();

                    aboutAdapter = new CategoryAdapter(CategoryActivity.this, categoryItemLists, instaImage, new CategoryAdapter.PackageClick() {
                        @Override
                        public void packageClick(int i) {
                            startActivityForResult(new Intent(CategoryActivity.this, HomeActivity.class)
                                    .putExtra("image",instaImage),30);
                        }
                    });
                    rvCategoryList.setAdapter(aboutAdapter);
                    rvCategoryList.setHasFixedSize(true);

                }
                commanUtils.hideProgressDialog();
            }

            @Override
            public void onFailure(Call<CategoryApiResponse> call, Throwable t) {
                t.printStackTrace();
                if (t instanceof NoConnectivityException) {
                    commanUtils.showToast(CategoryActivity.this, getResources().getString(R.string.check_network));
                }
                commanUtils.hideProgressDialog();
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        setResult(30,intent);
        finish();
    }
}