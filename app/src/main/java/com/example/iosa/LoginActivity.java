package com.example.iosa;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.widget.EditText;
import android.widget.TextView;

import com.appizona.yehiahd.fastsave.FastSave;
import com.example.iosa.Retrofitapi.APIInterface;
import com.example.iosa.Retrofitapi.ApiClient;
import com.example.iosa.VollyApi.NoConnectivityException;
import com.example.iosa.bean.LoginApiResponse;
import com.example.iosa.utils.CommanUtilsApp;
import com.example.iosa.utils.UtilsStatusbar;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.iosa.utils.ApiConstants.APIKEY;
import static com.example.iosa.utils.ApiConstants.BASE_URL;
import static com.example.iosa.utils.ApiConstants.LOGIN_ID;
import static com.example.iosa.utils.ApiConstants.NAME;
import static com.example.iosa.utils.ApiConstants.PASSWORD;
import static com.example.iosa.utils.ApiConstants.PRICE;
import static com.example.iosa.utils.ApiConstants.PROMO;
import static com.example.iosa.utils.ApiConstants.SUBSCRIPTION;

public class LoginActivity extends BaseActivity {
    @BindView(R.id.txtLogin)
    TextView txtLogin;
    @BindView(R.id.edtLogin)
    EditText edtLogin;
    @BindView(R.id.edtPassword)
    EditText edtPassword;
    CommanUtilsApp commanUtilsApp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new UtilsStatusbar(LoginActivity.this).setStatuBar();
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        commanUtilsApp = new CommanUtilsApp(this);
        commanUtilsApp.createProgressDialog(getResources().getString(R.string.loading));

        edtLogin.setText("maulik@sociallyapp.net");
        edtPassword.setText("maulik");

        if (FastSave.getInstance().getString(APIKEY, "").length() > 0) {
            startActivity(new Intent(LoginActivity.this, ProfileActivity.class).putExtra("isApiCall",false));
            finish();
        }
    }

    @OnClick(R.id.txtLogin)
    public void onViewClicked() {
        if (!(edtLogin.getText().length() > 0)) {
            CommanUtilsApp.showToast(LoginActivity.this, "Please enter login");
        } else if (!(edtPassword.getText().length() > 0)) {
            CommanUtilsApp.showToast(LoginActivity.this, "Please enter password");
        } else {
            commanUtilsApp.showProgressDialog();
            Call<JsonObject> call = ApiClient.getClient(BASE_URL, getApplicationContext()).create(APIInterface.class)
                    .getLogin(edtLogin.getText().toString(),
                            edtPassword.getText().toString(),
                            Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID) + "",
                            "A");
            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> objectResponse) {
                    if (objectResponse.body().get("success").toString().contains("1")) {
                        Gson gson = new Gson();
                        LoginApiResponse response = gson.fromJson(objectResponse.body().toString(), LoginApiResponse.class);

                        if (response.getSuccess().equalsIgnoreCase("1")) {
                            FastSave.getInstance().saveString(APIKEY, response.getResponse().getApikey());
                            FastSave.getInstance().saveString(NAME, response.getResponse().getName());
                          FastSave.getInstance().saveString(SUBSCRIPTION, response.getResponse().getTotalSubScription());
                            FastSave.getInstance().saveString(PROMO, response.getResponse().getTotalPromo());
                            FastSave.getInstance().saveString(PRICE, response.getResponse().getTotalPrice());
                            FastSave.getInstance().saveString(LOGIN_ID, edtLogin.getText().toString());
                            FastSave.getInstance().saveString(PASSWORD, edtPassword.getText().toString());

                            startActivity(new Intent(LoginActivity.this, ProfileActivity.class)
                                    .putExtra("isApiCall",true));
                            finish();
                        }
                    } else {
                        CommanUtilsApp.showToast(LoginActivity.this, objectResponse.body().get("error").toString());
                    }
                    commanUtilsApp.hideProgressDialog();
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    t.printStackTrace();
                    if (t instanceof NoConnectivityException) {
                        CommanUtilsApp.showToast(LoginActivity.this, getResources().getString(R.string.check_network));
                    }
                    commanUtilsApp.hideProgressDialog();
                }
            });
        }
    }
}
