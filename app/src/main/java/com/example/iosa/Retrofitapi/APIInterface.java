package com.example.iosa.Retrofitapi;

import com.example.iosa.bean.DateWiseResponse;
import com.example.iosa.bean.GetPackageApiResponse;
import com.example.iosa.bean.CategoryApiResponse;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface APIInterface {
    @FormUrlEncoded
    @POST("login/check")
    Call<JsonObject> loginCheck(
            @Field("login_user") String login_user,
            @Field("login_password") String login_password,
            @Field("device_id") String device_id,
            @Field("device_type") String device_type
    );

    @FormUrlEncoded
    @POST("category/getlist")
    Call<CategoryApiResponse> getcategoryList(
            @Field("apikey") String api_key,
            @Field("parent_category") String parent_category
    );

    @FormUrlEncoded
    @POST("Employeeapi/login/check")
    Call<JsonObject> getLogin(
            @Field("login_user") String login_user,
            @Field("login_password") String login_password,
            @Field("device_id") String device_id,
            @Field("device_type") String device_type
    );

    @FormUrlEncoded
    @POST("Employeeapi/customer/add_iosa")
    Call<JsonObject> addCustomer(
            @Field("apikey") String api_key,
            @Field("product_link") String product_link,
            @Field("profile_link") String profile_link,
            @Field("phone") String phone,
            @Field("package_id") String package_id

    );
    @FormUrlEncoded
    @POST("Employeeapi/report/this_week_customer_list")
    Call<JsonObject> reportWeek(@Field("apikey") String api_key);

    @FormUrlEncoded
    @POST("Employeeapi/report/this_month_customer_list")
    Call<JsonObject> reportMonth(@Field("apikey") String api_key);

    @FormUrlEncoded
    @POST("Employeeapi/report/three_month_customer_list")
    Call<JsonObject> reportThreeMonth(@Field("apikey") String api_key);

    @FormUrlEncoded
    @POST("Employeeapi/report/this_year_customer_list")
    Call<JsonObject> reportThisYear(@Field("apikey") String api_key);

    @FormUrlEncoded
    @POST("Employeeapi/report/created")
    Call<JsonObject> createReport(@Field("apikey") String api_key);

    @FormUrlEncoded
    @POST("Employeeapi/report/live")
    Call<JsonObject> liveReport(@Field("apikey") String api_key);

    @FormUrlEncoded
    @POST("Employeeapi/report/expire")
    Call<JsonObject> expireReport(@Field("apikey") String api_key);

    @FormUrlEncoded
    @POST("Employeeapi/report/subscription")
    Call<JsonObject> subscriptionReport(@Field("apikey") String api_key);

    @FormUrlEncoded
    @POST("Employeeapi/report/expire_subscription")
    Call<JsonObject> expireSubscriptionReport(@Field("apikey") String api_key);

    @FormUrlEncoded
    @POST("getemployeepackage")
    Call<GetPackageApiResponse> getPackageList(
            @Field("apikey") String api_key
    );

    @FormUrlEncoded
    @POST("Employeeapi/report/datewise_qr")
    Call<DateWiseResponse> getDateWiseList(
            @Field("apikey") String api_key
    );

    @FormUrlEncoded
    @POST("Employeeapi/report/customer_list")
    Call<JsonObject> getCustomerList(@Field("apikey") String api_key);

    @FormUrlEncoded
    @POST("Employeeapi/report/customer_history")
    Call<JsonObject> getPromoHistory(@Field("apikey") String api_key, @Field("customer_id") String customer_id);

}