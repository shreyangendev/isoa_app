package com.example.iosa;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.appizona.yehiahd.fastsave.FastSave;
import com.example.iosa.Adapter.SearchAdapter;
import com.example.iosa.Retrofitapi.APIInterface;
import com.example.iosa.Retrofitapi.ApiClient;
import com.example.iosa.VollyApi.NoConnectivityException;
import com.example.iosa.bean.ReportApiResponce;
import com.example.iosa.bean.ReportBean;
import com.example.iosa.utils.CommanUtilsApp;
import com.example.iosa.utils.Utils;
import com.example.iosa.utils.UtilsStatusbar;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.iosa.utils.ApiConstants.APIKEY;
import static com.example.iosa.utils.ApiConstants.BASE_URL;


public class SearchActivity extends BaseActivity {

    CommanUtilsApp commanUtilsApp;
    @BindView(R.id.imgBack)
    ImageView imgBack;
    @BindView(R.id.rVSearchlist)
    RecyclerView rVSearchlist;
    SearchAdapter searchAdapter;
    @BindView(R.id.edtSearchList)
    EditText edtSearchList;
    List<ReportBean> mainList = new ArrayList<>(), filterList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new UtilsStatusbar(SearchActivity.this).setStatuBar();
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);

        commanUtilsApp = new CommanUtilsApp(this);
        commanUtilsApp.createProgressDialog(getResources().getString(R.string.loading));

        getCustomerList();

        edtSearchList.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i0, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i0, int i1, int i2) {
                filterList.clear();
                for (int i = 0; i < mainList.size(); i++) {
                    if (mainList.get(i).getCustomerName().toLowerCase().contains(charSequence.toString().toLowerCase())
                            || mainList.get(i).getPhone().toLowerCase().contains(charSequence.toString().toLowerCase())
                            || mainList.get(i).getShopName().toLowerCase().contains(charSequence.toString().toLowerCase())) {
                        filterList.add(mainList.get(i));
                    }
                }
                setAdapter();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void setAdapter() {
        searchAdapter = new SearchAdapter(SearchActivity.this, filterList);
        rVSearchlist.setAdapter(searchAdapter);
        rVSearchlist.setHasFixedSize(false);
    }

    private void getCustomerList() {
        commanUtilsApp.showProgressDialog();
        Call<JsonObject> call = ApiClient.getClient(BASE_URL, getApplicationContext()).create(APIInterface.class)
                .getCustomerList(FastSave.getInstance().getString(APIKEY, ""));
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> objectResponse) {
                if (objectResponse.body().get("success").toString().contains("1")) {
                    Gson gson = new Gson();
                    ReportApiResponce response = gson.fromJson(objectResponse.body().toString(), ReportApiResponce.class);
                    if (response.getSuccess().equalsIgnoreCase("1")) {

                        mainList = response.getResponse();
                        filterList.addAll(mainList);
                        setAdapter();


                    }
                } else {
                    CommanUtilsApp.showToast(SearchActivity.this, objectResponse.body().get("error").toString());
                }
                commanUtilsApp.hideProgressDialog();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                t.printStackTrace();
                if (t instanceof NoConnectivityException) {
                    CommanUtilsApp.showToast(SearchActivity.this, getResources().getString(R.string.check_network));
                } else {
                    CommanUtilsApp.showToast(SearchActivity.this, t.getMessage());
                }
                commanUtilsApp.hideProgressDialog();
            }
        });
    }


    @OnClick(R.id.imgBack)
    public void onViewClicked() {
        finish();
    }
}