package com.example.iosa;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.coolerfall.download.DownloadCallback;
import com.coolerfall.download.DownloadManager;
import com.coolerfall.download.DownloadRequest;
import com.coolerfall.download.Logger;
import com.coolerfall.download.OkHttpDownloader;
import com.coolerfall.download.Priority;
import com.example.iosa.Adapter.PrimarySecondaryColorListAdapter;
import com.example.iosa.Coustomer.BorderedTextView;
import com.example.iosa.Coustomer.PrimarySecondaryTonePos;
import com.example.iosa.VollyApi.ApiCall;
import com.example.iosa.VollyApi.VolleyTaskCompleteListener;
import com.example.iosa.bean.FrameBean;
import com.example.iosa.customTextSticker.Font;
import com.example.iosa.utils.CommanUtilsApp;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import okhttp3.OkHttpClient;

import static android.graphics.Color.BLACK;
import static android.graphics.Color.WHITE;
import static android.graphics.Color.parseColor;
import static com.example.iosa.utils.ApiConstants.DATA;
import static com.example.iosa.utils.ApiConstants.ERROR;
import static com.example.iosa.utils.ApiConstants.GET_RANDOM_FRAME_TEST;
import static com.example.iosa.utils.ApiConstants.KEY_SUB_CAT_ID;
import static com.example.iosa.utils.ApiConstants.RESPONSE;
import static com.example.iosa.utils.ApiConstants.SUCCESS;
import static com.example.iosa.utils.ApiConstants.SuggestionRequest;
import static com.example.iosa.utils.ApiConstants.UNIQUE_KEY;
import static com.example.iosa.utils.ApiConstants.UNIQUE_KEY_VALUE;
import static com.example.iosa.utils.CommanUtilsApp.modifyOrientation;
import static com.example.iosa.utils.KeyParameterConst.KEY_CATEGORY_ID;
import static com.example.iosa.utils.KeyParameterConst.KEY_COLORS_ARRAYS;
import static com.example.iosa.utils.KeyParameterConst.KEY_COUNT;
import static com.example.iosa.utils.KeyParameterConst.KEY_FRAME_ID;
import static com.example.iosa.utils.KeyParameterConst.KEY_IMAGE;
import static com.example.iosa.utils.KeyParameterConst.KEY_LIST;
import static com.example.iosa.utils.KeyParameterConst.KEY_PRIMARY_COLOR;
import static com.example.iosa.utils.KeyParameterConst.KEY_SECONDARY_COLOR;
import static com.example.iosa.utils.KeyParameterConst.KEY_TYPE;
import static java.lang.Float.parseFloat;

public class SuggestionActivity extends BaseActivity implements VolleyTaskCompleteListener, PrimarySecondaryTonePos {
    String categoryid, image;
    @BindView(R.id.llList)
    LinearLayout llList;
    @BindView(R.id.scroll)
    ScrollView scroll;

    ApiCall apiCall;
    ArrayList<Integer> colorsArrays;
    ArrayList<FrameLayout> listFrames;
    ArrayList<String> frameIdList, maskImages;
    ArrayList<ArrayList<FrameBean>> allDataListBean = new ArrayList<>();
    public static int mainFrameHeight, mainFrameWidth, screenWidth, pos = 0, screenHeight;
    public static ArrayList<FrameBean> frameImagesBeans, frameTextBeans;
    PrimarySecondaryTonePos primarySecondaryTonePos;
    ArrayList<ImageView> hueImages = new ArrayList<>(), maskImageViewList = new ArrayList<>();
    ArrayList<View> btImages = new ArrayList<>(), wtImages = new ArrayList<>(), ptImages = new ArrayList<>(), stImages = new ArrayList<>();
    ArrayList<ArrayList<ImageView>> hueImagesList;
    ArrayList<ArrayList<View>> blackToneList, whiteToneList, secondaryToneList, primaryToneList;
    ArrayList<Boolean> whiteTextList = new ArrayList<>(), blackTextList = new ArrayList<>();
    int primaryColor = -1, secondaryColor = -1, primarySecondaryColorPos = -1;
    CommanUtilsApp commanUtilsApp;
    ArrayList<FrameBean> responseDataListBean;
    JSONArray array, jsonArray;
    JSONObject object, jsonObject;
    FrameBean responseDataBean;
    String[] colorsStrings;
    String color, success, border_color, frameId;
    int count, recordperPage = 10;
    HashMap<String, String> hashMap;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suggestion);
        ButterKnife.bind(this);

        colorsArrays = (ArrayList<Integer>) getIntent().getSerializableExtra(KEY_COLORS_ARRAYS);
        categoryid = getIntent().getStringExtra(KEY_CATEGORY_ID);
        count = getIntent().getIntExtra(KEY_COUNT, 0);
        image = getIntent().getStringExtra(KEY_IMAGE);
        frameId = getIntent().getStringExtra(KEY_TYPE);
        colorsArrays.remove(null);
        primarySecondaryTonePos = this::primarySecondaryTonePos;
        apiCall = new ApiCall(this, this);

        commanUtilsApp = new CommanUtilsApp(SuggestionActivity.this);
        commanUtilsApp.createProgressDialog(getResources().getString(R.string.loading));
        blackToneList = new ArrayList<>();
        whiteToneList = new ArrayList<>();
        secondaryToneList = new ArrayList<>();
        primaryToneList = new ArrayList<>();
        hueImagesList = new ArrayList<>();
        frameIdList = new ArrayList<>();
        maskImages = new ArrayList<>();
        listFrames = new ArrayList<>();
        frameImagesBeans = new ArrayList<>();
        frameTextBeans = new ArrayList<>();

        callApi();
    }

    private void callApi() {
        hashMap = new HashMap<String, String>();
        hashMap.put(UNIQUE_KEY, UNIQUE_KEY_VALUE);
        hashMap.put("categoryid_1", frameId);
        hashMap.put("categoryid_2", categoryid);
        hashMap.put("categoryid_3", getIntent().getStringExtra(KEY_SUB_CAT_ID));
        hashMap.put("recordsperpage", recordperPage + "");
        hashMap.put("currentpagenumber", count + "");

        if (CommanUtilsApp.isNetworkAvailable(SuggestionActivity.this)) {
            commanUtilsApp.showProgressDialog();
            apiCall.GetDataPagination(Request.Method.POST, GET_RANDOM_FRAME_TEST, hashMap, 0);
        }
    }

    @OnClick(R.id.addNewData)
    public void onClicks() {
        for (int i = 0; i < listFrames.size(); i++) {
            listFrames.get(i).removeAllViews();
        }
        listFrames.clear();
        llList.removeAllViewsInLayout();
        llList.requestLayout();
        count = count + 1;

        blackToneList.clear();
        whiteToneList.clear();
        secondaryToneList.clear();
        primaryToneList.clear();
        hueImagesList.clear();
        frameIdList.clear();
        maskImages.clear();
        listFrames.clear();
        frameImagesBeans.clear();
        frameTextBeans.clear();

        callApi();

        /* startActivityForResult(new Intent(this, SuggestionActivity.class)
                .putExtra(KEY_COLORS_ARRAYS, colorsArrays)
                .putExtra(KEY_IMAGE, image)
                .putExtra(KEY_TYPE, frameId)
                .putExtra(KEY_SUB_CAT_ID, getIntent().getStringExtra(KEY_SUB_CAT_ID))
                .putExtra(KEY_COUNT, count + 1)
                .putExtra(KEY_CATEGORY_ID, categoryid), SuggestionRequest);
        finish(); */
        /* pageNumber++;
        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put(UNIQUE_KEY, UNIQUE_KEY_VALUE);
        hashMap.put("categoryid_1", FastSave.getInstance().getString(CAT_ID, POSTER_ID));
        hashMap.put("categoryid_2", categoryid);
        hashMap.put("categoryid_3", getIntent().getStringExtra("subCatId"));
        hashMap.put("recordsperpage", "20");
        hashMap.put("currentpagenumber", pageNumber + "");
        if (CommanUtilsApp.isNetworkAvailable(SuggestionActivity.this))
            apiCall.GetData(Request.Method.POST, GET_RANDOM_FRAME_TEST, hashMap, 0); */
    }

    @OnClick(R.id.llBack)
    public void onClick() {
        onBackPressed();
    }


    private void parseDatas(String responseString) {
        frameImagesBeans.clear();
        frameTextBeans.clear();
        allDataListBean.clear();
        try {
            jsonObject = new JSONObject(responseString);
            success = jsonObject.getString(SUCCESS);
            if (success.equals("1")) {
                jsonArray = jsonObject.getJSONArray(RESPONSE);
                for (int j = 0; j < jsonArray.length(); j++) {
                    frameIdList.add(jsonArray.getJSONObject(j).getString("frameid"));
                    responseDataListBean = new ArrayList<>();
                    array = jsonArray.getJSONObject(j).getJSONArray(DATA);
                    if (array.length() > 0) {
                        for (int i = 0; i < array.length(); i++) {
                            object = array.getJSONObject(i);
                            responseDataBean = new FrameBean();
                            responseDataBean.type = object.getString("type");
                            responseDataBean.src = object.getString("src");
                            responseDataBean.name = object.getString("name");
                            responseDataBean.width = object.getString("width");
                            responseDataBean.height = object.getString("height");
                            responseDataBean.x = object.getString("x");
                            responseDataBean.y = object.getString("y");
                            // testBean.font = object.getString("font");
                            responseDataBean.is_lock = object.getString("is_lock");
                            responseDataBean.is_color = object.getString("is_color");
                            responseDataBean.is_hue = object.getString("is_hue");
                            responseDataBean.is_mask = object.getString("is_mask");
                            /*if (object.getString("is_mask").equals("1")) {
                                maskImages.add(object.getString("src"));
                            }*/
                            responseDataBean.font_name = object.getString("font_name");
                            responseDataBean.is_object = object.getString("is_object");
                            responseDataBean.is_shape = object.getString("is_shape");
                            responseDataBean.font_spacing = object.getString("font_spacing");
                            responseDataBean.spacing = object.getString("spacing");
                            responseDataBean.justification = object.getString("justification");
                            responseDataBean.is_pt = object.getString("is_pt");
                            responseDataBean.is_st = object.getString("is_st");
                            responseDataBean.is_bt = object.getString("is_bt");
                            responseDataBean.is_wt = object.getString("is_wt");
                            responseDataBean.lineheight = object.getString("lineheight");
                            color = object.getString("color");
                            color = color.replace("0x", "");
                            if (color.contains("#")) {
                                colorsStrings = object.getString("color").split("#");
                                color = "#" + colorsStrings[1];
                            } else {
                                color = "#" + color;
                            }
                            responseDataBean.color = color;
                            responseDataBean.size = object.getString("size");
                            responseDataBean.text = object.getString("text").replace("\r", "");
                            responseDataBean.rotation = object.getString("rotation");
                            responseDataBean.position = object.getString("position");
                            responseDataBean.border_size = object.getString("border_size");
                            border_color = object.getString("border_color");
                            if (border_color.length() > 0 && !border_color.equalsIgnoreCase("0"))
                                responseDataBean.border_color = border_color.contains("#") ? "" : "#"
                                        + border_color.replace("0x", "");
                            else responseDataBean.border_color = "";
                            responseDataListBean.add(responseDataBean);
                        }
                        allDataListBean.add(responseDataListBean);
                    }
                }
                if (allDataListBean.size() < recordperPage) {
                    count = 0;
                }
                if (allDataListBean.size() > 0) {
                    setFrame(0);
                } else if (!isFinishing())
                    commanUtilsApp.hideProgressDialog();
            } else CommanUtilsApp.showToast(SuggestionActivity.this, jsonObject.getString(ERROR));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        /*if (!isFinishing())
            commanUtilsApp.hideProgressDialog();*/
    }

    //=================  Setup Frame  ===================//
    private void setFrame(int i) {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.random_frame_list_item, null);
        FrameLayout mainFrame = rowView.findViewById(R.id.rlMain);
        CardView cardEditor = rowView.findViewById(R.id.cardEditor);
        RecyclerView rvPrimaryColor = rowView.findViewById(R.id.rvPrimaryColor);
        RecyclerView rvSecondaryColor = rowView.findViewById(R.id.rvSecondaryColor);
        CardView cardWt = rowView.findViewById(R.id.cardWt);
        CardView cardBt = rowView.findViewById(R.id.cardBt);
        SeekBar seekBar = rowView.findViewById(R.id.seekBar);
        RelativeLayout rlSeekBar = rowView.findViewById(R.id.rlSeekBar);

        cardWt.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                cardWt.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                cardWt.getLayoutParams().height = cardWt.getWidth();
                cardWt.requestLayout();
            }
        });
        cardBt.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                cardBt.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                cardBt.getLayoutParams().height = cardBt.getWidth();
                cardBt.requestLayout();
            }
        });
        cardWt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int j = 0; j < whiteToneList.get(i).size(); j++) {
                    if (whiteToneList.get(i).get(j) instanceof ImageView)
                        ((ImageView) whiteToneList.get(i).get(j)).setColorFilter(whiteTextList.get(i) ? BLACK : WHITE);
                    else {
                        if (whiteToneList.get(i).get(j) instanceof BorderedTextView)
                            ((BorderedTextView) whiteToneList.get(i).get(j)).setBorderedColor(whiteTextList.get(i) ? BLACK : WHITE);
                        else
                            ((TextView) whiteToneList.get(i).get(j)).setTextColor(whiteTextList.get(i) ? BLACK : WHITE);
                    }
                }
                /* for (int j = 0; j < whiteToneList.get(i).size(); j++) {
                    // whiteToneList.get(i).get(j).setColorFilter(whiteTextList.get(i) ? WHITE : BLACK);
                    if (whiteToneList.get(i).get(j) instanceof ImageView)
                        ((ImageView) whiteToneList.get(i).get(j)).setColorFilter(whiteTextList.get(i) ? BLACK : WHITE);
                    else
                        ((TextView) whiteToneList.get(i).get(j)).setTextColor(whiteTextList.get(i) ? getResources().getColor(R.color.black)
                                : getResources().getColor(R.color.white));
                } */
                whiteTextList.set(i, !whiteTextList.get(i));
                cardWt.setCardBackgroundColor(whiteTextList.get(i) ? WHITE : BLACK);
            }
        });
        cardBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int j = 0; j < blackToneList.get(i).size(); j++) {
                    if (blackToneList.get(i).get(j) instanceof ImageView)
                        ((ImageView) blackToneList.get(i).get(j)).setColorFilter(blackTextList.get(i) ? WHITE : BLACK);
                    else if (blackToneList.get(i).get(j) instanceof BorderedTextView)
                        ((BorderedTextView) blackToneList.get(i).get(j)).setBorderedColor(blackTextList.get(i) ? WHITE : BLACK);
                    else
                        ((TextView) blackToneList.get(i).get(j)).setTextColor(blackTextList.get(i) ? WHITE : BLACK);
                }
                /* for (int j = 0; j < blackToneList.get(i).size(); j++) {
                    if (blackToneList.get(i).get(j) instanceof ImageView)
                        ((ImageView) blackToneList.get(i).get(j)).setColorFilter(blackTextList.get(i) ? BLACK : WHITE);
                    else
                        ((TextView) blackToneList.get(i).get(j)).setTextColor(blackTextList.get(i) ? getResources().getColor(R.color.black)
                                : getResources().getColor(R.color.white));
                } */
                blackTextList.set(i, !blackTextList.get(i));
                cardBt.setCardBackgroundColor(blackTextList.get(i) ? BLACK : WHITE);
            }
        });
        if (colorsArrays != null && colorsArrays.size() > 0) {
            rvSecondaryColor.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    rvSecondaryColor.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    rvSecondaryColor.getLayoutParams().height = rvSecondaryColor.getWidth() + 10;
                    rvSecondaryColor.requestLayout();
                    rvSecondaryColor.setAdapter(
                            new PrimarySecondaryColorListAdapter(SuggestionActivity.this, colorsArrays, i,
                                    primarySecondaryTonePos, false, rvSecondaryColor.getWidth()));
                }
            });
            rvPrimaryColor.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    rvPrimaryColor.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    rvPrimaryColor.getLayoutParams().height = rvPrimaryColor.getWidth() + 10;
                    rvPrimaryColor.requestLayout();
                    rvPrimaryColor.setAdapter(
                            new PrimarySecondaryColorListAdapter(SuggestionActivity.this, colorsArrays, i,
                                    primarySecondaryTonePos, true, rvPrimaryColor.getWidth()));
                }
            });
        }
        cardEditor.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                if (cardEditor.getViewTreeObserver().isAlive())
                    cardEditor.getViewTreeObserver().removeOnPreDrawListener(this);
                screenWidth = cardEditor.getWidth();

                hueImages = new ArrayList<>();
                btImages = new ArrayList<>();
                wtImages = new ArrayList<>();
                ptImages = new ArrayList<>();
                stImages = new ArrayList<>();

                for (int j = 0; j < allDataListBean.get(i).size(); j++) {
                    setFrameData(mainFrame, j, i);
                }

                blackToneList.add(btImages);
                whiteToneList.add(wtImages);
                secondaryToneList.add(stImages);
                primaryToneList.add(ptImages);
                hueImagesList.add(hueImages);

                if (hueImagesList != null && hueImagesList.size() > i && hueImagesList.get(i).size() > 0)
                    rlSeekBar.setVisibility(View.VISIBLE);
                else rlSeekBar.setVisibility(View.GONE);
                return true;
            }
        });
        cardEditor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (maskImages.size() > i && maskImages.get(i).length() > 0 && !new File(CommanUtilsApp.getMaskImagePath(maskImages.get(i))).exists()) {
                    downloadMaskImage(i, CommanUtilsApp.getMaskImagePath(maskImages.get(i)));
                } else {
                    Intent intent = getIntent();
                    intent.putExtra(KEY_LIST, allDataListBean.get(i));
                    intent.putExtra(KEY_FRAME_ID, frameIdList.get(i));
                    intent.putExtra(KEY_PRIMARY_COLOR, primarySecondaryColorPos == i ? primaryColor : -1);
                    intent.putExtra(KEY_SECONDARY_COLOR, primarySecondaryColorPos == i ? secondaryColor : -1);
                    setResult(SuggestionRequest, intent);
                    finish();
                }
            }
        });
        seekBar.setMax(180);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                for (int k = 0; k < hueImagesList.get(i).size(); k++) {
                    hueImagesList.get(i).get(k).setColorFilter(new ColorFilterGenerator().adjustHue(progress - 180));
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
        whiteTextList.add(true);
        blackTextList.add(true);
        llList.addView(rowView);
        llList.requestLayout();
        listFrames.add(mainFrame);

        if (allDataListBean.size() > i + 1 && allDataListBean.get(i) != null) // && allDataListBean.get(i).size() > (i + 1))
            setFrame(i + 1);
        else if (!isFinishing())
            commanUtilsApp.hideProgressDialog();
    }

    private void setFrameData(FrameLayout mainFrame, int i, int productCount) {
        if (i == 0) {
            mainFrameWidth = (int) Float.parseFloat(allDataListBean.get(productCount).get(i).width);
            mainFrameHeight = (int) Float.parseFloat(allDataListBean.get(productCount).get(i).height);
            screenHeight = (mainFrameHeight * screenWidth) / mainFrameWidth;
            mainFrame.getLayoutParams().width = screenWidth;
            mainFrame.getLayoutParams().height = (mainFrameHeight * screenWidth) / mainFrameWidth;
            mainFrame.requestLayout();
        }
        if (allDataListBean.get(productCount).get(i).type.equalsIgnoreCase("image")) {
            frameImagesBeans.add(allDataListBean.get(productCount).get(i));
            int pos = frameImagesBeans.size() - 1;
            addImage(frameImagesBeans.get(pos), mainFrame, productCount);
        } else if (allDataListBean.get(productCount).get(i).type.equalsIgnoreCase("text")) {
            frameTextBeans.add(allDataListBean.get(productCount).get(i));
            int sizes = frameTextBeans.size() - 1;
            int size = (int) (Float.parseFloat(frameTextBeans.get(sizes).size) * screenWidth)
                    / mainFrameWidth;
            Font font = getFont(allDataListBean.get(productCount).get(i));
            font.setSize(size / getResources().getDisplayMetrics().density);
            Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/" +
                    CommanUtilsApp.getFontFileName(SuggestionActivity.this, frameTextBeans.get(sizes).font_name));
            font.setTypeface(tf);
            // testStickers.add(allDataListBean.get(productCount).get(i));
            addApiNewText(allDataListBean.get(productCount).get(i), font, mainFrame);
            pos++;
        }
        /* if (!(allDataListBean.size() > productCount + 1)) {
            if (maskImages.size() > 0)
                startImageMasking(0);
            else if (!isFinishing())
                commanUtilsApp.hideProgressDialog();
        } */
    }

    private void startImageMasking(int i) {
        try {
            CommanUtilsApp.strictModePermitAll();
            URL url = new URL(maskImages.get(i));
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap mask = BitmapFactory.decodeStream(input);
            makeMaskImage(mask, MediaStore.Images.Media.getBitmap(getContentResolver(),
                    Uri.fromFile(new File(image))), maskImageViewList.get(i));
            if (maskImages.size() > i + 1) {
                startImageMasking(i + 1);
            } else {
                if (!isFinishing())
                    commanUtilsApp.hideProgressDialog();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Font getFont(FrameBean frameBean) {
        Font font = new Font();
        if (frameBean.color.length() > 0)
            font.setColor(parseColor(frameBean.color));
        return font;
    }

    double width, height;
    float x, y;
    BorderedTextView borderedTextView;
    TextView ed_myEdittext;

    private void addApiNewText(FrameBean frameBean, Font font, FrameLayout mainFrame) {
        x = (parseFloat(frameBean.x) * screenWidth) / mainFrameWidth;
        y = (parseFloat(frameBean.y) * screenWidth) / mainFrameWidth;
        width = Math.ceil(((Double.parseDouble(frameBean.width) * screenWidth) / mainFrameWidth));
        height = Math.ceil((Double.parseDouble(frameBean.height) * screenWidth) / mainFrameWidth);

        if (!(frameBean.border_color.length() > 0)) {
            ed_myEdittext = CommanUtilsApp.getApiTextView(SuggestionActivity.this, frameBean, x, y, width, height, font, true);
            mainFrame.addView(ed_myEdittext);
            mainFrame.requestLayout();
            addChangeColorViewList(frameBean, ed_myEdittext);
        } else {
            borderedTextView = CommanUtilsApp.getBorderedTextFromApi(SuggestionActivity.this,
                    frameBean, x, y, width, height, font);
            mainFrame.addView(borderedTextView);
            mainFrame.requestLayout();
            addChangeColorViewList(frameBean, borderedTextView);
        }
    }

    // Uri uri;
    ImageView imageView;

    private void addImage(FrameBean frameBean, FrameLayout mainFrame, int productCount) {
        //uri = Uri.fromFile(new File(image));
        imageView = new ImageView(this);
        if (frameBean.is_shape.equals("1") || frameBean.is_mask.equals("1")) {
            imageView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT));
            imageView.getLayoutParams().width = (int) (Float.parseFloat(frameBean.width) * screenWidth) / mainFrameWidth;
            imageView.getLayoutParams().height = (int) (Float.parseFloat(frameBean.height) * screenWidth) / mainFrameWidth;
            imageView.setX(Float.parseFloat(frameBean.x) * screenWidth / mainFrameWidth);
            imageView.setY(Float.parseFloat(frameBean.y) * screenWidth / mainFrameWidth);
            imageView.setAdjustViewBounds(true);
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            if (frameBean.is_mask.equalsIgnoreCase("1")) {
                maskImageViewList.add(imageView);
                maskImages.add(frameBean.src);
                try {
                    CommanUtilsApp.strictModePermitAll();
                    URL url = new URL(frameBean.src);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setDoInput(true);
                    connection.connect();
                    InputStream input = connection.getInputStream();
                    Bitmap mask = BitmapFactory.decodeStream(input);
                    makeMaskImage(mask, MediaStore.Images.Media.getBitmap(getContentResolver(),
                            Uri.fromFile(new File(image))), imageView);
                    /* if (maskImages.size() > i + 1) {
                        startImageMasking(i + 1);
                    } else {
                        if (!isFinishing())
                            commanUtilsApp.hideProgressDialog();
                    } */
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                imageView.setBackgroundColor(parseColor("#CECDCD"));
                Bitmap bitmap = modifyOrientation(image);
                imageView.setImageBitmap(bitmap);
                // imageView.setImageURI(uri);
                imageView.requestLayout();
            }
            mainFrame.addView(imageView);
            mainFrame.requestLayout();
        } else {
            imageView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT));
            imageView.setX(Float.parseFloat(frameBean.x) * screenWidth / mainFrameWidth);
            imageView.setY(Float.parseFloat(frameBean.y) * screenWidth / mainFrameWidth);
            imageView.getLayoutParams().width = (int) (Float.parseFloat(frameBean.width) * screenWidth) / mainFrameWidth;
            imageView.getLayoutParams().height = (int) (Float.parseFloat(frameBean.height) * screenWidth) / mainFrameWidth;
            imageView.requestLayout();
            imageView.setAdjustViewBounds(true);
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.requestLayout();
            if (!frameBean.src.isEmpty()) {
                Picasso.get().load(frameBean.src).into(imageView); // .placeholder(R.drawable.progress).into(imageView);
            }
            mainFrame.addView(imageView);
            mainFrame.requestLayout();
        }
        if (frameBean.is_hue.equals("1")) {
            hueImages.add(imageView);
        }
        addChangeColorViewList(frameBean, imageView);
    }

    private void addChangeColorViewList(FrameBean frameBean, View view) {
        if (frameBean.is_bt.equals("1")) {
            btImages.add(view);
        }
        if (frameBean.is_wt.equals("1")) {
            wtImages.add(view);
        }
        if (frameBean.is_pt.equals("1")) {
            ptImages.add(view);
        }
        if (frameBean.is_st.equals("1")) {
            stImages.add(view);
        }
    }

    public void makeMaskImage(Bitmap mask, Bitmap original, ImageView imageView) {
        original = Bitmap.createScaledBitmap(original,
                mask.getWidth(), mask.getHeight(), true);

        Bitmap result = Bitmap.createBitmap(mask.getWidth(), mask.getHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas mCanvas = new Canvas(result);
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mCanvas.drawBitmap(mask, 0, 0, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        mCanvas.drawBitmap(original, 0, 0, paint);
        paint.setXfermode(null);
        imageView.setImageBitmap(result);
        imageView.requestLayout();
        imageView.invalidate();
    }

    public void primarySecondaryTonePos(boolean isPicker, boolean isPt, int i,
                                        int color) {
        if (isPt) {
            if (primarySecondaryColorPos != i)
                secondaryColor = -1;
            primaryColor = color;
            changesPrimarySecondaryColor(true, i, primaryColor);
        } else {
            if (primarySecondaryColorPos != i)
                primaryColor = -1;
            secondaryColor = color;
            changesPrimarySecondaryColor(false, i, secondaryColor);
        }
        primarySecondaryColorPos = i;
    }

    private void changesPrimarySecondaryColor(boolean isPrimary, int pos, int color) {
        if (isPrimary)
            for (int j = 0; j < primaryToneList.get(pos).size(); j++) {
                if (primaryToneList.get(pos).get(j) instanceof ImageView)
                    ((ImageView) primaryToneList.get(pos).get(j)).setColorFilter(color);
                else if (primaryToneList.get(pos).get(j) instanceof BorderedTextView)
                    ((BorderedTextView) primaryToneList.get(pos).get(j)).setBorderedColor(color);
                else
                    ((TextView) primaryToneList.get(pos).get(j)).setTextColor(color);
            }
        else
            for (int j = 0; j < secondaryToneList.get(pos).size(); j++) {
                if (secondaryToneList.get(pos).get(j) instanceof ImageView)
                    ((ImageView) secondaryToneList.get(pos).get(j)).setColorFilter(color);
                else if (secondaryToneList.get(pos).get(j) instanceof BorderedTextView)
                    ((BorderedTextView) secondaryToneList.get(pos).get(j)).setBorderedColor(color);
                else
                    ((TextView) secondaryToneList.get(pos).get(j)).setTextColor(color);
            }
    }

    private void downloadMaskImage(int i, String destPath) {
        if (!CommanUtilsApp.isNetworkAvailable(SuggestionActivity.this))
            return;
        CommanUtilsApp commanUtilsApp = new CommanUtilsApp(SuggestionActivity.this);
        commanUtilsApp.createProgressDialog(getResources().getString(R.string.loading));
        commanUtilsApp.showProgressDialog();
        OkHttpClient client = new OkHttpClient.Builder().build();
        DownloadManager manager = new DownloadManager.Builder().context(this)
                .downloader(OkHttpDownloader.create(client))
                .threadPoolSize(3)
                .logger(new Logger() {
                    @Override
                    public void log(String message) {
                    }
                })
                .build();
        String maskUrl = maskImages.get(i);
        DownloadRequest request =
                new DownloadRequest.Builder()
                        .url(maskUrl)
                        .retryTime(5)
                        .retryInterval(2, TimeUnit.SECONDS)
                        .progressInterval(1, TimeUnit.SECONDS)
                        .priority(Priority.HIGH)
                        .destinationFilePath(destPath)
                        .downloadCallback(new DownloadCallback() {
                            @Override
                            public void onStart(int downloadId, long totalBytes) {
                            }

                            @Override
                            public void onRetry(int downloadId) {
                            }

                            @Override
                            public void onProgress(int downloadId, long bytesWritten, long totalBytes) {
                            }

                            @Override
                            public void onSuccess(int downloadId, String filePath) {
                                if (!isFinishing())
                                    commanUtilsApp.hideProgressDialog();
                                Intent intent = getIntent();
                                intent.putExtra(KEY_LIST, allDataListBean.get(i));
                                intent.putExtra(KEY_FRAME_ID, frameIdList.get(i));
                                intent.putExtra(KEY_PRIMARY_COLOR, primarySecondaryColorPos == i ? primaryColor : -1);
                                intent.putExtra(KEY_SECONDARY_COLOR, primarySecondaryColorPos == i ? secondaryColor : -1);
                                setResult(SuggestionRequest, intent);
                                finish();
                            }

                            @Override
                            public void onFailure(int downloadId, int statusCode, String errMsg) {
                                commanUtilsApp.hideProgressDialog();
                            }
                        })
                        .build();
        manager.add(request);
    }

    @Override
    protected void onDestroy() {
        if (commanUtilsApp != null)
            commanUtilsApp.hideProgressDialog();
        super.onDestroy();
    }

    @Override
    public void onTaskCompleted(int service_code, String response) {
        if (!isFinishing())
            commanUtilsApp.hideProgressDialog();
    }
}
