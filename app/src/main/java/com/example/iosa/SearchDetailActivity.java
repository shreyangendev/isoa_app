package com.example.iosa;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;

import com.appizona.yehiahd.fastsave.FastSave;
import com.example.iosa.Retrofitapi.APIInterface;
import com.example.iosa.Retrofitapi.ApiClient;
import com.example.iosa.VollyApi.NoConnectivityException;
import com.example.iosa.bean.ReportApiResponce;
import com.example.iosa.bean.ReportBean;
import com.example.iosa.utils.CommanUtilsApp;
import com.example.iosa.utils.GpsTracker;
import com.example.iosa.utils.UtilsStatusbar;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.iosa.utils.ApiConstants.APIKEY;
import static com.example.iosa.utils.ApiConstants.BASE_URL;


public class SearchDetailActivity extends BaseActivity {

    @BindView(R.id.imgBack)
    ImageView imgBack;
    @BindView(R.id.txtName)
    TextView txtName;
    @BindView(R.id.cardTop)
    CardView cardTop;
    @BindView(R.id.imgCall)
    ImageView imgCall;
    @BindView(R.id.txtSearchDetailNumber)
    TextView txtSearchDetailNumber;
    @BindView(R.id.txtSearchDetailShopeNm)
    TextView txtSearchDetailShopeNm;
    @BindView(R.id.txtSearchDetailEmail)
    TextView txtSearchDetailEmail;
    @BindView(R.id.imgWAShareLocationnUrl)
    ImageView imgWAShareLocationnUrl;
    @BindView(R.id.txtSearchDetailLocation)
    TextView txtSearchDetailLocation;
    @BindView(R.id.llLocation)
    LinearLayout llLocation;
    @BindView(R.id.txtDate)
    TextView txtDate;
    @BindView(R.id.llOne)
    LinearLayout llOne;
    @BindView(R.id.llTwo)
    LinearLayout llTwo;
    @BindView(R.id.llThree)
    LinearLayout llThree;

    private GpsTracker gpsTracker;
    CommanUtilsApp commanUtilsApp;
    ReportBean reportBean;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new UtilsStatusbar(SearchDetailActivity.this).setStatuBar();
        setContentView(R.layout.activity_search_detail);
        ButterKnife.bind(this);

        reportBean = (ReportBean) getIntent().getSerializableExtra("data");
        String date = reportBean.getAddedOn();
        try {
            Date newDate = new SimpleDateFormat("yyyy-MM-dd").parse(date);
            date = new SimpleDateFormat("dd MMM yyyy").format(newDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        txtDate.setText(date);

        txtSearchDetailShopeNm.setText(reportBean.getShopName());
        txtSearchDetailEmail.setText(reportBean.getEmail());
        txtSearchDetailNumber.setText(reportBean.getPhone());
        txtName.setText(reportBean.getCustomerName());

        commanUtilsApp = new CommanUtilsApp(this);
        commanUtilsApp.createProgressDialog(getResources().getString(R.string.loading));
        getLocation();

        getPromoHistory();
    }

    public void getLocation() {
        gpsTracker = new GpsTracker(SearchDetailActivity.this);
        if (gpsTracker.canGetLocation()) {
            double latitude = gpsTracker.getLatitude();
            double longitude = gpsTracker.getLongitude();
            txtSearchDetailLocation.setText(latitude + " " + longitude);
        } else {
            gpsTracker.showSettingsAlert();
        }
    }

    private void getPromoHistory() {
        commanUtilsApp.showProgressDialog();
        Call<JsonObject> call = ApiClient.getClient(BASE_URL, getApplicationContext()).create(APIInterface.class)
                .getPromoHistory(FastSave.getInstance().getString(APIKEY, ""), reportBean.getCustomerId());
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> objectResponse) {
                if (objectResponse.body().get("success").toString().contains("1")) {
                    Gson gson = new Gson();

                    ReportApiResponce response = gson.fromJson(objectResponse.body().toString(), ReportApiResponce.class);
                    if (response.getSuccess().equalsIgnoreCase("1")) {

                    }
                } else {
                    CommanUtilsApp.showToast(SearchDetailActivity.this, objectResponse.body().get("error").toString());
                }
                commanUtilsApp.hideProgressDialog();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                t.printStackTrace();
                if (t instanceof NoConnectivityException) {
                    CommanUtilsApp.showToast(SearchDetailActivity.this, getResources().getString(R.string.check_network));
                }
                commanUtilsApp.showProgressDialog();
            }
        });
    }

    @OnClick({R.id.imgBack, R.id.imgWAShareLocationnUrl})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgBack:
                finish();
                break;
            case R.id.imgWAShareLocationnUrl:
                getLocation();
                break;
        }
    }
}