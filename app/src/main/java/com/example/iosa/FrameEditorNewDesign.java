package com.example.iosa;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.Layout;
import android.text.TextUtils;
import android.transition.Slide;
import android.transition.Transition;
import android.transition.TransitionManager;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.appizona.yehiahd.fastsave.FastSave;
import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.features.ReturnMode;
import com.esafirm.imagepicker.model.Image;
import com.example.iosa.Adapter.PrimarySecondaryColorListAdapter;
import com.example.iosa.Coustomer.BorderedTextView;
import com.example.iosa.Coustomer.MagnifierView;
import com.example.iosa.Coustomer.PrimarySecondaryTonePos;
import com.example.iosa.VollyApi.ApiCall;
import com.example.iosa.VollyApi.VolleyTaskCompleteListener;
import com.example.iosa.bean.FrameBean;
import com.example.iosa.bean.ListBean;
import com.example.iosa.bean.TutorialBean;
import com.example.iosa.customTextSticker.Font;
import com.example.iosa.customTextSticker.ImageStickerViewNew;
import com.example.iosa.logo.SelectLogoActivity;
import com.example.iosa.texteditor.ColorListAdapter;
import com.example.iosa.texteditor.FontListAdapter;
import com.example.iosa.texteditor.TextEditorDataBean;
import com.example.iosa.texteditor.TextStickerViewNew1;
import com.example.iosa.utils.ApiConstants;
import com.example.iosa.utils.CommanUtilsApp;
import com.example.iosa.utils.TextEditorDialogFragment;
import com.example.iosa.utils.UtilsStatusbar;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.jaredrummler.android.colorpicker.ColorPickerDialog;
import com.jaredrummler.android.colorpicker.ColorPickerDialogListener;
import com.kimo.lib.alexei.algorithms.Quantize;
import com.lihang.ShadowLayout;
import com.squareup.picasso.Picasso;
import com.yalantis.ucrop.UCrop;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.graphics.BitmapFactory.decodeStream;
import static android.graphics.Color.BLACK;
import static android.graphics.Color.RED;
import static android.graphics.Color.TRANSPARENT;
import static android.graphics.Color.WHITE;
import static android.graphics.Color.parseColor;
import static androidx.browser.customtabs.CustomTabsService.KEY_URL;
import static com.example.iosa.utils.ApiConstants.CAT_ID;
import static com.example.iosa.utils.ApiConstants.FRAME_ID;
import static com.example.iosa.utils.ApiConstants.GET_LIST_BY_ID;
import static com.example.iosa.utils.ApiConstants.GET_TUTORIAL_LIST;
import static com.example.iosa.utils.ApiConstants.KEY_BEAN;
import static com.example.iosa.utils.ApiConstants.KEY_IMAGE;
import static com.example.iosa.utils.ApiConstants.KEY_IMAGES;
import static com.example.iosa.utils.ApiConstants.KEY_MAIN_CAT_ID;
import static com.example.iosa.utils.ApiConstants.KEY_POS;
import static com.example.iosa.utils.ApiConstants.KEY_SUB_CAT_ID;
import static com.example.iosa.utils.ApiConstants.KEY_TUTORIAL_LIST;
import static com.example.iosa.utils.ApiConstants.KEY_TYPE;
import static com.example.iosa.utils.ApiConstants.OFFER_ID;
import static com.example.iosa.utils.ApiConstants.POSTER_ID;
import static com.example.iosa.utils.ApiConstants.PurchaseRequest;
import static com.example.iosa.utils.ApiConstants.REQUEST_CROP;
import static com.example.iosa.utils.ApiConstants.SUCCESS;
import static com.example.iosa.utils.ApiConstants.SUGGETION_COUNT;
import static com.example.iosa.utils.ApiConstants.SelectLogoRequest;
import static com.example.iosa.utils.ApiConstants.SuggestionRequest;
import static com.example.iosa.utils.ApiConstants.UNIQUE_KEY;
import static com.example.iosa.utils.ApiConstants.UNIQUE_KEY_VALUE;
import static com.example.iosa.utils.ApiConstants.WISH_ID;
import static com.example.iosa.utils.CommanUtilsApp.getBitmapFromURI;
import static com.example.iosa.utils.CommanUtilsApp.getFontFileName;
import static com.example.iosa.utils.CommanUtilsApp.getLogoList;
import static com.example.iosa.utils.CommanUtilsApp.getMyPostIdList;
import static com.example.iosa.utils.CommanUtilsApp.getPixelsMatrixFromBitmap;
import static com.example.iosa.utils.CommanUtilsApp.isPurchase;
import static com.example.iosa.utils.CommanUtilsApp.isPurchaseSuccess;
import static com.example.iosa.utils.CommanUtilsApp.modifyOrientation;
import static com.example.iosa.utils.KeyParameterConst.KEY_CATEGORY_ID;
import static com.example.iosa.utils.KeyParameterConst.KEY_COLORS_ARRAYS;
import static com.example.iosa.utils.KeyParameterConst.KEY_COUNT;
import static com.example.iosa.utils.KeyParameterConst.KEY_FRAME_ID;
import static com.example.iosa.utils.KeyParameterConst.KEY_LIST;
import static com.example.iosa.utils.KeyParameterConst.KEY_PATH;
import static com.example.iosa.utils.KeyParameterConst.KEY_PRIMARY_COLOR;
import static com.example.iosa.utils.KeyParameterConst.KEY_SECONDARY_COLOR;
import static java.lang.Float.parseFloat;

public class FrameEditorNewDesign extends BaseActivity implements
        VolleyTaskCompleteListener, MagnifierView.OnTouchList,
        FontListAdapter.FontClick, ColorListAdapter.ColorClick,
        ColorPickerDialogListener, PrimarySecondaryTonePos, ImageStickerViewNew.GetNewPos {

    @BindView(R.id.rlTop)
    RelativeLayout rlTop;
    public static RelativeLayout rlMainView;
    @BindView(R.id.llList)
    LinearLayout llList;
    @BindView(R.id.scroll)
    ScrollView scroll;
    @BindView(R.id.rlFull)
    RelativeLayout rlFull;
    @BindView(R.id.rlBottomMoveDeleteDialog)
    RelativeLayout rlBottomMoveDeleteDialog;

    CardView suggestionTextGroup1, suggestionTextGroup2;
    PrimarySecondaryTonePos primarySecondaryTonePos;
    ApiCall apiCall;
    public static ImageView selectedImage;
    public static TextView currentEdtSticker;
    public static BorderedTextView currentBorderText;
    String responseString;
    public static int pos = 0;
    public static float mainFrameHeight, mainFrameWidth, screenWidth, screenHeight;
    ArrayList<FrameBean> testStickers;
    public static ArrayList<FrameBean> frameImagesBeans, frameTextBeans;
    public static ArrayList<ImageView> allImages;
    ArrayList<Boolean> whiteTextList, blackTextList;
    ArrayList<ArrayList<ImageView>> hueImagesList;
    ArrayList<ArrayList<View>> secondaryToneList, primaryToneList;
    ArrayList<ArrayList<View>> blackToneList, whiteToneList;
    public static ImageView clickedImageView;
    public static RecyclerView clickedPrimaryRv, suggestionPrimaryRv, clickedSecondaryRv, suggestionSecondaryRv;
    RelativeLayout rlHueSeekbar;
    public static String clickedImageUrl;
    public static boolean isLogo = false, isText = false, isRemoveBg = false, isBordered = false,
            isMask = false, isAddMoreProduct = false, isPrimaryTone, isSecondaryTone, isTextColor;
    ArrayList<ArrayList<FrameBean>> allDataList;
    ArrayList<FrameBean> responseDataListBean;
    FrameLayout clickedFrame;
    String NEW_ARRIVAL_STICKER = "NEW_ARRIVAL_STICKER";
    int clickedPos = 0, clickedFramePos, suggestionClickPos, textClickCount;
    FrameLayout suggestionFrame, suggetionRlLogo, clickedRlLogo;
    RelativeLayout suggestionFrameView;
    ImageView suggestionImgWaterMark;
    //List<String> productImages;
    ArrayList<ArrayList<Integer>> colorsArrays;
    ArrayList<Float> listShapeImageX, listShapeImageY, listShapeImageHeight;
    ArrayList<ImageView> hueImages;
    ArrayList<View> ptImages, stImages;
    ArrayList<View> btImages, wtImages;
    ArrayList<FrameLayout> mainFrameList;
    ArrayList<RelativeLayout> rlMainFrameViewList;
    ArrayList<FrameLayout> rlLogoList;
    ArrayList<String> frameIdList;
    ArrayList<TutorialBean> tutorialList;
    String instaImage;

    int suggetionCount;
    public static MagnifierView.OnTouchList touchList;
    public static MagnifierView mv;
    int addedProductSize = 0, tutorial_list_request = 7;
    CommanUtilsApp commanUtilsApp;
    String mainCatId, frameId, subCatId, maskImageUrl;
    ApiConstants apiConstants;
    boolean isDownloadAll = false, isFirst = true;// isWhiteTextGroupClick = false, isBlackTextGroupClick = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new UtilsStatusbar(FrameEditorNewDesign.this).setStatuBar();
        setContentView(R.layout.activity_new_design_edit_frame);
        ButterKnife.bind(this);
        instaImage = getIntent().getExtras().getString("image");
        Log.d("image", instaImage + " ");


        commanUtilsApp = new CommanUtilsApp(FrameEditorNewDesign.this);
        initArrays();
        getNewPos = this;
        tutorialList = (ArrayList<TutorialBean>) getIntent().getSerializableExtra(KEY_TUTORIAL_LIST);
        //productImages = (ArrayList<String>) getIntent().getSerializableExtra(KEY_IMAGES);
        frameId = getIntent().getStringExtra(KEY_TYPE);
        mainCatId = getIntent().getStringExtra(KEY_MAIN_CAT_ID);
        subCatId = getIntent().getStringExtra(KEY_SUB_CAT_ID);
        rlMainView = findViewById(R.id.rlMainView);
        apiConstants = new ApiConstants();
        primarySecondaryTonePos = this;
        touchList = this;
        rlFull.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (findViewById(R.id.llDeleteImage).isShown())
                    openDeleteTextImage(false);
            }
        });
        getDataFromApi();
        setUpTextEditor();
    }

    void initArrays() {
        suggetionCount = FastSave.getInstance().getInt(SUGGETION_COUNT, 0);
        frameIdList = new ArrayList<>();
        rlMainFrameViewList = new ArrayList<>();
        whiteTextList = new ArrayList<>();
        blackTextList = new ArrayList<>();
        mainFrameList = new ArrayList<>();
        rlLogoList = new ArrayList<>();
        // productImages = new ArrayList<>();
        colorsArrays = new ArrayList<>();
        allDataList = new ArrayList<>();
        hueImages = new ArrayList<>();
        btImages = new ArrayList<>();
        wtImages = new ArrayList<>();
        ptImages = new ArrayList<>();
        stImages = new ArrayList<>();
        listShapeImageX = new ArrayList<>();
        listShapeImageY = new ArrayList<>();
        listShapeImageHeight = new ArrayList<>();
        frameImagesBeans = new ArrayList<>();
        frameTextBeans = new ArrayList<>();
        testStickers = new ArrayList<>();
        allImages = new ArrayList<>();
        blackToneList = new ArrayList<>();
        whiteToneList = new ArrayList<>();
        secondaryToneList = new ArrayList<>();
        primaryToneList = new ArrayList<>();
        hueImagesList = new ArrayList<>();
    }

    //=================  Frame Data Api Call  ===================//
    private void getDataFromApi() {
        ListBean listBean = (ListBean) getIntent().getSerializableExtra(KEY_BEAN);
        apiCall = new ApiCall(this, this);
        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put(UNIQUE_KEY, UNIQUE_KEY_VALUE);
        hashMap.put(FRAME_ID, listBean.id);
        frameIdList.add(listBean.id);
        if (CommanUtilsApp.isNetworkAvailable(FrameEditorNewDesign.this))
            apiCall.GetData(Request.Method.POST, GET_LIST_BY_ID, hashMap, 0);
    }

    private void parseDatas() {
        frameImagesBeans.clear();
        frameTextBeans.clear();
        allImages.clear();
        pos = 0;
        responseDataListBean = new ArrayList<>();
        ArrayList<FrameBean> frameBeans = apiConstants.getFrameSetupData(FrameEditorNewDesign.this, responseString);
        if (frameBeans.size() > 0) {
            responseDataListBean = frameBeans;
            scroll.setVisibility(View.VISIBLE);
            allDataList.add(responseDataListBean);
            new SetData(false).execute();
        } else {
            scroll.setVisibility(View.GONE);
        }
    }

    //=================  Setup Frame  ===================//
    private void setFrame(int i, boolean isFromMoreProduct) {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView;
        if (FastSave.getInstance().getString(CAT_ID, "").equalsIgnoreCase(WISH_ID)) {
            rowView = inflater.inflate(R.layout.greetin_frame_edit_list_item, null);
        } else {
            rowView = inflater.inflate(R.layout.frame_edit_list_item, null);
        }

        FrameLayout mainFrame = rowView.findViewById(R.id.rlMain);
        FrameLayout rlLogo = rowView.findViewById(R.id.rlLogo);
        RelativeLayout rlMainView = rowView.findViewById(R.id.rlMainView);
        RelativeLayout rlMainFrameView = rowView.findViewById(R.id.rlMainFrameView);
        ImageView imgWaterMark = rowView.findViewById(R.id.imgWaterMark);
        SeekBar seekBar = rowView.findViewById(R.id.seekBar);
        CardView cardPrice = rowView.findViewById(R.id.cardPrice);
        CardView cardText = rowView.findViewById(R.id.cardAddText);
        CardView cardLogo = rowView.findViewById(R.id.cardLogo);
        CardView cardEditor = rowView.findViewById(R.id.cardEditor);
        CardView cardWt = rowView.findViewById(R.id.cardWt);
        CardView cardBt = rowView.findViewById(R.id.cardBt);
        CardView cardSuggestOption = rowView.findViewById(R.id.cardSuggestOption);
        CardView cardShare = rowView.findViewById(R.id.cardShare);
        CardView cardDelete = rowView.findViewById(R.id.cardDelete);
        rowView.findViewById(R.id.cardNewArrival).setVisibility(View.GONE);
        // ImageView imgPicker = rowView.findViewById(R.id.imgPicker);
        RecyclerView rvPrimaryColor = rowView.findViewById(R.id.rvPrimaryColor);
        RecyclerView rvSecondaryColor = rowView.findViewById(R.id.rvSecondaryColor);
        RelativeLayout rlSeekBar = rowView.findViewById(R.id.rlSeekBar);
        CardView cardTextGroup1 = rowView.findViewById(R.id.llTextGroup1);
        CardView cardTextGroup2 = rowView.findViewById(R.id.llTextGroup2);
        TextView txtCounter = rowView.findViewById(R.id.txtCounter);

        rowView.findViewById(R.id.cardCounter).setVisibility(
                CommanUtilsApp.isPurchase(FrameEditorNewDesign.this, false) ? View.GONE : View.VISIBLE);

        txtCounter.setText((10 - suggetionCount) + " FREE");

        imgWaterMark.setVisibility(isPurchase(FrameEditorNewDesign.this, false) ? View.GONE : View.VISIBLE);

        if (colorsArrays != null && colorsArrays.size() > i) {
            if (colorsArrays.get(i) != null) {
                rvSecondaryColor.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        rvSecondaryColor.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        rvSecondaryColor.getLayoutParams().height = rvSecondaryColor.getWidth() + 10;
                        rvSecondaryColor.requestLayout();
                        rvSecondaryColor.setAdapter(
                                new PrimarySecondaryColorListAdapter(FrameEditorNewDesign.this, colorsArrays.get(i), i,
                                        primarySecondaryTonePos, false, rvSecondaryColor.getWidth()));
                    }
                });
                rvPrimaryColor.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        rvPrimaryColor.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        rvPrimaryColor.getLayoutParams().height = rvPrimaryColor.getWidth() + 10;
                        rvPrimaryColor.requestLayout();
                        rvPrimaryColor.setAdapter(
                                new PrimarySecondaryColorListAdapter(FrameEditorNewDesign.this, colorsArrays.get(i), i,
                                        primarySecondaryTonePos, true, rvPrimaryColor.getWidth()));
                    }
                });
            }
        }
        rlMainView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeAllEdiotors();
            }
        });
        cardWt.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                cardWt.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                cardWt.getLayoutParams().height = cardWt.getWidth();
                cardWt.requestLayout();
            }
        });
        cardBt.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                cardBt.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                cardBt.getLayoutParams().height = cardBt.getWidth();
                cardBt.requestLayout();
            }
        });
        cardWt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // isWhiteTextGroupClick = true;
                closeAllEdiotors();
                clickedFramePos = i;
                /* toggle(true);
                Log.d("====== white size :- ", whiteToneList.get(i).size() + "");
                CustomBottomColorChangeDialog(false); */
                for (int j = 0; j < whiteToneList.get(i).size(); j++) {
                    // whiteToneList.get(i).get(j).setColorFilter(whiteTextList.get(i) ? WHITE : BLACK);
                    if (whiteToneList.get(i).get(j) instanceof ImageView)
                        ((ImageView) whiteToneList.get(i).get(j)).setColorFilter(whiteTextList.get(i) ? BLACK : WHITE);
                    else if (whiteToneList.get(i).get(j) instanceof BorderedTextView)
                        ((BorderedTextView) whiteToneList.get(i).get(j)).setBorderedColor(whiteTextList.get(i) ? BLACK : WHITE);
                    else
                        ((TextView) whiteToneList.get(i).get(j)).setTextColor(whiteTextList.get(i) ? BLACK : WHITE);
                }
                whiteTextList.set(i, !whiteTextList.get(i));
                cardWt.setCardBackgroundColor(whiteTextList.get(i) ? WHITE : BLACK);
            }
        });

        cardBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // isBlackTextGroupClick = true;
                closeAllEdiotors();
                clickedFramePos = i;
                /*toggle(true);
                CustomBottomColorChangeDialog(false);
                Log.d("====== black size :- ", blackToneList.get(i).size() + "");*/
                for (int j = 0; j < blackToneList.get(i).size(); j++) {
                    if (blackToneList.get(i).get(j) instanceof ImageView)
                        ((ImageView) blackToneList.get(i).get(j)).setColorFilter(blackTextList.get(i) ? WHITE : BLACK);
                    else if (blackToneList.get(i).get(j) instanceof BorderedTextView)
                        ((BorderedTextView) blackToneList.get(i).get(j)).setBorderedColor(blackTextList.get(i) ? WHITE : BLACK);
                    else
                        ((TextView) blackToneList.get(i).get(j)).setTextColor(blackTextList.get(i) ? WHITE : BLACK);
                }
                blackTextList.set(i, !blackTextList.get(i));
                cardBt.setCardBackgroundColor(blackTextList.get(i) ? BLACK : WHITE);
            }
        });
        /* imgPicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickedFramePos = i;
                              isTextColor = true;
                toggle(true);
                CustomBottomColorChangeDialog(false);
            }
        }); */
        cardLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeAllEdiotors();
                //if (isPurchase(FrameEditorNewDesign.this, true)) {
                if (!isSetSctickers) {
                    clickedFramePos = i;
                    clickedFrame = mainFrame;
                    clickedRlLogo = rlLogo;
                    selectLogo();
                } else isSetSctickers = false;
                // }
                // openAddLogoDialog(mainFrame);
            }
        });
        cardText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeAllEdiotors();
                if (!isSetSctickers) {
                    clickedFrame = mainFrame;
                    // addNewText(mainFrame);
                    createTextSticker(mainFrame);
                } else isSetSctickers = false;
            }
        });
        /* cardNewArrival.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeAllEdiotors();
                clickedFramePos = i;
                if (isPurchase(FrameEditorNewDesign.this, true)) {
                    startActivityForResult(new Intent(FrameEditorNewDesign.this,
                            NewArrivalActivity.class).putExtra("pos", clickedFramePos), 90);
                }
            }
        }); */
        cardPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeAllEdiotors();
                // if (isPurchase(FrameEditorNewDesign.this, true)) {
                if (!isSetSctickers) {
                    clickedFramePos = i;
                    AddPriceTagPriceTagSticker();
                } else isSetSctickers = false;
                // }
            }
        });
        cardShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(FrameEditorNewDesign.this, "Share Image", Toast.LENGTH_SHORT).show();
                closeAllEdiotors();
                clickedPos = i;
                List<String> ids = getMyPostIdList(FrameEditorNewDesign.this);
                if (!ids.contains(frameIdList.get(clickedPos))) {
                    ids.add(frameIdList.get(clickedPos));
                    CommanUtilsApp.saveMyPostIdList(ids, FrameEditorNewDesign.this);
                }
                new ShareImage().execute(rlMainFrameViewList.get(clickedPos));
            }
        });
        cardDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeAllEdiotors();
                mainFrameList.remove(mainFrame);
                rlLogoList.remove(rlLogo);
                llList.removeView(rowView);
                llList.requestLayout();
            }
        });
        cardEditor.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                if (cardEditor.getViewTreeObserver().isAlive())
                    cardEditor.getViewTreeObserver().removeOnPreDrawListener(this);
                screenWidth = cardEditor.getWidth();

                hueImages = new ArrayList<>();
                btImages = new ArrayList<>();
                wtImages = new ArrayList<>();
                ptImages = new ArrayList<>();
                stImages = new ArrayList<>();

                RelativeLayout relativeLayout = new RelativeLayout(FrameEditorNewDesign.this);
                relativeLayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT));
                // relativeLayout.setBackgroundColor(getResources().getColor(R.color.transparent_white));

                if (allDataList.size() > i)
                    for (int j = 0; j < allDataList.get(i).size(); j++) {
                        setFrameData(imgWaterMark, rlMainFrameView, relativeLayout, rlLogo, mainFrame, j, i, rvPrimaryColor, rvSecondaryColor,
                                cardTextGroup1, cardTextGroup2, isFromMoreProduct);
                    }
                mainFrame.addView(relativeLayout);
                blackToneList.add(btImages);
                whiteToneList.add(wtImages);
                secondaryToneList.add(stImages);
                primaryToneList.add(ptImages);
                hueImagesList.add(hueImages);

                if (hueImagesList != null && hueImagesList.size() > i && hueImagesList.get(i).size() > 0) {
                    rlSeekBar.setVisibility(View.VISIBLE);
                } else rlSeekBar.setVisibility(View.GONE);
                return true;
            }
        });
        seekBar.setMax(180);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                for (int k = 0; k < hueImagesList.get(i).size(); k++) {
                    hueImagesList.get(i).get(k).setColorFilter(new ColorFilterGenerator().adjustHue(progress - 180));
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
        whiteTextList.add(true);
        blackTextList.add(true);

        llList.addView(rowView);
        rlMainFrameViewList.add(rlMainFrameView);
        mainFrameList.add(mainFrame);
        rlLogoList.add(rlLogo);
        llList.requestLayout();

        if (!FastSave.getInstance().getString(CAT_ID, "").equalsIgnoreCase(WISH_ID)) {
            if (mainFrameList.size() > addedProductSize && firstFrameLogo != null) {
                float xx = firstFrameLogo.getLayoutLastX() - ((firstFrameLogo.getBitmaps().getWidth() - firstFrameLogo.getWidths()) / 2);
                float yy = firstFrameLogo.getLayoutLastY() - ((firstFrameLogo.getBitmaps().getHeight() - firstFrameLogo.getHeights()) / 2);
                setLogoInFrame(addedProductSize, firstFrameLogo.getStickerPath(), xx, yy,
                        firstFrameLogo.getHeights(), true);
            }
            // commanUtilsApp.hideProgressDialog();
        } /* else
            commanUtilsApp.hideProgressDialog(); */
    }

    @Override
    public void onTaskCompleted(int service_code, String response) {

        if (service_code == tutorial_list_request) {
            tutorialList.clear();
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.getString(SUCCESS).equalsIgnoreCase("1")) {
                    JSONArray jsonArray = jsonObject.getJSONArray("response");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject object = jsonArray.getJSONObject(i);
                        TutorialBean tutorialBean = new TutorialBean();
                        tutorialBean.category_id = object.getString("category_id");
                        tutorialBean.name = object.getString("name");
                        tutorialBean.video = object.getString("video");
                        tutorialBean.image = object.getString("image");
                        tutorialBean.youtube_url = object.getString("youtube_url");
                        tutorialList.add(tutorialBean);
                    }
                    openVideo();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            responseString = response;
            parseDatas();
        }

    }

    class SetNewArrival extends AsyncTask<Void, Void, Void> {
        String s;
        int pos;
        Bitmap decodeFile;

        public SetNewArrival(String s, int pos) {
            this.s = s;
            this.pos = pos;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            commanUtilsApp = new CommanUtilsApp(FrameEditorNewDesign.this);
            commanUtilsApp.createProgressDialog(getResources().getString(R.string.loading));
            if (!isFinishing())
                commanUtilsApp.showProgressDialog();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            for (int i = 0; i < mainFrameList.get(clickedFramePos).getChildCount(); i++) {
                if (mainFrameList.get(clickedFramePos).getChildAt(i) instanceof ImageStickerViewNew) {
                    if (mainFrameList.get(clickedFramePos).getChildAt(i).getTag() != null) {
                        if (mainFrameList.get(clickedFramePos).getChildAt(i).getTag().equals(NEW_ARRIVAL_STICKER)) {
                            mainFrameList.get(clickedFramePos).removeViewAt(i);
                            mainFrameList.get(clickedFramePos).requestLayout();
                        }
                    }
                }
            }
            commanUtilsApp.hideProgressDialog();
            ImageStickerViewNew imageSticker = new ImageStickerViewNew(FrameEditorNewDesign.this, s,
                    screenWidth / 2, decodeFile.getHeight() + 50, 1f, 0.0f, 2, true);
            imageSticker.setGif(false);
            imageSticker.setTag(NEW_ARRIVAL_STICKER);
            imageSticker.setBitmap(decodeFile, true);
            imageSticker.setSize(250f);
            imageSticker.setOperationListener(new ImageStickerViewNew.OperationListener() {
                public void onDeleteClick(ImageStickerViewNew imageStickerView) {
                    removeAllStickers();
                    mainFrameList.get(clickedFramePos).removeView(imageStickerView);
                }

                public void onEdit(ImageStickerViewNew imageStickerView) {
                    if (imageStickerView != null) {
                        imageStickerView.setInEdit(false);
                    }
                    imageStickerView.setInEdit(true);
                }

                public void onTop(ImageStickerViewNew imageStickerView) {
                }

                @Override
                public void onSelect(Boolean isClick) {
                    scroll.requestDisallowInterceptTouchEvent(isClick);
                }
            });
            mainFrameList.get(clickedFramePos).addView(imageSticker);
            mainFrameList.get(clickedFramePos).requestLayout();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            CommanUtilsApp.strictModePermitAll();
            try {
                URL url = new URL(s);
                decodeFile = decodeStream(url.openConnection().getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    private void selectLogo() {
        // Convert to byte array
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        CommanUtilsApp.makeBlurBg(FrameEditorNewDesign.this).compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        startActivityForResult(new Intent(getApplicationContext(), SelectLogoActivity.class)
                .putExtra(KEY_IMAGE, byteArray), SelectLogoRequest);
    }

    private void setFrameData(ImageView imgWaterMark, RelativeLayout rlMainFrameView, RelativeLayout relativeLayout,
                              FrameLayout rlLogo, FrameLayout mainFrame, int i, int productCount,
                              RecyclerView rvPrimaryColor, RecyclerView rvSecondaryColor,
                              CardView llTextGroup1, CardView llTextGroup2, boolean isFromMoreProduct) {
        if (i == 0) {
            mainFrameWidth = (int) parseFloat(allDataList.get(productCount).get(i).width);
            mainFrameHeight = (int) parseFloat(allDataList.get(productCount).get(i).height);
            screenHeight = (mainFrameHeight * screenWidth) / mainFrameWidth;
            mainFrame.getLayoutParams().width = (int) screenWidth;
            mainFrame.getLayoutParams().height = (int) screenHeight;
            mainFrame.requestLayout();
            rlLogo.getLayoutParams().width = (int) screenWidth;
            rlLogo.getLayoutParams().height = (int) screenHeight;
            rlLogo.requestLayout();
            rlMainFrameView.getLayoutParams().width = (int) screenWidth;
            rlMainFrameView.getLayoutParams().height = (int) screenHeight;
            rlMainFrameView.requestLayout();
        }
        if (allDataList.get(productCount).get(i).type.equalsIgnoreCase("image")) {
            frameImagesBeans.add(allDataList.get(productCount).get(i));
            int pos = frameImagesBeans.size() - 1;
            addImage(imgWaterMark, relativeLayout, frameImagesBeans.get(pos), mainFrame, productCount, rvPrimaryColor, rvSecondaryColor
                    , llTextGroup1, llTextGroup2, isFromMoreProduct);
        } else if (allDataList.get(productCount).get(i).type.equalsIgnoreCase("text")) {
            frameTextBeans.add(allDataList.get(productCount).get(i));
            int sizes = frameTextBeans.size() - 1;

            float size = (parseFloat(frameTextBeans.get(sizes).size) * screenWidth)
                    / mainFrameWidth;
            testStickers.add(allDataList.get(productCount).get(i));

            Font font = getFont(testStickers.get(sizes));
            font.setSize((float) (size / Resources.getSystem().getDisplayMetrics().density));
            Typeface tf = Typeface.createFromAsset(getAssets(),
                    "fonts/" + getFontFileName(FrameEditorNewDesign.this, frameTextBeans.get(sizes).font_name));
            font.setTypeface(tf);

            addApiNewText(testStickers.get(pos), font, mainFrame);
            pos++;
        }
    }


    Bitmap mask;

    private void addImage(ImageView imgWaterMark, RelativeLayout relativeLayout, FrameBean frameBean, FrameLayout mainFrame,
                          int productCount, RecyclerView rvPrimaryColor, RecyclerView rvSecondaryColor
            , CardView llTextGroup1, CardView llTextGroup2, boolean isFromMoreProduct) {
        Float x = parseFloat(frameBean.x) * screenWidth / mainFrameWidth;
        Float y = parseFloat(frameBean.y) * screenWidth / mainFrameWidth;

        int imgWidth = (int) ((parseFloat(frameBean.width) * screenWidth) / mainFrameWidth);
        int imgHeight = (int) ((parseFloat(frameBean.height) * screenWidth) / mainFrameWidth);

        ImageView imageView = new ImageView(this);
        if (frameBean.is_shape.equals("1") || frameBean.is_mask.equals("1")) {
            imageView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT));
            imageView.getLayoutParams().width = imgWidth;
            imageView.getLayoutParams().height = imgHeight;
            imageView.setX(x);
            imageView.setY(y);
            if (listShapeImageX.size() > productCount) {
                listShapeImageHeight.set(productCount, (float) imgHeight);
                listShapeImageX.set(productCount, x);
                listShapeImageY.set(productCount, y);
            } else {
                listShapeImageHeight.add((float) imgHeight);
                listShapeImageX.add(x);
                listShapeImageY.add(y);
                //if (!isFromMoreProduct) {
                isFirst = false;
                if (!FastSave.getInstance().getString(CAT_ID, "").equalsIgnoreCase(WISH_ID)) {
                    List<String> logoList = getLogoList(FrameEditorNewDesign.this);
                    // if (CommanUtilsApp.isPurchase(FrameEditorNewDesign.this, false))
                    if (logoList.size() > 0) {
                        Bitmap decodeFile = BitmapFactory.decodeFile(new File(logoList.get(0)).getAbsolutePath());
                        float newWidth = ((decodeFile.getWidth() * 150f) / decodeFile.getHeight());
                        setLogoInFrame(productCount, logoList.get(0),
                                x + ((decodeFile.getWidth()) / 2) - (decodeFile.getWidth() - newWidth) / 2 + 20,
                                y + ((decodeFile.getHeight()) / 2) - (decodeFile.getHeight() - 150f) / 2 + 20,
                                150f, false);
                    }
                }
            }

            imageView.setTag(instaImage);
            imageView.setAdjustViewBounds(true);
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            if (frameBean.is_mask.equals("0") && (frameId.equalsIgnoreCase(OFFER_ID) || frameId.equalsIgnoreCase(POSTER_ID)))
                if (!isPurchase(FrameEditorNewDesign.this, false)) {
                    imgWaterMark.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                        @Override
                        public void onGlobalLayout() {
                            imgWaterMark.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                            imgWaterMark.setX(x + imgWidth - imgWaterMark.getWidth());
                            imgWaterMark.setY(y + imgHeight - imgWaterMark.getHeight());
                        }
                    });
                }

            if (frameBean.is_mask.equalsIgnoreCase("1")) {
                CommanUtilsApp.strictModePermitAll();
                clickedImageView = imageView;
                clickedPrimaryRv = rvPrimaryColor;
                clickedSecondaryRv = rvSecondaryColor;
                maskImageUrl = frameBean.src;
               cropImage(instaImage, imgWidth, imgHeight);
            } else if (frameBean.is_shape.equals("1")) {
               /* if (productImages != null && productImages.size() > productCount) {
                    if (productImages.get(productCount) != null) {
                        Bitmap bitmap = modifyOrientation(productImages.get(productCount));
                        imageView.setImageBitmap(bitmap);
                        // imageView.setImageURI(Uri.parse(productImages.get(productCount)));
                    } else
                        Picasso.get().load(R.drawable.add_mask_image).placeholder(R.drawable.progress).into(imageView);
                } else
                    Picasso.get().load(R.drawable.add_mask_image).placeholder(R.drawable.progress).into(imageView);*/
                Picasso.get().load(new File(instaImage)).placeholder(R.drawable.progress).into(imageView);
            }
            imageView.requestLayout();
            imageView.setOnTouchListener(new SingleDoubleTapListner(FrameEditorNewDesign.this,
                    imageView, productCount, frameBean, rvPrimaryColor, rvSecondaryColor));

            mainFrame.addView(imageView);
            mainFrame.requestLayout();
        } else {
            imageView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT));
            imageView.setX(x);
            imageView.setY(y);
            imageView.getLayoutParams().width = imgWidth;
            imageView.getLayoutParams().height = imgHeight;
            imageView.requestLayout();
            imageView.setAdjustViewBounds(true);
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.requestLayout();
            if (frameBean.is_object.equalsIgnoreCase("1")) {
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        closeAllEdiotors();

                        if (!isSetSctickers) {
                            clickedFramePos = productCount;
                            if (selectedImage != null) {
                                selectedImage.setOnTouchListener(null);
                                selectedImage.setBackground(null);
                            }
                            if (currentEdtSticker != null) {
                                currentEdtSticker.setBackground(null);
                            }
                            selectedImage = imageView;
                            clickedImageView = imageView;
                            // if (frameBean.is_color.equals("1")) {
                            // rlBottomMoveDeleteDialog.setVisibility(View.VISIBLE);
                            isPrimaryTone = false;
                            isSecondaryTone = false;
                            isTextColor = false;
                            toggle(true);
                            isText = false;
                            // makeImageMovable(k);
                            if (frameBean.is_object.equalsIgnoreCase("1")) {
                                isRemoveBg = true;
                                isMask = false;
                                BottomSheetDialogFragment bottomSheetDialogFragment = new AddImageDialogFragment();
                                bottomSheetDialogFragment.show(getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
                            }
                            /* else {
                        clickedImageView.setBackground(getResources().getDrawable(R.drawable.border_to_view));
                        CustomBottomColorChangeDialog(true);
                    }
                    BottomSheetDialogFragment bottomSheetDialogFragment = new CustomBottomColorChangeDialog(k);
                    bottomSheetDialogFragment.show(getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
                              }
                      else if (frameBean.is_hue.equals("1")) {
                    BottomSheetDialogFragment bottomSheetDialogFragment = new CustomBottomHueDialog();
                    bottomSheetDialogFragment.show(getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
                } */
                        } else isSetSctickers = false;
                    }
                });
            }
            if (!frameBean.src.isEmpty()) {
                Picasso.get().load(frameBean.src).into(imageView);// .placeholder(R.drawable.progress).into(imageView);
            }
            allImages.add(imageView);
            mainFrame.addView(imageView);
            mainFrame.requestLayout();
        }

        if (frameBean.is_hue.equals("1")) {
            hueImages.add(imageView);
        }
        if (frameBean.is_bt.equals("1")) {
            btImages.add(imageView);
        }
        if (frameBean.is_wt.equals("1")) {
            wtImages.add(imageView);
        }
        if (frameBean.is_pt.equals("1")) {
            ptImages.add(imageView);
        }
        if (frameBean.is_st.equals("1")) {
            stImages.add(imageView);
        }
        if (frameBean.is_st.equals("1") || frameBean.is_pt.equals("1") || frameBean.is_wt.equals("1") || frameBean.is_bt.equals("1"))
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isLongClicked || isEditTextClicked) {
                        isEditTextClicked = false;
                        isLongClicked = false;
                    } else {
                        closeAllEdiotors();
                        clickedFramePos = productCount;
                        if (selectedImage != null) {
                            selectedImage.setOnTouchListener(null);
                            selectedImage.setBackground(null);
                        }
                        if (currentEdtSticker != null) {
                            currentEdtSticker.setBackground(null);
                        }
                        selectedImage = imageView;
                        clickedImageView = imageView;

                        isPrimaryTone = false;
                        isSecondaryTone = false;
                        isTextColor = false;

                        isText = false;
                        if (frameBean.is_wt.equals("1") || frameBean.is_bt.equals("1")) {
                            highLightTextGroup(frameBean.is_wt.equals("1") ? llTextGroup1 : llTextGroup2);
                        } else
                            highLightTextGroup(frameBean.is_pt.equals("1") ? rvPrimaryColor : rvSecondaryColor);
                    }
                }
            });
        // if (frameBean.is_wt.equals("1") || frameBean.is_bt.equals("1")) {
        if (!frameBean.is_shape.equals("1") || !frameBean.is_mask.equals("1"))
            imageView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    isLongClicked = true;
                    closeAllEdiotors();
                    clickedFrame = mainFrame;
                    clickedImageView = imageView;
                    openDeleteTextImage(true);
                    return false;
                }
            });
        // }
    }

    boolean isLongClicked = false;

    public Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            return decodeStream(input);
        } catch (IOException e) {
            // Log exception
            return null;
        }
    }

    public void makeMaskImage(Bitmap mask, Bitmap original, ImageView imageView) {
        original = Bitmap.createScaledBitmap(original,
                mask.getWidth(), mask.getHeight(), true);

        Bitmap result = Bitmap.createBitmap(mask.getWidth(), mask.getHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas mCanvas = new Canvas(result);
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mCanvas.drawBitmap(mask, 0, 0, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        mCanvas.drawBitmap(original, 0, 0, paint);
        paint.setXfermode(null);
        imageView.setImageBitmap(result);
        imageView.requestLayout();
        imageView.invalidate();
    }

    @SuppressLint("WrongConstant")
    private void highLightTextGroup(View ll) {
        // ll.setVisibility(View.INVISIBLE);
        Animation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(700); // You can manage the blinking time with this parameter
        anim.setStartOffset(100);
        anim.setRepeatMode(Animation.REVERSE);
        anim.setRepeatCount(2);
        ll.startAnimation(anim);

    }

    //================= Single Double Tap Listener ===================//
    private class SetData extends AsyncTask<URL, Integer, Bitmap> {

        boolean isFromMoreProduct;

        public SetData(boolean b) {
            isFromMoreProduct = b;
        }

        @Override
        protected Bitmap doInBackground(URL... urls) {

            setColor(instaImage, addedProductSize, false);

            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            /* commanUtilsApp.createProgressDialog("Frames Setting...");
            commanUtilsApp.showProgressDialog(); */
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            setFrame(addedProductSize, isFromMoreProduct);
        }
    }

    //================= Color Selection result ===================//
    @Override
    public void onTouchList(float pxl) {
        mv = null;
        rlFull.setVisibility(View.GONE);
        changeColors((int) pxl);
    }

    private void changeColors(int pxl) {
        if (isTextColor) {
            for (int j = 0; j < blackToneList.get(clickedFramePos).size(); j++) {
                // blackToneList.get(clickedFramePos).get(j).setColorFilter((int) pxl);
                if (blackToneList.get(clickedFramePos).get(j) instanceof ImageView)
                    ((ImageView) blackToneList.get(clickedFramePos).get(j)).setColorFilter((int) pxl);
                else
                    ((TextView) blackToneList.get(clickedFramePos).get(j)).setTextColor((int) pxl);
            }
            for (int j = 0; j < whiteToneList.get(clickedFramePos).size(); j++) {
                // whiteToneList.get(clickedFramePos).get(j).setColorFilter((int) pxl);
                if (whiteToneList.get(clickedFramePos).get(j) instanceof ImageView)
                    ((ImageView) whiteToneList.get(clickedFramePos).get(j)).setColorFilter((int) pxl);
                else
                    ((TextView) whiteToneList.get(clickedFramePos).get(j)).setTextColor((int) pxl);
            }
        } else if (isPrimaryTone) {
            // isPrimaryTone = false;
            changesPrimarySecondaryColor(true, clickedFramePos, (int) pxl);
            /* for (int j = 0; j < primaryToneList.get(clickedFramePos).size(); j++) {
                // primaryToneList.get(clickedFramePos).get(j).setColorFilter((int) pxl);
                if (primaryToneList.get(clickedFramePos).get(j) instanceof ImageView)
                    ((ImageView) primaryToneList.get(clickedFramePos).get(j)).setColorFilter((int) pxl);
                else
                    ((TextView) primaryToneList.get(clickedFramePos).get(j)).setTextColor((int) pxl);
            } */
        } else if (isSecondaryTone) {
            // isSecondaryTone = false;
            changesPrimarySecondaryColor(false, clickedFramePos, (int) pxl);
           /* for (int j = 0; j < secondaryToneList.get(clickedFramePos).size(); j++) {
                // secondaryToneList.get(clickedFramePos).get(j).setColorFilter((int) pxl);
                if (secondaryToneList.get(clickedFramePos).get(j) instanceof ImageView)
                    ((ImageView) secondaryToneList.get(clickedFramePos).get(j)).setColorFilter((int) pxl);
                else
                    ((TextView) secondaryToneList.get(clickedFramePos).get(j)).setTextColor((int) pxl);
            }*/
        } else if (isText) {
            if (isBordered) {
                currentBorderText.setBorderedColor((int) pxl);
            } else {
                currentEdtSticker.setTextColor((int) pxl);
            }
        } else clickedImageView.setColorFilter((int) pxl);
    }

    @Override
    public void onColorSelected(int dialogId, int color) {
        closeAllEdiotors();
        if (!isSetSctickers) {
            changeColors((int) color);
        } else isSetSctickers = false;
    }

    @Override
    public void onDialogDismissed(int dialogId) {
    }

    public void CustomBottomColorChangeDialog(boolean isDeleteEnable) { //extends BottomSheetDialogFragment {
        RecyclerView colorList = (RecyclerView) findViewById(R.id.rlColor);
        colorList.setAdapter(new ColorListAdapter(FrameEditorNewDesign.this,
                getResources().getStringArray(R.array.colors_list)));
        findViewById(R.id.viewDisableDelete).setVisibility(View.GONE);
        findViewById(R.id.viewDelete).setVisibility((isDeleteEnable) ? View.VISIBLE : View.INVISIBLE);
        // findViewById(R.id.viewDisableDelete).setVisibility((!isText) ? View.VISIBLE : View.GONE);
        findViewById(R.id.viewDisableMove).setVisibility((!isText) ? View.VISIBLE : View.GONE);
        findViewById(R.id.viewDisableMove).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
        findViewById(R.id.viewDisableDelete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
        findViewById(R.id.txtCancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // closeFragment();
                // rlBottomMoveDeleteDialog.setVisibility(View.GONE);
                closeAllEdiotors();
            }
        });
        findViewById(R.id.txtDone).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // closeFragment();
                // rlBottomMoveDeleteDialog.setVisibility(View.GONE);
                closeAllEdiotors();
            }
        });
        findViewById(R.id.llColorPicker).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // closeFragment();
                // rlBottomMoveDeleteDialog.setVisibility(View.GONE);
                closeAllEdiotors();
                ColorPickerDialog colorPickerDialog = new ColorPickerDialog();
                colorPickerDialog.newBuilder().setColor(R.color.white);
                colorPickerDialog.newBuilder().show(FrameEditorNewDesign.this);
            }
        });
        findViewById(R.id.llPickColor).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // closeFragment();
                // rlBottomMoveDeleteDialog.setVisibility(View.GONE);
                closeAllEdiotors();
                rlFull.setVisibility(View.VISIBLE);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mv = new MagnifierView.Builder(FrameEditorNewDesign.this, rlMainView)
                                .intiLT((int) screenWidth / 2 - 60, (int) screenHeight / 2 - 60)
                                .viewWH(250, 250)
                                .scale(4f)
                                .alpha(16)
                                .color(android.R.color.transparent)
                                .build();
                        mv.setListner(touchList);
                        mv.startViewToRoot();
                    }
                }, 600);
            }
        });
        findViewById(R.id.viewDelete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // closeFragment();
                // rlBottomMoveDeleteDialog.setVisibility(View.GONE);
                closeAllEdiotors();
                if (isText) {
                    if (isBordered)
                        currentBorderText.setVisibility(View.GONE);
                    else
                        currentEdtSticker.setVisibility(View.GONE);
                } else {
                    // frameImagesBeans.get(k).isDeleted = true;
                    selectedImage.setVisibility(View.GONE);
                }
            }
        });
        // }
        /* void closeFragment () {
            if (behavior != null && behavior instanceof BottomSheetBehavior) {
                ((BottomSheetBehavior) behavior).setState(BottomSheetBehavior.STATE_HIDDEN);
            }
        } */
    }

    private Font getFont(FrameBean frameBean) {
        Font font = new Font();
        if (frameBean.color.length() > 0)
            font.setColor(parseColor(frameBean.color));
        return font;
    }

    double width;
    double height;
    float x, y;

    private void addApiNewText(FrameBean frameBean, Font font, FrameLayout mainFrame) {
        x = (parseFloat(frameBean.x) * screenWidth) / mainFrameWidth;
        y = (parseFloat(frameBean.y) * screenWidth) / mainFrameWidth;
        width = ((Double.parseDouble(frameBean.width) * screenWidth) / mainFrameWidth);
        height = ((Double.parseDouble(frameBean.height) * screenHeight) / mainFrameHeight);

        if (!(frameBean.border_color.length() > 0)) {
            TextView ed_myEdittext = CommanUtilsApp.getApiTextView(FrameEditorNewDesign.this,
                    frameBean, x, y, width, height, font, true);
            final GestureDetector gestureDetector = new GestureDetector(new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapConfirmed(MotionEvent e) {
                    // do something
                    closeAllEdiotors();
                    TextEditorDialogFragment textEditorDialogFragment = TextEditorDialogFragment.show(FrameEditorNewDesign.this,
                            ed_myEdittext.getText().toString(), true, frameBean.text);
                    textEditorDialogFragment.setOnTextEditorListener(new TextEditorDialogFragment.TextEditor() {
                        @Override
                        public void onDone(String inputText, int colorCode) {
                            ed_myEdittext.setText(inputText);
                            /* if (frameBean.text.length() < inputText.length()) {
                                Paint mPaint = new Paint();
                                mPaint.setTextSize(font.getSize());
                                mPaint.setTypeface(font.getTypeface());
                                ed_myEdittext.setTextSize((font.getSize() * mPaint.measureText(frameBean.text, 0, frameBean.text.length()))
                                        / mPaint.measureText(inputText, 0, inputText.length()));

                                        / inputText.length() + "");
                            } */
                        }

                        @Override
                        public void onCancel(String s) {
                        }
                    });
                    return true;
                }

                public void onLongPress(MotionEvent e) {
                    isPriceTagLongPressed = true;
                    viewPriceTag = ed_myEdittext;
                    clickedFrame = mainFrame;
                    openDeleteTextImage(true);
                }
            });
            // ed_myEdittext.setBackgroundColor(getResources().getColor(R.color.transparent_yellow));
            ed_myEdittext.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    gestureDetector.onTouchEvent(event);
                    return true;
                }
            });
            if (frameBean.rotation.length() > 0 && Integer.parseInt(frameBean.rotation) > 0) {
                ed_myEdittext.setX((float) (x - (height / 2) + (width / 2))); // + (width / 2));
                ed_myEdittext.setY((float) (y - (width / 2) + (height / 2)));
                ed_myEdittext.setRotation(Integer.parseInt(frameBean.rotation));
                ed_myEdittext.getLayoutParams().height = (int) width;
                ed_myEdittext.getLayoutParams().width = (int) height;
                ed_myEdittext.requestLayout();
            }
            if (frameBean.is_bt.equals("1")) {
                btImages.add(ed_myEdittext);
            }
            if (frameBean.is_wt.equals("1")) {
                wtImages.add(ed_myEdittext);
            }
            if (frameBean.is_pt.equals("1")) {
                ptImages.add(ed_myEdittext);
            }
            if (frameBean.is_st.equals("1")) {
                stImages.add(ed_myEdittext);
            }
            mainFrame.addView(ed_myEdittext);
            mainFrame.requestLayout();
        } else {
            BorderedTextView ed_myEdittext = CommanUtilsApp.getBorderedTextFromApi(FrameEditorNewDesign.this,
                    frameBean, x, y, width, height, font);
            final GestureDetector gestureDetector = new GestureDetector(new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapConfirmed(MotionEvent e) {
                    // do something
                    closeAllEdiotors();
                    TextEditorDialogFragment textEditorDialogFragment = TextEditorDialogFragment.show(FrameEditorNewDesign.this,
                            ed_myEdittext.getText().toString(), true, frameBean.text);
                    textEditorDialogFragment.setOnTextEditorListener(new TextEditorDialogFragment.TextEditor() {
                        @Override
                        public void onDone(String inputText, int colorCode) {
                            ed_myEdittext.setText(inputText);
                        }

                        @Override
                        public void onCancel(String s) {
                        }
                    });
                    return true;
                }

                public void onLongPress(MotionEvent e) {
                    isPriceTagLongPressed = true;
                    viewPriceTag = ed_myEdittext;
                    clickedFrame = mainFrame;
                    openDeleteTextImage(true);
                }
            });
            ed_myEdittext.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    gestureDetector.onTouchEvent(event);
                    return true;
                }
            });
            if (frameBean.rotation.length() > 0 && Integer.parseInt(frameBean.rotation) > 0) {
                ed_myEdittext.setX((float) (x - (height / 2) + (width / 2))); // + (width / 2));
                ed_myEdittext.setY((float) (y - (width / 2) + (height / 2)));
                ed_myEdittext.setRotation(Integer.parseInt(frameBean.rotation));
                ed_myEdittext.getLayoutParams().height = (int) width;
                ed_myEdittext.getLayoutParams().width = (int) height;
                ed_myEdittext.requestLayout();
            }
            if (frameBean.is_bt.equals("1")) {
                btImages.add(ed_myEdittext);
            }
            if (frameBean.is_wt.equals("1")) {
                wtImages.add(ed_myEdittext);
            }
            if (frameBean.is_pt.equals("1")) {
                ptImages.add(ed_myEdittext);
            }
            if (frameBean.is_st.equals("1")) {
                stImages.add(ed_myEdittext);
            }
            mainFrame.addView(ed_myEdittext);
            mainFrame.requestLayout();
        }
    }

    private void cropImage(String path, int wid, int hei) {
        /*int wid = clickedImageView.getWidth();
        int hei = clickedImageView.getHeight();*/
        CommanUtilsApp.enableStrictMode();
        UCrop.Options options = new UCrop.Options();
        options.setCompressionFormat(Bitmap.CompressFormat.JPEG);
        options.setCompressionQuality(100);
        options.setToolbarTitle("Crop");
        options.setHideBottomControls(true);
        options.setFreeStyleCropEnabled(false);

        UCrop.of(Uri.fromFile(new File(path)), Uri.fromFile(new File(getCacheDir(), System.currentTimeMillis() + ".png")))
                .withAspectRatio(wid, hei)
                .withMaxResultSize(2200, 2200)
                .withOptions(options)
                .start(FrameEditorNewDesign.this);
    }

    private void makeImageMovable(int k) {
        final float[] dX = new float[1];
        final float[] dY = new float[1];
        final int[] lastAction = new int[1];
        if (k == 0) {
            selectedImage.setOnTouchListener(null);
            selectedImage.setBackground(null);
        } else {
            selectedImage.setBackground(getResources().getDrawable(R.drawable.border_to_view));
            selectedImage.setOnTouchListener(new View.OnTouchListener() {

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getActionMasked()) {
                        case MotionEvent.ACTION_DOWN:
                            scroll.requestDisallowInterceptTouchEvent(true);
                            dX[0] = v.getX() - event.getRawX();
                            dY[0] = v.getY() - event.getRawY();
                            lastAction[0] = MotionEvent.ACTION_DOWN;
                            break;

                        case MotionEvent.ACTION_MOVE:
                            v.setY(event.getRawY() + dY[0]);
                            v.setX(event.getRawX() + dX[0]);
                            lastAction[0] = MotionEvent.ACTION_MOVE;
                            break;

                        case MotionEvent.ACTION_UP:
                            scroll.requestDisallowInterceptTouchEvent(false);
                            selectedImage.setOnTouchListener(null);
                            selectedImage.setBackground(null);
                            break;
                        default:
                            return false;
                    }
                    return true;
                }

            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, final int resultCode, Intent data) {
        if (PurchaseRequest == requestCode) {
            if (isPurchaseSuccess) {
                finish();
            }
        } else if (requestCode == 90 && data != null) {
            String s = data.getStringExtra(KEY_URL);
            int pos = data.getIntExtra(KEY_POS, 0);
            new SetNewArrival(s, pos).execute();
        } else if (requestCode == SelectLogoRequest && data != null) {
            String s = data.getStringExtra(KEY_PATH);
            if (!s.isEmpty()) {
                clickedRlLogo.removeAllViews();
                Bitmap decodeFile = BitmapFactory.decodeFile(new File(s).getAbsolutePath());
                float newWidth = ((decodeFile.getWidth() * 150f) / decodeFile.getHeight());
                float x = listShapeImageX.get(clickedFramePos) + ((decodeFile.getWidth()) / 2) - (decodeFile.getWidth() - newWidth) / 2 + 20;
                float y = listShapeImageY.get(clickedFramePos) + ((decodeFile.getHeight()) / 2) - (decodeFile.getHeight() - 150f) / 2 + 20;
                if (isFirst) {
                    isFirst = false;
                    setLogoInFrame(0, s, x, y, 150f, true);
                } else
                    setLogoInFrame(clickedFramePos, s, x, y, 150f, false);
            }
        } else if (SuggestionRequest == requestCode && data != null) {
            int primaryColor = data.getIntExtra(KEY_PRIMARY_COLOR, -1);
            int secondaryColor = data.getIntExtra(KEY_SECONDARY_COLOR, -1);
            ArrayList<FrameBean> frameBeans = (ArrayList<FrameBean>) data.getSerializableExtra(KEY_LIST);
            if (frameIdList.size() > suggestionClickPos)
                frameIdList.set(suggestionClickPos, data.getStringExtra(KEY_FRAME_ID));
            else
                frameIdList.add(data.getStringExtra(KEY_FRAME_ID));
            allDataList.set(suggestionClickPos, frameBeans);
            if (suggestionClickPos == 0)
                responseDataListBean = frameBeans;
            suggestionFrame.removeAllViews();
            hueImages = new ArrayList<>();
            btImages = new ArrayList<>();
            wtImages = new ArrayList<>();
            ptImages = new ArrayList<>();
            stImages = new ArrayList<>();

            RelativeLayout relativeLayout = new RelativeLayout(FrameEditorNewDesign.this);
            relativeLayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            relativeLayout.setLayerPaint(setPaint());

            for (int j = 0; j < allDataList.get(suggestionClickPos).size(); j++) {
                suggetionRlLogo.removeAllViews();
                setFrameData(suggestionImgWaterMark, suggestionFrameView, relativeLayout, suggetionRlLogo, suggestionFrame, j, suggestionClickPos, suggestionPrimaryRv, suggestionSecondaryRv,
                        suggestionTextGroup1, suggestionTextGroup2, false);
            }
            suggestionFrame.addView(relativeLayout);

            blackToneList.set(suggestionClickPos, btImages);
            whiteToneList.set(suggestionClickPos, wtImages);
            secondaryToneList.set(suggestionClickPos, stImages);
            primaryToneList.set(suggestionClickPos, ptImages);
            hueImagesList.set(suggestionClickPos, hueImages);

            rlHueSeekbar.setVisibility(hueImages.size() > 0 ? View.VISIBLE : View.GONE);

            if (primaryColor != -1)
                changesPrimarySecondaryColor(true, suggestionClickPos, primaryColor);
            if (secondaryColor != -1)
                changesPrimarySecondaryColor(false, suggestionClickPos, secondaryColor);
            /* for (int j = 0; j < secondaryToneList.get(suggestionClickPos).size(); j++) {
                // secondaryToneList.get(suggestionClickPos).get(j).setColorFilter((int) secondaryColor);
                if (secondaryToneList.get(clickedFramePos).get(j) instanceof ImageView)
                    ((ImageView) secondaryToneList.get(clickedFramePos).get(j)).setColorFilter((int) primaryColor);
                else
                    ((TextView) secondaryToneList.get(clickedFramePos).get(j)).setTextColor((int) primaryColor);
            } */
            llList.requestLayout();
        } else {
            // addedProductSize = productImages.size();
            if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
                Image image = ImagePicker.getFirstImageOrNull(data);

                    clickedImageView.setTag(image.getPath());
                    if (isMask) {
                        maskImageUrl = clickedImageUrl;
                        cropImage(image.getPath(), clickedImageView.getWidth(), clickedImageView.getHeight());
                        CommanUtilsApp.enableStrictMode();
                       setColor(image.getPath(),  0, true);
                    } else {
                        Bitmap bitmap = CommanUtilsApp.modifyOrientation(image.getPath());
                        clickedImageView.setImageBitmap(bitmap);
                        clickedImageView.setBackgroundColor(getResources().getColor(android.R.color.transparent));
                        setColor(image.getPath(), 0, true);
                    }


            }
            if (resultCode == RESULT_OK && requestCode == UCrop.REQUEST_CROP) {
                final Uri resultUri = UCrop.getOutput(data);

                clickedImageView.setImageURI(resultUri);
                setColor(resultUri.getPath(),  0, true);
                if (maskImageUrl.length() > 0)
                    try {
                        CommanUtilsApp.strictModePermitAll();
                        URL url = new URL(maskImageUrl);
                        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                        connection.setDoInput(true);
                        connection.connect();
                        InputStream input = connection.getInputStream();
                        makeMaskImage(BitmapFactory.decodeStream(input), MediaStore.Images.Media.getBitmap(getContentResolver(),
                                Uri.fromFile(new File(resultUri.getPath()))), clickedImageView);
                        maskImageUrl = "";
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                else {
                    clickedImageView.setImageURI(resultUri);
                    CommanUtilsApp.enableStrictMode();
                    setColor(instaImage,  0, true);
                }
            } else if (requestCode == REQUEST_CROP && data != null) {
                final Uri resultUri = Uri.parse(data.getStringExtra("uri"));
                if (mask != null)
                    try {
                        CommanUtilsApp.enableStrictMode();
                        Bitmap btmp = MediaStore.Images.Media.getBitmap(getContentResolver(), Uri.fromFile(new File(resultUri.getPath())));
                        makeMaskImage(mask, btmp, clickedImageView);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                else {
                    clickedImageView.setImageURI(resultUri);
                    CommanUtilsApp.enableStrictMode();
                    // setColor(resultUri.getPath(), productImages.size() > clickedFramePos ? clickedFramePos : 0, true);
                }
            } else if (requestCode == 1234) {
                byte[] byteArray = getIntent().getByteArrayExtra(KEY_IMAGE);
                CommanUtilsApp.strictModePermitAll();
                // makeMaskImage(BitmapFactory.decodeFile(image.getPath()));
                clickedImageView.setImageBitmap(maskingImage(BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length)
                        , clickedImageUrl));
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void changesPrimarySecondaryColor(boolean isPrimary, int pos, int color) {
        if (isPrimary)
            for (int j = 0; j < primaryToneList.get(pos).size(); j++) {
                if (primaryToneList.get(pos).get(j) instanceof ImageView)
                    ((ImageView) primaryToneList.get(pos).get(j)).setColorFilter(color);
                else if (primaryToneList.get(pos).get(j) instanceof BorderedTextView)
                    ((BorderedTextView) primaryToneList.get(pos).get(j)).setBorderedColor(color);
                else
                    ((TextView) primaryToneList.get(pos).get(j)).setTextColor(color);
            }
        else
            for (int j = 0; j < secondaryToneList.get(pos).size(); j++) {
                if (secondaryToneList.get(pos).get(j) instanceof ImageView)
                    ((ImageView) secondaryToneList.get(pos).get(j)).setColorFilter(color);
                else if (secondaryToneList.get(pos).get(j) instanceof BorderedTextView)
                    ((BorderedTextView) secondaryToneList.get(pos).get(j)).setBorderedColor(color);
                else
                    ((TextView) secondaryToneList.get(pos).get(j)).setTextColor(color);
            }
    }

    private Paint setPaint() {
        Paint paint = new Paint();
        paint.setColor(RED);
        paint.setAntiAlias(true);
        paint.setDither(true);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(3);
        paint.setPathEffect(new DashPathEffect(new float[]{10, 15}, 0));
        return paint;
    }

    @Override
    public void primarySecondaryTonePos(boolean isPicker, boolean isPt, int i, int color) {
        closeAllEdiotors();
        if (!isSetSctickers) {
            isPrimaryTone = false;
            isSecondaryTone = false;
            isTextColor = false;
            if (isPicker) {
                if (isPt)
                    isPrimaryTone = true;
                else isSecondaryTone = true;
                clickedFramePos = i;
                // clickedImageView.setBackground(getResources().getDrawable(R.drawable.rounded_border_to_view));
                toggle(true);
                CustomBottomColorChangeDialog(false);
            } else if (isPt) {
                changesPrimarySecondaryColor(true, i, color);
            } else {
                changesPrimarySecondaryColor(false, i, color);
            }
        } else isSetSctickers = false;
    }

    int[][] pixelsMatrix;
    int[] calculatedPalette;
    ArrayList<Integer> colorsList;

    private void setColor(String image, int i, boolean isSetNewImage) {


            Bitmap bitmap = getBitmapFromURI(FrameEditorNewDesign.this, Uri.fromFile(new File(image)));
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, new ByteArrayOutputStream());


            pixelsMatrix = getPixelsMatrixFromBitmap(bitmap);
            calculatedPalette = Quantize.quantizeImage(pixelsMatrix, 25);

            colorsList = new ArrayList<>();
            for (int c : calculatedPalette)
                colorsList.add(c);
            colorsList.add(Color.WHITE);
            colorsList.add(Color.BLACK);
            colorsList.add(null);
            if (isSetNewImage) {
                if (colorsArrays.size() > clickedFramePos)
                    colorsArrays.set(clickedFramePos, colorsList);
                else colorsArrays.add(colorsList);
                clickedSecondaryRv.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        clickedSecondaryRv.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        clickedSecondaryRv.getLayoutParams().height = clickedSecondaryRv.getWidth();
                        clickedSecondaryRv.requestLayout();
                        clickedSecondaryRv.setAdapter(new PrimarySecondaryColorListAdapter(FrameEditorNewDesign.this, colorsList,
                                clickedFramePos, primarySecondaryTonePos, false, clickedSecondaryRv.getWidth()));
                    }
                });
                clickedPrimaryRv.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        clickedPrimaryRv.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        clickedPrimaryRv.getLayoutParams().height = clickedPrimaryRv.getWidth();
                        clickedPrimaryRv.requestLayout();
                        clickedPrimaryRv.setAdapter(new PrimarySecondaryColorListAdapter(FrameEditorNewDesign.this,
                                colorsList, clickedFramePos, primarySecondaryTonePos, true, clickedPrimaryRv.getWidth()));
                    }
                });
            } else {
                colorsArrays.add(colorsList);
                i++;
            /*if (productImages != null && productImages.size() > i) {
                CommanUtilsApp.enableStrictMode();
                setColor(productImages.get(i), i, false);
            }*/
            }



    }
  /*  private void setColor(String path, int i, boolean isSetNewImage) {
        Bitmap bitmap = getBitmapFromURI(FrameEditorNewDesign.this, Uri.fromFile(new File(path)));
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, new ByteArrayOutputStream());

        pixelsMatrix = getPixelsMatrixFromBitmap(bitmap);
        calculatedPalette = Quantize.quantizeImage(pixelsMatrix, 25);

        colorsList = new ArrayList<>();
        for (int c : calculatedPalette)
            colorsList.add(c);
        colorsList.add(Color.WHITE);
        colorsList.add(Color.BLACK);
        colorsList.add(null);
        if (isSetNewImage) {
            if (colorsArrays.size() > clickedFramePos)
                colorsArrays.set(clickedFramePos, colorsList);
            else colorsArrays.add(colorsList);
            clickedSecondaryRv.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    clickedSecondaryRv.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    clickedSecondaryRv.getLayoutParams().height = clickedSecondaryRv.getWidth();
                    clickedSecondaryRv.requestLayout();
                    clickedSecondaryRv.setAdapter(new PrimarySecondaryColorListAdapter(FrameEditorNewDesign.this, colorsList,
                            clickedFramePos, primarySecondaryTonePos, false, clickedSecondaryRv.getWidth()));
                }
            });
            clickedPrimaryRv.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    clickedPrimaryRv.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    clickedPrimaryRv.getLayoutParams().height = clickedPrimaryRv.getWidth();
                    clickedPrimaryRv.requestLayout();
                    clickedPrimaryRv.setAdapter(new PrimarySecondaryColorListAdapter(FrameEditorNewDesign.this,
                            colorsList, clickedFramePos, primarySecondaryTonePos, true, clickedPrimaryRv.getWidth()));
                }
            });
        } else {
            colorsArrays.add(colorsList);
            i++;
            *//*if (productImages != null && productImages.size() > i) {
                CommanUtilsApp.enableStrictMode();
                setColor(productImages.get(i), i, false);
            }*//*
        }
    }*/

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        setResult(30, intent);
        closeAllEdiotors();
        finish();
    }

    @Override
    protected void onDestroy() {
        llList.removeAllViews();
        llList.requestLayout();
        if (commanUtilsApp != null)
            commanUtilsApp.hideProgressDialog();
        super.onDestroy();
    }

    private Bitmap maskingImage(Bitmap s, String urls) {
        Bitmap original = s;
        try {
            CommanUtilsApp.strictModePermitAll();

            URL url = new URL(urls);
            Bitmap mask = decodeStream(url.openConnection().getInputStream());
            Bitmap result = Bitmap.createBitmap(original.getWidth(), original.getHeight(), Bitmap.Config.ARGB_8888);

            Canvas mCanvas = new Canvas(result);
            Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
            mCanvas.drawBitmap(original, 0, 0, null);
            mCanvas.drawBitmap(mask, 0, 0, paint);
            paint.setXfermode(null);
            return result;
        } catch (IOException e) {
            System.out.println(e);
        }
        return null;
    }

    @OnClick({R.id.rlBottomMoveDeleteDialog, R.id.llScroll, R.id.rlTop, R.id.txtDownloadAll, R.id.llBack, R.id.llCamera, R.id.llGallery})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rlBottomMoveDeleteDialog:
                closeAllEdiotors();
                break;
            case R.id.llScroll:
                closeAllEdiotors();
                break;
            case R.id.rlTop:
                closeAllEdiotors();
                break;
            case R.id.txtDownloadAll:
                isDownloadAll = true;
                closeAllEdiotors();
                new DownloadImage().execute();
                break;
            case R.id.llBack:
                finish();
                break;
            case R.id.llCamera:
                // if (isPurchase(FrameEditorNewDesign.this, true)) {
                // addedProductSize = productImages.size();
                isAddMoreProduct = true;
                /* for (int i = 0; i < responseDataListBean.size(); i++) {
                        if (responseDataListBean.get(i).is_mask.equals("1")) {
                            isMask = true;
                            break;
                        }
                    } */
                ImagePicker.cameraOnly().start(FrameEditorNewDesign.this);
                /* startActivityForResult(new Intent(FrameEditorNewDesign.this, TakePhoteActivity.class)
                                    .putExtra(TakePhoteActivity.EXTRA_SELECTION_LIMIT, isMask ? 1 : 15)
                            , INTENT_REQUEST_GET_N_IMAGES); */
                // }
                break;
            case R.id.llGallery:
                // if (isPurchase(FrameEditorNewDesign.this, true)) {
                // addedProductSize = productImages.size();
                isAddMoreProduct = true;
                boolean isMask = false;
                for (int i = 0; i < responseDataListBean.size(); i++) {
                    if (responseDataListBean.get(i).is_mask.equals("1")) {
                        isMask = true;
                        break;
                    }
                }
                ImagePicker.create(FrameEditorNewDesign.this)
                        .folderMode(true)
                        .limit(isMask ? 1 : 20)
                        .showCamera(false)
                        .start();
                // }
                break;
        }
    }

    public static class AddImageDialogFragment extends BottomSheetDialogFragment {
        CoordinatorLayout.Behavior behavior;

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
        }

        @SuppressLint("RestrictedApi")
        @Override
        public void setupDialog(Dialog dialog, int style) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(TRANSPARENT));
            dialog.getWindow().setDimAmount(0.0f);
            super.setupDialog(dialog, style);
            View contentView = View.inflate(getContext(), R.layout.dialog_select_image, null);
            dialog.setContentView(contentView);
            CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) ((View) contentView.getParent()).getLayoutParams();
            behavior = layoutParams.getBehavior();
            if (behavior != null && behavior instanceof BottomSheetBehavior) {
                ((BottomSheetBehavior) behavior).setState(BottomSheetBehavior.STATE_EXPANDED);
            }
            dialog.findViewById(R.id.llCamera).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    closeFragment();
                    isLogo = false;
                    ImagePicker.cameraOnly().start(getActivity());
                }
            });
            dialog.findViewById(R.id.llGallery).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    closeFragment();
                    isLogo = false;
                    ImagePicker.create(getActivity())
                            .folderMode(true)
                            .returnMode(ReturnMode.GALLERY_ONLY)
                            .single()
                            .showCamera(false)
                            .start();
                }
            });
        }

        void closeFragment() {
            if (behavior != null && behavior instanceof BottomSheetBehavior) {
                ((BottomSheetBehavior) behavior).setState(BottomSheetBehavior.STATE_HIDDEN);
            }
        }
    }

    public class ColorListAdapter extends RecyclerView.Adapter<ColorListAdapter.ViewHolder> {
        Activity activity;
        String[] colors;

        public ColorListAdapter(Activity activity, String[] colors) {
            this.activity = activity;
            this.colors = colors;
        }

        @Override
        public ColorListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_color_list_item, parent, false);
            return new ColorListAdapter.ViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final ColorListAdapter.ViewHolder holder, int position) {
            holder.card.setCardBackgroundColor(parseColor(colors[position]));
            if (colors[position].contains("FFFFFF")) {
                holder.llbg.setBackground(activity.getResources().getDrawable(R.drawable.rounded_corner_black_border));
            } else
                holder.llbg.setBackground(null);
            holder.card.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    changeColors(parseColor(colors[position]));
                }
            });
        }

        @Override
        public int getItemCount() {
            return colors.length;
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            @BindView(R.id.card)
            CardView card;
            @BindView(R.id.llbg)
            LinearLayout llbg;

            public ViewHolder(View view) {
                super(view);
                ButterKnife.bind(this, view);
            }
        }
    }

    private void toggle(boolean show) {
        if (!show) {
            /*isWhiteTextGroupClick = false;
            isBlackTextGroupClick = false;*/
            if (currentEdtSticker != null)
                currentEdtSticker.setBackground(null);
            if (clickedImageView != null)
                clickedImageView.setBackground(null);
        } else if (findViewById(R.id.llTextEditor).isShown())
            openTextEditor(false);
        View redLayout = findViewById(R.id.rlBottomMoveDeleteDialog);

        ViewGroup parent = findViewById(R.id.rlMainView);

        Transition transition = new Slide(Gravity.BOTTOM);
        transition.setDuration(500);
        transition.addTarget(R.id.rlBottomMoveDeleteDialog);

        TransitionManager.beginDelayedTransition(parent, transition);
        redLayout.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    //=================  Add Price Tag Data ===================//
    boolean isPriceTagLongPressed = false;
    View viewPriceTag;
    float dXcs;
    float dYcs;

    private void AddPriceTagPriceTagSticker() {
        FrameLayout mainFrame = mainFrameList.get(clickedFramePos);
        View viewScticker = LayoutInflater.from(FrameEditorNewDesign.this).inflate(R.layout.custom_price_tag_sticker, null);
        viewScticker.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        TextView txtPrice = viewScticker.findViewById(R.id.txtPrice);

        txtPrice.setBackground(null);
        viewScticker.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                viewScticker.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                if (listShapeImageX.size() > clickedFramePos) {
                    viewScticker.setX(listShapeImageX.get(clickedFramePos));
                    viewScticker.setY(listShapeImageY.get(clickedFramePos) + listShapeImageHeight.get(clickedFramePos) - viewScticker.getHeight());
                }
            }
        });

        final GestureDetector gestureDetector = new GestureDetector(new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onDoubleTap(MotionEvent e) {
                currentEdtSticker = txtPrice;
                isText = true;
                openPriceTagEditor(mainFrame, viewScticker, txtPrice);
                return super.onDoubleTap(e);
            }

            @Override
            public boolean onSingleTapConfirmed(MotionEvent e) {
                // do something
                currentEdtSticker = txtPrice;
                if (txtPrice.getBackground() == null)
                    txtPrice.setBackground(getResources().getDrawable(R.drawable.rounded_border_to_view));
                else
                    txtPrice.setBackground(null);
                return true;
            }

            public void onLongPress(MotionEvent e) {
                isPriceTagLongPressed = true;
                viewPriceTag = viewScticker;
                clickedFrame = mainFrame;
                openDeleteTextImage(true);
            }
        });

        viewScticker.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (gestureDetector.onTouchEvent(event)) {
                    return true;
                } else if (!isPriceTagLongPressed) {
                    if (txtPrice.getBackground() != null)
                        switch (event.getActionMasked()) {
                            case MotionEvent.ACTION_DOWN:
                                scroll.requestDisallowInterceptTouchEvent(true);
                                dXcs = v.getX() - event.getRawX();
                                dYcs = v.getY() - event.getRawY();
                                break;
                            case MotionEvent.ACTION_MOVE:
                                scroll.requestDisallowInterceptTouchEvent(true);
                                v.setY(event.getRawY() + dYcs);
                                v.setX(event.getRawX() + dXcs);
                                break;
                            default:
                                return false;
                        }
                } else {
                    isPriceTagLongPressed = false;
                    return false;
                }
                return true;
            }
        });
        // testStickerView.addView(viewScticker);
        mainFrame.addView(viewScticker);
        mainFrame.requestLayout();

        if (currentEdtSticker != null && currentEdtSticker != txtPrice)
            currentEdtSticker.setBackground(getResources().getDrawable(R.drawable.rounded_corner_white));

        openPriceTagEditor(mainFrame, viewScticker, txtPrice);
        /* BottomSheetDialogFragment dialogFragment1 = new CustomEditStickerDialogFragment();
        dialogFragment1.show(getSupportFragmentManager(), dialogFragment1.getTag()); */
    }

    private void openPriceTagEditor(FrameLayout mainFrame, View viewScticker, TextView txtPrice) {
        closeAllEdiotors();
        if (!isSetSctickers) {
            TextEditorDialogFragment textEditorDialogFragment = TextEditorDialogFragment.showPriceTagEditor(FrameEditorNewDesign.this,
                    txtPrice.getText().toString(), false, "");
            textEditorDialogFragment.setOnTextEditorListener(new TextEditorDialogFragment.TextEditor() {
                @Override
                public void onDone(String inputText, int colorCode) {
                    // txtPrice.setBackground(getResources().getDrawable(R.drawable.rounded_corner_white));
                    txtPrice.setBackground(null);
                    if (inputText.length() == 0) {
                        mainFrame.removeView(viewScticker);
                        mainFrame.requestLayout();
                    } else
                        txtPrice.setText(inputText);
                }

                @Override
                public void onCancel(String inputText) {
                    // txtPrice.setBackground(getResources().getDrawable(R.drawable.rounded_corner_white));
                    txtPrice.setBackground(null);
                    if (inputText.length() == 0) {
                        mainFrame.removeView(viewScticker);
                        mainFrame.requestLayout();
                    } else
                        txtPrice.setText(inputText);
                }
            });
        } else isSetSctickers = false;
    }

    //=================  Download Image ===================//
    public class DownloadImage extends AsyncTask<String, Boolean, File> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            commanUtilsApp = new CommanUtilsApp(FrameEditorNewDesign.this);
            commanUtilsApp.createProgressDialog(getResources().getString(R.string.downloading));
            commanUtilsApp.showProgressDialog();
        }

        @Override
        protected File doInBackground(String... URL) {
            List<String> ids = getMyPostIdList(FrameEditorNewDesign.this);
            for (int i = 0; i < frameIdList.size(); i++) {
                if (!ids.contains(frameIdList.get(i)))
                    ids.add(frameIdList.get(i));
            }
            CommanUtilsApp.saveMyPostIdList(ids, FrameEditorNewDesign.this);
            saveImage(0);
            return new File("");
        }

        private void saveImage(int i) {
            /* commanUtilsApp.setProgressDialogMessage(getResources().getString(R.string.downloading)
                    + "(" + (i + 1) + "/" + rlMainFrameViewList.size() + ")"); */
            File file = null;
            try {
                Bitmap generatedBitmap = loadBitmapFromView(rlMainFrameViewList.get(i));
                file = getNewFile();
                if (file != null) {
                    FileOutputStream output = new FileOutputStream(file.getAbsolutePath());
                    generatedBitmap.compress(Bitmap.CompressFormat.PNG, 100, output);
                    output.close();
                    i++;
                    CommanUtilsApp.scanDownloadedFile(FrameEditorNewDesign.this, file);
                    if (rlMainFrameViewList.size() > i) {
                        saveImage(i);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected void onPostExecute(File file) {
            isDownloadAll = false;
            commanUtilsApp.hideProgressDialog();
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.download_sucess), Toast.LENGTH_LONG).show();
        }
    }

    public File getNewFile() {
        String downloadFolderPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
                .getAbsolutePath() + File.separator + "SociallySaveImage";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US);
        String timeStamp = simpleDateFormat.format(new Date());
        String path;
        File file = new File(downloadFolderPath);
        if (!file.exists())
            file.mkdirs();
        path = file + File.separator + timeStamp + ".jpg";
        if (TextUtils.isEmpty(path)) {
            return null;
        }
        return new File(path);
    }

    public File getNewHidenFoler(String s) {
        String downloadFolderPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath() + File.separator + ".Socially";
        String path;
        File file = new File(downloadFolderPath);
        if (!file.exists())
            file.mkdirs();
        path = file + File.separator + s;
        if (TextUtils.isEmpty(path)) {
            return null;
        }
        return new File(path);
    }

    public Bitmap loadBitmapFromView(View view) {
        boolean tooBig = (view.getHeight() > 1000) || (view.getWidth() > 1000);

        int height = view.getHeight() * (tooBig ? 2 : 3);
        int width = view.getWidth() * (tooBig ? 2 : 3);
        view.measure(View.MeasureSpec.makeMeasureSpec(width, View.MeasureSpec.EXACTLY),
                View.MeasureSpec.makeMeasureSpec(height, View.MeasureSpec.EXACTLY));
        Bitmap bitmap = null;
        try {
            bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            canvas.translate(0.0f, 0.0f);
            canvas.drawColor(-1, PorterDuff.Mode.CLEAR);
            canvas.scale(width / view.getWidth(), height / view.getHeight());
            view.draw(canvas);
            return bitmap;
        } catch (Exception e) {
            e.printStackTrace();
            return bitmap;
        }
    }

    //================= Add Same Logo In All Frames ===================//
    ImageStickerViewNew selectedImageStickerViewNew, firstFrameLogo;
    ImageStickerViewNew.GetNewPos getNewPos;
    boolean isSetSctickers = false;

    @Override
    public void getNewData(float x, float y, Bitmap bitmap, String tag) {
        isSetSctickers = true;
        if (rlLogoList.size() > 1) {
            openDialog((x - ((bitmap.getWidth() - selectedImageStickerViewNew.getWidths()) / 2)),
                    (y - ((bitmap.getHeight() - selectedImageStickerViewNew.getHeights()) / 2)));
        }
    }

    Dialog dialog;

    public void openDialog(float xx, float yy) {
        dialog = new Dialog(FrameEditorNewDesign.this, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.setContentView(R.layout.dialog_apply_logo_conformation);

        dialog.findViewById(R.id.txtOnlythis).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.findViewById(R.id.txtAll).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                removeAllStickers();
                setLogoInFrame(0, selectedImageStickerViewNew.getStickerPath(),
                        xx, yy, selectedImageStickerViewNew.getHeights(), true);
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void setLogoInFrame(int framePos, String imgPath, float x, float y, float height,
                               boolean isSetInAllFrame) {
        File file = new File(imgPath);
        if (file.exists()) {
            try {
                Bitmap decodeFile = BitmapFactory.decodeFile(file.getAbsolutePath());
                ImageStickerViewNew imageSticker = new ImageStickerViewNew(FrameEditorNewDesign.this, imgPath,
                        x, y, 1.0f, 0.0f, 2, true);
                imageSticker.setGif(false);
                imageSticker.setBitmap(decodeFile, true);
                imageSticker.setOperationListener(new ImageStickerViewNew.OperationListener() {
                    public void onDeleteClick(ImageStickerViewNew imageStickerView) {
                        rlLogoList.get(framePos).removeView(selectedImageStickerViewNew);
                    }

                    public void onEdit(ImageStickerViewNew imageStickerView) {
                        if (selectedImageStickerViewNew != null) {
                            selectedImageStickerViewNew.setInEdit(false);
                        }
                        selectedImageStickerViewNew = imageStickerView;
                        if (framePos == 0)
                            firstFrameLogo = imageSticker;
                        selectedImageStickerViewNew.setInEdit(true);
                    }

                    public void onTop(ImageStickerViewNew imageStickerView) {
                    }

                    @Override
                    public void onSelect(Boolean isClick) {
                        scroll.requestDisallowInterceptTouchEvent(isClick);
                        if (isClick) {
                            selectedImageStickerViewNew = imageSticker;
                            if (framePos == 0)
                                firstFrameLogo = imageSticker;
                        }
                    }
                });
                imageSticker.setInEdit(false);
                imageSticker.setSize(height);
                // imageSticker.setSizeFirst(height, x, y);
                imageSticker.setListner(getNewPos, "");
                for (int j = 0; j < rlLogoList.get(framePos).getChildCount(); j++) {
                    if (rlLogoList.get(framePos).getChildAt(j) instanceof ImageStickerViewNew) {
                        rlLogoList.get(framePos).removeViewAt(j);
                    }
                }
                rlLogoList.get(framePos).addView(imageSticker);
                rlLogoList.get(framePos).requestLayout();
                if (isSetInAllFrame && rlLogoList.size() > framePos + 1)
                    setLogoInFrame(framePos + 1, imgPath, x, y, height, isSetInAllFrame);
            } catch (OutOfMemoryError unused) {
                unused.printStackTrace();
            }
        }
    }

    private void removeAllStickers() {
        if (!isDownloadAll)
            for (int i = 0; i < rlLogoList.size(); i++) {
                for (int j = 0; j < rlLogoList.get(i).getChildCount(); j++) {
                    if (rlLogoList.get(i).getChildAt(j) instanceof ImageStickerViewNew) {
                        rlLogoList.get(i).removeViewAt(j);
                    }
                }
                rlLogoList.get(i).requestLayout();
            }
    }

    public class SingleDoubleTapListner implements View.OnTouchListener {
        private GestureDetector gestureDetector;
        private int index;
        ImageView photoView;
        Activity activity;
        FrameBean frameBean;
        RecyclerView rvPrimaryColor;
        RecyclerView rvSecondaryColor;

        public SingleDoubleTapListner(Activity activity, ImageView photoView, int index,
                                      FrameBean frameBean, RecyclerView rvPrimaryColor, RecyclerView rvSecondaryColor) {
            this.rvPrimaryColor = rvPrimaryColor;
            this.rvSecondaryColor = rvSecondaryColor;
            this.photoView = photoView;
            this.activity = activity;
            this.index = index;
            this.frameBean = frameBean;
            gestureDetector = new GestureDetector(activity, new SingleTapConfirm(frameBean));
        }

        @Override
        public boolean onTouch(View view, MotionEvent event) {
            if (gestureDetector.onTouchEvent(event)) {
                return true;
            } else {
                return true;
            }
        }

        private class SingleTapConfirm extends GestureDetector.SimpleOnGestureListener {
            FrameBean frameBean;

            public SingleTapConfirm(FrameBean frameBean) {
                this.frameBean = frameBean;
            }

            @Override
            public boolean onSingleTapConfirmed(MotionEvent e) {
                if (isEditTextClicked) {
                    isEditTextClicked = false;
                } else {
                    closeAllEdiotors();
                    if (!isSetSctickers) {
                        clickedImageUrl = frameBean.src;
                        clickedImageView = photoView;
                        clickedPrimaryRv = rvPrimaryColor;
                        clickedSecondaryRv = rvSecondaryColor;
                        clickedFramePos = index;
                        if (frameBean.is_mask.equalsIgnoreCase("1")) {
                            isMask = true;
                            isRemoveBg = false;
                            BottomSheetDialogFragment bottomSheetDialogFragment = new AddImageDialogFragment();
                            bottomSheetDialogFragment.show(getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
                        } else {
                            isMask = false;
                            isRemoveBg = false;
                            BottomSheetDialogFragment bottomSheetDialogFragment = new AddImageDialogFragment();
                            bottomSheetDialogFragment.show(getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
                        }
                    } else isSetSctickers = false;
                }
                return false;
            }

            @Override
            public boolean onDoubleTap(MotionEvent e) {
                if (isEditTextClicked) {
                    isEditTextClicked = false;
                } else {
                    closeAllEdiotors();
                    clickedImageUrl = frameBean.src;
                    clickedImageView = photoView;
                    clickedPrimaryRv = rvPrimaryColor;
                    clickedSecondaryRv = rvSecondaryColor;
                    clickedFramePos = index;
                    maskImageUrl = frameBean.is_mask.equalsIgnoreCase("1") ? frameBean.src : "";
                    if (clickedImageView.getTag() != null)
                        cropImage(clickedImageView.getTag().toString(), clickedImageView.getWidth(), clickedImageView.getHeight());
                }
                return false;
            }

            @Override
            public boolean onDoubleTapEvent(MotionEvent e) {
                return false;
            }
        }
    }

    //===========-------- Add Text Setup --------===========
    @BindView(R.id.llTextEditor)
    LinearLayout llTextEditor;
    @BindView(R.id.imgFont)
    ImageView imgFont;
    @BindView(R.id.imgColor)
    ImageView imgColor;
    @BindView(R.id.imgAlign)
    ImageView imgAlign;
    @BindView(R.id.imgSpace)
    ImageView imgSpace;
    @BindView(R.id.imgLine)
    ImageView imgLine;
    @BindView(R.id.imgCaps)
    ImageView imgCaps;
    @BindView(R.id.listFont)
    RecyclerView listFont;
    @BindView(R.id.llFonts)
    LinearLayout llFonts;
    @BindView(R.id.listColor)
    RecyclerView listColor;
    @BindView(R.id.llColors)
    LinearLayout llColors;
    @BindView(R.id.imgAlignLeft)
    ImageView imgAlignLeft;
    @BindView(R.id.imgAlignCenter)
    ImageView imgAlignCenter;
    @BindView(R.id.imgAlignRight)
    ImageView imgAlignRight;
    @BindView(R.id.llAlign)
    LinearLayout llAlign;
    @BindView(R.id.imgFirstCap)
    ImageView imgFirstCap;
    @BindView(R.id.imgSmall)
    ImageView imgSmall;
    @BindView(R.id.imgAllCap)
    ImageView imgAllCap;
    @BindView(R.id.llCaps)
    LinearLayout llCaps;
    @BindView(R.id.seekBarFontSpacing)
    SeekBar seekBarFontSpacing;
    @BindView(R.id.llSpacing)
    LinearLayout llSpacing;
    @BindView(R.id.seekBarFontHeight)
    SeekBar seekBarFontHeight;
    @BindView(R.id.llLine)
    LinearLayout llLine;

    FontListAdapter fontListAdapter;
    // ColorListAdapter colorListAdapter;
    com.example.iosa.texteditor.ColorListAdapter colorListAdapter;
    int selectedColorPos = -1, selectedFontPos = -1;
    ArrayList<TextEditorDataBean> textEditorDataBeans = new ArrayList<>();
    TextStickerViewNew1 currentTextSticker;
    boolean isEditTextClicked = false;

    private void setUpTextEditor() {
        imgFont.setColorFilter(Color.BLACK);
        llFonts.bringToFront();

        fontListAdapter = new FontListAdapter(FrameEditorNewDesign.this,
                fonts, this::fontClick, selectedFontPos);
        listFont.setAdapter(fontListAdapter);

        colorListAdapter = new com.example.iosa.texteditor.ColorListAdapter(FrameEditorNewDesign.this,
                colors, this::colorClick, selectedColorPos);
        listColor.setAdapter(colorListAdapter);

        seekBarFontSpacing.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    currentTextSticker.setLetterSpacing(progress);
                    currentTextSticker.requestLayout();
                    TextEditorDataBean dataBean = textEditorDataBeans.get((Integer) currentTextSticker.getTag());
                    dataBean.spacing = progress;
                    textEditorDataBeans.set((Integer) currentTextSticker.getTag(), dataBean);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
        seekBarFontHeight.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    currentTextSticker.setLineSpacing(progress);
                    currentTextSticker.requestLayout();
                    TextEditorDataBean dataBean = textEditorDataBeans.get((Integer) currentTextSticker.getTag());
                    dataBean.lineHeight = progress + 5;
                    textEditorDataBeans.set((Integer) currentTextSticker.getTag(), dataBean);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
    }

    private void openTextEditor(boolean show) {
        if (show) {
            changeItemSelection(imgFont);

            TextEditorDataBean dataBean = textEditorDataBeans.get((Integer) currentTextSticker.getTag());
            selectedColorPos = dataBean.color;
            selectedFontPos = dataBean.font;

            seekBarFontSpacing.setProgress(dataBean.spacing);
            seekBarFontHeight.setProgress(dataBean.lineHeight);

            colorListAdapter.setPosition(selectedColorPos);
            fontListAdapter.setPosition(selectedFontPos);

            changeAlignMent(dataBean.align);
            changeCaps(dataBean.caps);
        }

        View redLayout = findViewById(R.id.llTextEditor);
        ViewGroup parent = findViewById(R.id.rlMainView);

        Transition transition = new Slide(Gravity.BOTTOM);
        transition.setDuration(500);
        transition.addTarget(R.id.llTextEditor);

        TransitionManager.beginDelayedTransition(parent, transition);
        redLayout.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    private void openVideo() {
        for (int i = 0; i < tutorialList.size(); i++) {
            if (FastSave.getInstance().getString(CAT_ID, "").equals(tutorialList.get(i).category_id)) {
               /* startActivity(new Intent(FrameEditorNewDesign.this, VideoPlayerActivity.class).putExtra(KEY_URL,
                        tutorialList.get(i).video));*/
                break;
            }
        }
    }

    void getTutorialList() {
        HashMap hashMap = new HashMap<>();
        hashMap.put(UNIQUE_KEY, UNIQUE_KEY_VALUE);
        if (CommanUtilsApp.isNetworkAvailable(FrameEditorNewDesign.this))
            apiCall.GetData(Request.Method.POST, GET_TUTORIAL_LIST, hashMap, tutorial_list_request);
    }

    @OnClick({R.id.cardTutorial, R.id.imgFont, R.id.imgColor, R.id.imgAlign, R.id.imgSpace, R.id.imgLine, R.id.imgCaps, R.id.imgAlignLeft,
            R.id.imgAlignCenter, R.id.imgAlignRight, R.id.imgFirstCap, R.id.imgSmall, R.id.imgAllCap,
            R.id.llFonts, R.id.llColors, R.id.llCaps, R.id.llSpacing, R.id.llLine, R.id.llAlign})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cardTutorial:
                if (tutorialList != null && tutorialList.size() > 0) {
                    openVideo();
                } else
                    getTutorialList();
                break;
            case R.id.llFonts:
                break;
            case R.id.llColors:
                break;
            case R.id.llCaps:
                break;
            case R.id.llSpacing:
                break;
            case R.id.llLine:
                break;
            case R.id.llAlign:
                break;
            case R.id.imgFont:
                changeItemSelection(imgFont);
                break;
            case R.id.imgColor:
                changeItemSelection(imgColor);
                break;
            case R.id.imgAlign:
                changeItemSelection(imgAlign);
                break;
            case R.id.imgSpace:
                changeItemSelection(imgSpace);
                break;
            case R.id.imgLine:
                changeItemSelection(imgLine);
                break;
            case R.id.imgCaps:
                changeItemSelection(imgCaps);
                break;
            case R.id.imgAlignLeft:
                changeAlignMent(-1);
                break;
            case R.id.imgAlignCenter:
                changeAlignMent(0);
                break;
            case R.id.imgAlignRight:
                changeAlignMent(1);
                break;
            case R.id.imgFirstCap:
                changeCaps(1);
                break;
            case R.id.imgSmall:
                changeCaps(0);
                break;
            case R.id.imgAllCap:
                changeCaps(2);
                break;
        }
    }

    private void changeItemSelection(ImageView image) {
        imgFont.setColorFilter(image == imgFont ? Color.BLACK : getResources().getColor(R.color.gray));
        imgColor.setColorFilter(image == imgColor ? Color.BLACK : getResources().getColor(R.color.gray));
        imgAlign.setColorFilter(image == imgAlign ? Color.BLACK : getResources().getColor(R.color.gray));
        imgSpace.setColorFilter(image == imgSpace ? Color.BLACK : getResources().getColor(R.color.gray));
        imgLine.setColorFilter(image == imgLine ? Color.BLACK : getResources().getColor(R.color.gray));
        imgCaps.setColorFilter(image == imgCaps ? Color.BLACK : getResources().getColor(R.color.gray));

        if (image == imgFont)
            llFonts.bringToFront();
        else if (image == imgColor) {
            llColors.bringToFront();
        } else if (image == imgAlign) {
            llAlign.bringToFront();
        } else if (image == imgSpace) {
            llSpacing.bringToFront();
        } else if (image == imgLine) {
            llLine.bringToFront();
        } else if (image == imgCaps) {
            llCaps.bringToFront();
        }
    }

    private void changeCaps(int i) {
        imgFirstCap.setColorFilter(i == 1 ? Color.BLACK : getResources().getColor(R.color.gray));
        imgAllCap.setColorFilter(i == 2 ? Color.BLACK : getResources().getColor(R.color.gray));
        imgSmall.setColorFilter(i == 0 ? Color.BLACK : getResources().getColor(R.color.gray));

        String text = currentTextSticker.getText().toString();
        currentTextSticker.setText(i == 2 ? text.toUpperCase() : (i == 0 ? text.toLowerCase() : toCamelCase(text)));

        TextEditorDataBean dataBean = textEditorDataBeans.get((Integer) currentTextSticker.getTag());
        dataBean.caps = i;
        textEditorDataBeans.set((Integer) currentTextSticker.getTag(), dataBean);
    }

    @SuppressLint("NewApi")
    private void changeAlignMent(int i) {
        imgAlignCenter.setColorFilter(i == 0 ? Color.BLACK : getResources().getColor(R.color.gray));
        imgAlignLeft.setColorFilter(i == -1 ? Color.BLACK : getResources().getColor(R.color.gray));
        imgAlignRight.setColorFilter(i == 1 ? Color.BLACK : getResources().getColor(R.color.gray));

        String text = currentTextSticker.getText().toString();
        currentTextSticker.setAlign(i == 0 ? Layout.Alignment.ALIGN_CENTER :
                (i == 1 ? Layout.Alignment.ALIGN_OPPOSITE : Layout.Alignment.ALIGN_NORMAL));
        currentTextSticker.setText(text);
        currentTextSticker.requestLayout();

        TextEditorDataBean dataBean = textEditorDataBeans.get((Integer) currentTextSticker.getTag());
        dataBean.align = i;
        textEditorDataBeans.set((Integer) currentTextSticker.getTag(), dataBean);
    }

    @Override
    public void fontClick(Typeface i, int pos) {
        Font font = currentTextSticker.getFont();
        font.setTypeface(i);
        currentTextSticker.setFont(font);
        TextEditorDataBean dataBean = textEditorDataBeans.get((Integer) currentTextSticker.getTag());
        dataBean.font = pos;
        textEditorDataBeans.set((Integer) currentTextSticker.getTag(), dataBean);
    }

    @Override
    public void colorClick(int i) {
        Font font = currentTextSticker.getFont();
        font.setColor(Color.parseColor(colors[i]));
        currentTextSticker.setFont(font);

        TextEditorDataBean dataBean = textEditorDataBeans.get((Integer) currentTextSticker.getTag());
        dataBean.color = i;
        textEditorDataBeans.set((Integer) currentTextSticker.getTag(), dataBean);
    }

    public void createTextSticker(FrameLayout rlMain) {
        TextStickerViewNew1 textSticker = getTextSticker(FrameEditorNewDesign.this,
                scroll, rlMain.getWidth(), rlMain.getHeight(), fonts.get(0));

        EditTextOfSticker(rlMain, textSticker);
        textSticker.setOperationListener(new TextStickerViewNew1.OperationListener() {
            public void onDelete(TextStickerViewNew1 textStickerView) {
                if (currentTextSticker != null && textStickerView.getTag().equals(currentTextSticker.getTag())
                        && textStickerView.isShowHelpBox()) {
                    rlMain.removeView(textStickerView);
                }
            }

            @Override
            public void onDoubleTap(TextStickerViewNew1 textStickerView) {
                EditTextOfSticker(rlMain, textSticker);
            }

            @Override
            public void onSingleTap(TextStickerViewNew1 textStickerView) {
            }

            @Override
            public void onMoveRightToLeft(float x, float y) {

            }

            public void onEdit(TextStickerViewNew1 textStickerView) {
                isEditTextClicked = true;
                currentTextSticker = textStickerView;
                openTextEditor(true);
            }

            public void onTouch(TextStickerViewNew1 textStickerView) {
                scroll.requestDisallowInterceptTouchEvent(true);
                if (currentTextSticker != null) {
                    currentTextSticker.setInEdit(false);
                    currentTextSticker.setShowHelpBox(false);
                }
                currentTextSticker = textStickerView;
                currentTextSticker.setInEdit(true);
            }

            public void onSelect(TextStickerViewNew1 textStickerView) {
                scroll.requestDisallowInterceptTouchEvent(true);
                if (currentTextSticker != null) {
                    currentTextSticker.setInEdit(false);
                    currentTextSticker.setShowHelpBox(false);
                }
                currentTextSticker = textStickerView;
                currentTextSticker.setInEdit(true);
                currentTextSticker.setShowHelpBox(true);
            }
        });
        textSticker.setTag(textEditorDataBeans.size());
        textEditorDataBeans.add(new TextEditorDataBean());
        rlMain.addView(textSticker);
        rlMain.requestLayout();
    }

    private void EditTextOfSticker(FrameLayout rlMain, TextStickerViewNew1 textSticker) {
        TextEditorDialogFragment textEditorDialogFragment = TextEditorDialogFragment.show(
                FrameEditorNewDesign.this, textSticker.getText().toString(),
                false, "");
        textEditorDialogFragment.setOnTextEditorListener(new TextEditorDialogFragment.TextEditor() {
            @Override
            public void onDone(String inputText, int colorCode) {
                if (inputText.length() == 0) {
                    rlMain.removeView(textSticker);
                    rlMain.requestLayout();
                } else
                    textSticker.setText(inputText);
            }

            @Override
            public void onCancel(String s) {
            }
        });
    }

    private void openDeleteTextImage(boolean show) {
        rlFull.setVisibility(show ? View.VISIBLE : View.GONE);

        View redLayout = findViewById(R.id.llDeleteImage);
        ViewGroup parent = findViewById(R.id.rlMainView);

        Transition transition = new Slide(Gravity.BOTTOM);
        transition.setDuration(500);
        transition.addTarget(R.id.llDeleteImage);

        TransitionManager.beginDelayedTransition(parent, transition);
        redLayout.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @OnClick({R.id.cardDeleteImage, R.id.cardCancelImage, R.id.llDeleteImage})
    public void onViewClick(View view) {
        switch (view.getId()) {
            case R.id.llDeleteImage:
                break;
            case R.id.cardDeleteImage:
                if (viewPriceTag != null) {
                    clickedFrame.removeView(viewPriceTag);
                    clickedFrame.requestLayout();
                    viewPriceTag = null;
                } else {
                    if (wtImages.contains(clickedImageView))
                        wtImages.remove(clickedImageView);
                    if (btImages.contains(clickedImageView))
                        btImages.remove(clickedImageView);
                    allImages.remove(clickedImageView);
                    clickedFrame.removeView(clickedImageView);
                    clickedFrame.requestLayout();
                }
                openDeleteTextImage(false);
                break;
            case R.id.cardCancelImage:
                openDeleteTextImage(false);
                break;
        }
    }

    void closeAllEdiotors() {
        if (findViewById(R.id.llDeleteImage).isShown())
            openDeleteTextImage(false);
        if (rlBottomMoveDeleteDialog.isShown())
            toggle(false);
        if (selectedImageStickerViewNew != null)
            selectedImageStickerViewNew.setInEdit(false);
        if (currentTextSticker != null)
            currentTextSticker.setInEdit(false);
        if (currentEdtSticker != null)
            currentEdtSticker.setBackground(null);
        if (findViewById(R.id.llTextEditor).isShown())
            openTextEditor(false);
    }
}