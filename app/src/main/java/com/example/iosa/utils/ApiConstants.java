package com.example.iosa.utils;

import android.app.Activity;

import com.example.iosa.bean.FrameBean;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class ApiConstants {

    JSONObject jsonObject, object, jsonObject1;
    JSONArray jsonArray;
    ArrayList<FrameBean> frameBeans;
    FrameBean responseDataBean;
    String color, border_color;
    HashMap<String, String> hashMap;


    public static int SuggestionRequest = 624;
    public static int SelectLogoRequest = 7924, SelectFrameRequest = 7024, PurchaseRequest = 724;
    public static int REQUEST_CROP = 6709;

    public static String FESTIVAL_FRAME_LIST = "Festival_Frame_list";
    public static String IS_FROM_DEEP_LINK = "isFromDeepLink";
    public static String BARCODE = "barcode";
    public static String EMPLOYEE_ID = "employee_id";

    public static String LOGO_LIST = "Logo_list";
    public static String MYPOST_LIST = "MyPost_list";
    public static String FONT_LIST = "Font_list";
    public static String PROMO_SHARE_URL = "https://www.sociallyapp.net/scan?r=";
    // api url
    // public static String BASE_URL_FOR_FRAMES = "https://master.sociallyapp.net/";
    public static String BASE_URL_OLd = "https://sociallyapp.net/marketing/Api/";


    public static String BASE_URL = "https://master.sociallyapp.net/";
    public static String BASE_URL_API_2 = "https://master.sociallyapp.net/Api/packages/";
    public static String BASE_URL_NEW = "https://sociallyapi.et.r.appspot.com/Api_3/"; // TODO:Socially Version 3 live url

    // public static String BASE_URL_API_2 = "https://sociallyapi.et.r.appspot.com/Api_2/"; // TODO Live base URL
    // public static String BASE_URL_PACKAGES = "https://sociallyapi.et.r.appspot.com/Api/";


    public static String ADD_CUSTOMER = ApiConstants.BASE_URL + "customer/add";
    public static String GET_RANDOM_FRAME = ApiConstants.BASE_URL + "frames/getrandom";
    public static String BACKGROUND_LIST = ApiConstants.BASE_URL + "background/getlist";
    public static String NEW_ARRIVAL_LIST = ApiConstants.BASE_URL + "newarrival/getlist";
    public static String MY_POST_LIST = ApiConstants.BASE_URL + "frames/getlistframeid";
    public static String GET_TODAY_FESTIVAL_LIST = ApiConstants.BASE_URL + "festival/todayfestival";
    public static String GET_RANDOM_FRAME_TEST = ApiConstants.BASE_URL + "frames/getalldata";
    public static String GET_FONT_LIST = ApiConstants.BASE_URL + "Api/fonts/getlist";
    public static String GET_SETTING = ApiConstants.BASE_URL + "setting/getlist";


    public static String GET_TUTORIAL_LIST = ApiConstants.BASE_URL + "tutorial/getlist";
    public static String GET_ADS_LIST = ApiConstants.BASE_URL + "ads/getlist";
    public static String GET_REFERENCE_LIST = BASE_URL_OLd + "reference/getlist";


    public static String CAT_GETLIST = BASE_URL_OLd + "category/getlist";
    public static String GET_FRAME_LIST = BASE_URL_NEW + "frames/getlist";
    public static String GET_LIST_BY_ID = BASE_URL_NEW + "frames/getframedatabyid";
    // api parameters
    public static String DATA = "data";
    public static String SUCCESS = "success";
    public static String RESPONSE = "response";
    public static String ERROR = "error";
    public static String UNIQUE_KEY_VALUE = "a12984dysdsl94923460bsdfsfs097wwendsfumklgdsdffsdj89753487dfskmflamfdh";
    public static String UNIQUE_KEY = "uniquekey";
    public static String CAT_ID = "categoryid";
    public static String CAT_ID_1 = "categoryid_1";
    public static String CAT_ID_2 = "categoryid_2";
    public static String CAT_ID_3 = "categoryid_3";
    public static String RECORD_PER_PAGE = "recordsperpage";
    public static String CURRENT_PAGE_NUMBER = "currentpagenumber";
    public static String FRAME_ID = "frame_id";
    public static String LOGGED_IN_NAME = "logged_in_name";
    public static String LOGGED_IN_EMAIL = "logged_in_email";

    /* public static String IMAGE_LOGO_1 = "IMAGE_LOGO_1";
    public static String IMAGE_LOGO_2 = "IMAGE_LOGO_2"; */
    public static String BRAND_COLOR_1 = "BRAND_COLOR_1";
    public static String BRAND_COLOR_2 = "BRAND_COLOR_2";
    public static String BRAND_COLOR_3 = "BRAND_COLOR_3";
    public static String CUSTOMER_NAME = "customer";
    public static String MAIN_CAT_POS = "main_cat_pos";
    public static String SUB_CAT_POS = "sub_cat_pos";
    public static String PURCHASE = "is_purchase";
    public static String SUGGETION_COUNT = "suggetion_count";

    //Reference NEW ID
    public static String POSTER_ID = "1";
    public static String COVER_ID = "28";
    public static String OFFER_ID = "31";
    public static String GREETING_ID = "32";
    public static String ONLY_LOGO_ID = "104";
    public static String ZOOM_ID = "29";
    public static String GRID_ID = "30";
    public static String FESTIVAL_ID = "103";
    public static String WISH_ID = "32";
  //  public static String CANVAS_ID = "120";
    public static String CANVAS_ID = "153"; // TODO:Socially for Live



    /*login activity*/
    public static String APIKEY = "apikey";
    public static String APIKEY_VALUE = "qa4sc6f2028dd6b29f91e0e656790c8c";

    public static String NAME = "name";
    public static String SUBSCRIPTION = "SubScripition";
    public static String PROMO = "Promo";
    public static String PRICE = "Price";
    public static String LOGIN_ID = "login_id";
    public static String PASSWORD = "password";
    public static String PHONE = "phone";
    public static String USER_TYPE = "user_type";


    public static String KEY_BEAN = "bean";
    public static String KEY_IMAGES = "images";
    public static String KEY_IMAGE = "image";
    public static String KEY_TYPE = "type";
    public static String KEY_POS = "pos";
    public static String KEY_SUB_CAT_ID = "subCatId";
    public static String KEY_MAIN_CAT_ID = "mainCatId";
    public static String KEY_TUTORIAL_LIST = "tutorialList";
    public static String KEY_MASK_IMAGE = "mask_image";

    public ArrayList<FrameBean> getFrameSetupData(Activity activity, String response) {
        frameBeans = new ArrayList<>();
        try {
            jsonObject1 = new JSONObject(response);
            if (jsonObject1.getString(SUCCESS).equals("1")) {
                jsonArray = jsonObject1.getJSONArray(RESPONSE);
                for (int i = 0; i < jsonArray.length(); i++) {
                    object = jsonArray.getJSONObject(i);
                    responseDataBean = new FrameBean();
                    responseDataBean.type = object.getString("type");
                    responseDataBean.src = object.getString("src");
                    responseDataBean.name = object.getString("name");
                    responseDataBean.width = object.getString("width");
                    responseDataBean.height = object.getString("height");
                    responseDataBean.x = object.getString("x");
                    responseDataBean.y = object.getString("y");
                    // testBean.font = object.getString("font");
                    responseDataBean.is_lock = object.getString("is_lock");
                    responseDataBean.is_color = object.getString("is_color");
                    responseDataBean.is_hue = object.getString("is_hue");
                    responseDataBean.is_mask = object.getString("is_mask");
                    responseDataBean.font_name = object.getString("font_name");
                    responseDataBean.is_object = object.getString("is_object");
                    responseDataBean.is_shape = object.getString("is_shape");
                    responseDataBean.is_zoom = object.getString("is_zoom");
                    responseDataBean.is_logo = object.getString("is_logo");
                    responseDataBean.font_spacing = object.getString("font_spacing");
                    responseDataBean.spacing = object.getString("spacing");
                    responseDataBean.justification = object.getString("justification");
                    responseDataBean.is_pt = object.getString("is_pt");
                    responseDataBean.is_st = object.getString("is_st");
                    responseDataBean.is_bt = object.getString("is_bt");
                    responseDataBean.is_wt = object.getString("is_wt");
                    responseDataBean.lineheight = object.getString("lineheight");
                    color = object.getString("color");
                    color = color.replace("0x", "");
                    if (color.contains("#")) {
                        color = "#" + object.getString("color").split("#")[1];
                    } else {
                        color = "#" + color;
                    }
                    responseDataBean.color = color;
                    responseDataBean.size = object.getString("size");
                    responseDataBean.text = object.getString("text").replace("\r", "");
                    responseDataBean.rotation = object.getString("rotation");
                    responseDataBean.position = object.getString("position");
                    responseDataBean.border_size = object.getString("border_size");
                    border_color = object.getString("border_color");
                    if (border_color.length() > 0 && !border_color.equalsIgnoreCase("0"))
                        responseDataBean.border_color = border_color.contains("#") ? "" : "#"
                                + border_color.replace("0x", "");
                    else responseDataBean.border_color = "";
                    frameBeans.add(responseDataBean);
                }
            } else
                CommanUtilsApp.showToast(activity, jsonObject1.getString(ERROR));
            return frameBeans;
        } catch (JSONException e) {
            e.printStackTrace();
            return frameBeans;
        }
    }


}
