package com.example.iosa.utils;

public class KeyParameterConst {
    // parameter for deep linking
    public static String KEY_SCAN_QR_MATCH = "/scan?r=";
    public static String KEY_EMPLOYEE_ID_MATCH = "/employee?eid=";
    public static String KEY_FESTIVAL_MATCH = "festival?fid=";
    public static String KEY_OFFER_ID_MATCH = "offer?oid=";
    public static String KEY_FESTIVAL_SUBCAT_ID_MATCH = "&fdid=";
    // key for pass data in screen
    public static String KEY_BEAN = "bean";
    public static String KEY_IMAGES = "images";
    public static String KEY_IMAGE = "image";
    public static String KEY_TYPE = "type";
    public static String KEY_POS = "pos";
    public static String KEY_SUB_CAT_ID = "subCatId";
    public static String KEY_MAIN_CAT_ID = "mainCatId";
    public static String KEY_TUTORIAL_LIST = "tutorialList";
    public static String KEY_MASK_IMAGE = "mask_image";
    public static String KEY_IS_REFRESH = "isRefresh";
    public static String KEY_FRAMES = "frames";
    public static String KEY_FROM_HOME = "fromHome";
    public static String KEY_URL = "url";
    public static String KEY_LIST = "list";
    public static String KEY_FRAME_ID = "frame_id";
    public static String KEY_PRIMARY_COLOR = "primaryColor";
    public static String KEY_SECONDARY_COLOR = "secondaryColor";
    public static String KEY_COLORS_ARRAYS = "colorsArrays";
    public static String KEY_COUNT = "count";
    public static String KEY_CATEGORY_ID = "categoryid";
    public static String KEY_CODE = "code";
    public static String KEY_PATH = "path";
}
