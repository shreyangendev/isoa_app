package com.example.iosa.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Environment;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;


import com.example.iosa.R;

import java.io.IOException;


public class UtilsStatusbar {
    Activity mActivity;
    Context mContext;

    public UtilsStatusbar(Context mContext) {
        this.mContext = mContext;
    }

    public UtilsStatusbar(Activity activity) {
        this.mActivity = activity;
    }

    public void setStatuBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = mActivity.getWindow();
            Drawable background = mActivity.getResources().getDrawable(R.color.white);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(mActivity.getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
    }
}

