package com.example.iosa.utils;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Source {
    public static String getURL(String html, String regex) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(html);
        while (matcher.find()) {
            return matcher.group(1);
        }
        return "";
    }

    public static ArrayList<String> getVideoUrlList(String html) {
        ArrayList<String> stringArrayList = new ArrayList<>();
        Pattern pattern = Pattern.compile("\"video_url\":\"(.*?)\"");
        Matcher matcher = pattern.matcher(html);
        while (matcher.find()) {
            for (int i = 0; i < matcher.groupCount(); i++) {
                String url = matcher.group(i).replace("\"video_url\":\"", "").replace("\"", "");
                if (!stringArrayList.contains(url))
                    stringArrayList.add(url);
            }
        }
        return stringArrayList;
    }

    public static ArrayList<String> getArrayURL(String html) {
        ArrayList<String> stringArrayList = new ArrayList<>();
        Pattern pattern = Pattern.compile("\"display_url\":\"(.*?)\"");
        Matcher matcher = pattern.matcher(html);
        while (matcher.find()) {
            for (int i = 0; i < matcher.groupCount(); i++) {
                String url = matcher.group(i).replace("\"display_url\":\"", "").replace("\"", "");
                if (!stringArrayList.contains(url))
                    stringArrayList.add(url);
            }
        }
        return stringArrayList;
    }
}


