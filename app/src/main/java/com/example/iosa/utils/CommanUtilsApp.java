package com.example.iosa.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.exifinterface.media.ExifInterface;

import com.appizona.yehiahd.fastsave.FastSave;
import com.example.iosa.Coustomer.BorderedTextView;
import com.example.iosa.FrameEditorNewDesign;
import com.example.iosa.R;
import com.example.iosa.bean.FrameBean;
import com.example.iosa.customTextSticker.Font;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

import static com.example.iosa.utils.ApiConstants.FONT_LIST;
import static com.example.iosa.utils.ApiConstants.LOGO_LIST;
import static com.example.iosa.utils.ApiConstants.MYPOST_LIST;
import static com.example.iosa.utils.ApiConstants.PURCHASE;
import static com.example.iosa.utils.ApiConstants.PurchaseRequest;
import static java.lang.Float.parseFloat;


public class CommanUtilsApp {
    public static boolean isPurchaseSuccess = false;

    // email pattern
    public static final Pattern EMAIL_ADDRESS_PATTERN = Pattern
            .compile("[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" + "\\@"
                    + "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" + "(" + "\\."
                    + "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" + ")+");
    public static String ALLOWED_URI_CHARS = "@#&=*+-_.,:!?()/~'%";

    public static boolean isEmailValid(String email) {
        return EMAIL_ADDRESS_PATTERN.matcher(email).matches();
    }

    public static void scanDownloadedFile(Context context, File file) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            mediaScanIntent.setData(Uri.fromFile(file));
            context.sendBroadcast(mediaScanIntent);
        } else {
            context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.fromFile(file)));
        }
    }
    public static File getLogoImagePath() {
        simpleDateFormat = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US);
        timeStamp = simpleDateFormat.format(new Date());
        file = new File(Environment.getExternalStorageDirectory() + "/.Socially_Logo/");
        if (!file.exists()) {
            file.mkdirs();
        }
        return new File(file.getAbsolutePath() + "/" + timeStamp + ".jpg");
    }

    public static File getLogoImagePathWithName(int s) {
        simpleDateFormat = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US);
        timeStamp = simpleDateFormat.format(new Date());
        file = new File(Environment.getExternalStorageDirectory() + "/.Socially_Logo/");
        if (!file.exists()) {
            file.mkdirs();
        }
        return new File(file.getAbsolutePath() + "/" + s + "_" + timeStamp + ".jpg");
    }

    public static void hideKeyboard(Context activity, EditText editText) {
        editText.clearFocus();
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.showSoftInput(editText, 1);
        inputMethodManager.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }
    public static void strictModePermitAll() {
        policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    public static void enableStrictMode() {
        builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
    }
    Context mActivity;
    public ProgressDialog progressDialog;

    public CommanUtilsApp(Context activity) {
        this.mActivity = activity;
    }

    public static int[][] pixelMatrix;

    public static int[][] getPixelsMatrixFromBitmap(Bitmap bitmap) {
        pixelMatrix = new int[bitmap.getWidth()][bitmap.getHeight()];
        for (int i = 0; i < bitmap.getWidth(); i++)
            for (int j = 0; j < bitmap.getHeight(); j++)
                pixelMatrix[i][j] = bitmap.getPixel(i, j);
        return pixelMatrix;
    }

    public static List<String> getFontList(Context context) {
        list = new ArrayList<String>();
        mPrefs = context.getSharedPreferences(FONT_LIST, context.MODE_PRIVATE);
        gson = new Gson();
        json = mPrefs.getString("FontList", "");
        if (!json.isEmpty()) {
            type = new TypeToken<List<String>>() {
            }.getType();
            list = gson.fromJson(json, type);
        }
        return list;
    }
    public static int w, h;

    public static Bitmap rotate(Bitmap bitmap, float degrees, boolean isCanvas) {
        Bitmap bitm = AdjustBitmapSize(bitmap, new Matrix(), isCanvas);
        Matrix matrix = new Matrix();
        matrix.postRotate(degrees);
        return Bitmap.createBitmap(bitm, 0, 0, bitm.getWidth(), bitm.getHeight(), matrix, true);
        // return AdjustBitmapSize(bitmap, matrix);
    }
    public static Bitmap AdjustBitmapSize(Bitmap bitmap, Matrix matrix, boolean isCanvas) {
        w = bitmap.getWidth();
        h = bitmap.getHeight();
        if (w > (isCanvas ? 900 : 2000) || h > (isCanvas ? 1000 : 2000)) {
            if (w > h || w == h) {
                h = (h * (isCanvas ? 900 : 2000)) / w;
                w = (isCanvas ? 900 : 2000);
            } else {
                w = (w * (isCanvas ? 1000 : 2000)) / h;
                h = (isCanvas ? 1000 : 2000);
            }
            if (bitmap.getWidth() != w || bitmap.getHeight() != h) {
                matrix.setScale(w / (float) bitmap.getWidth(), h / (float) bitmap.getHeight());
            }
        }
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }
    public static Bitmap flip(Bitmap bitmap, boolean horizontal, boolean vertical, boolean isCanvas) {
        Matrix matrix = new Matrix();
        matrix.preScale(horizontal ? -1 : 1, vertical ? -1 : 1);
        return AdjustBitmapSize(bitmap, matrix, isCanvas);
    }
    public static Bitmap modifyOrientation(String path) {
        try {
            ExifInterface ei = new ExifInterface(path);
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    return rotate(BitmapFactory.decodeFile(path, new BitmapFactory.Options()), 90, false);
                case ExifInterface.ORIENTATION_ROTATE_180:
                    return rotate(BitmapFactory.decodeFile(path, new BitmapFactory.Options()), 180, false);
                case ExifInterface.ORIENTATION_ROTATE_270:
                    return rotate(BitmapFactory.decodeFile(path, new BitmapFactory.Options()), 270, false);
                case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                    return flip(BitmapFactory.decodeFile(path, new BitmapFactory.Options()), true, false, false);
                case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                    return flip(BitmapFactory.decodeFile(path, new BitmapFactory.Options()), false, true, false);
                default:
                    return AdjustBitmapSize(BitmapFactory.decodeFile(path, new BitmapFactory.Options()), new Matrix(), false);
            }
            // return AdjustBitmapSize(BitmapFactory.decodeFile(path, new BitmapFactory.Options()), new Matrix());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    public static BorderedTextView getBorderedTextFromApi(Activity activity, FrameBean frameBean, float x, float y, double width, double height, Font font) {
        BorderedTextView ed_myEdittext = new BorderedTextView(activity);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        ed_myEdittext.setLayoutParams(layoutParams);
        ed_myEdittext.setX(x);
        ed_myEdittext.setY(y);
        ed_myEdittext.getLayoutParams().height = (int) Math.ceil(height);
        ed_myEdittext.getLayoutParams().width = (int) Math.ceil(width);
        ed_myEdittext.setBorderTypeface(font.getTypeface());
        ed_myEdittext.setBorderedColor(font.getColor());
        ed_myEdittext.setIncludeFontPadding(false);
        ed_myEdittext.setLineSpacing(0f, 0f);
        ed_myEdittext.setSingleLine(true);
        if (frameBean.border_color.length() > 0) {
            ed_myEdittext.setBorderedColor(Color.parseColor(frameBean.border_color));
            ed_myEdittext.setBorderWidth((int) Float.parseFloat(frameBean.border_size));
            ed_myEdittext.setInsetColor(android.R.color.transparent);
        }
        ed_myEdittext.setTextSizes(font.getSize());
        ed_myEdittext.setTypeface(font.getTypeface());
        // ed_myEdittext.setBackground(activity.getResources().getDrawable(R.color.transparent_yellow));
        ed_myEdittext.setTextAlign(frameBean.justification.equalsIgnoreCase("left") ? BorderedTextView.TextAlign.left
                : (frameBean.justification.equalsIgnoreCase("center") ? BorderedTextView.TextAlign.centerHorizontal : BorderedTextView.TextAlign.right));
        if (frameBean.font_spacing.length() > 0) {
            ed_myEdittext.setLetterSpacing(parseFloat(frameBean.spacing) / 1000f);
        }
        if (frameBean.rotation.length() > 0 && Integer.parseInt(frameBean.rotation) > 0) {
            ed_myEdittext.setX((float) (x - (height / 2) + (width / 2)));
            ed_myEdittext.setY((float) (y - (width / 2) + (height / 2)));
            ed_myEdittext.setRotation(Integer.parseInt(frameBean.rotation));
            ed_myEdittext.getLayoutParams().height = (int) width;
            ed_myEdittext.getLayoutParams().width = (int) height;
            ed_myEdittext.requestLayout();
        }
        ed_myEdittext.setText(frameBean.text);
        ed_myEdittext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextEditorDialogFragment textEditorDialogFragment = TextEditorDialogFragment.show(activity,
                        ed_myEdittext.getText().toString(), true, frameBean.text);
                textEditorDialogFragment.setOnTextEditorListener(new TextEditorDialogFragment.TextEditor() {
                    @Override
                    public void onDone(String inputText, int colorCode) {
                        ed_myEdittext.setText(inputText);
                    }

                    @Override
                    public void onCancel(String s) {
                    }
                });
            }
        });
        return ed_myEdittext;
    }
    public static File getNewHidenFile() {
        String downloadFolderPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US);
        String timeStamp = "." + simpleDateFormat.format(new Date());
        String path;
        File file = new File(downloadFolderPath);
        if (!file.exists())
            file.mkdirs();
        path = file + File.separator + timeStamp + ".jpg";
        if (TextUtils.isEmpty(path)) {
            return null;
        }
        return new File(path);
    }

    public static Bitmap getBitmapFromURI(Context context, Uri uri) {
        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), uri);
            return scaleDown(bitmap, 50, true);
        } catch (IOException e) {
            throw new IllegalArgumentException("Error creating bitmap from URI");
        }
    }


    public static Bitmap modifyOrientationAndHeightWidth(String path) {
        try {
            ExifInterface ei = new ExifInterface(path);
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    return rotate(BitmapFactory.decodeFile(path, new BitmapFactory.Options()), 90, true);
                case ExifInterface.ORIENTATION_ROTATE_180:
                    return rotate(BitmapFactory.decodeFile(path, new BitmapFactory.Options()), 180, true);
                case ExifInterface.ORIENTATION_ROTATE_270:
                    return rotate(BitmapFactory.decodeFile(path, new BitmapFactory.Options()), 270, true);
                case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                    return flip(BitmapFactory.decodeFile(path, new BitmapFactory.Options()), true, false, true);
                case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                    return flip(BitmapFactory.decodeFile(path, new BitmapFactory.Options()), false, true, true);
                default:
                    return AdjustBitmapSize(BitmapFactory.decodeFile(path, new BitmapFactory.Options()), new Matrix(), true);
            }
            // return AdjustBitmapSize(BitmapFactory.decodeFile(path, new BitmapFactory.Options()), new Matrix());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void saveMyPostIdList(List<String> callLog, Context context) {
        mPrefs = context.getSharedPreferences(MYPOST_LIST, context.MODE_PRIVATE);
        prefsEditor = mPrefs.edit();
        gson = new Gson();
        json = gson.toJson(callLog);
        prefsEditor.putString("MyPostIds", json);
        prefsEditor.commit();
    }

    public static List<String> getMyPostIdList(Context context) {
        list = new ArrayList<>();
        mPrefs = context.getSharedPreferences(MYPOST_LIST, context.MODE_PRIVATE);
        gson = new Gson();
        json = mPrefs.getString("MyPostIds", "");
        if (!json.isEmpty()) {
            type = new TypeToken<List<String>>() {
            }.getType();
            list = gson.fromJson(json, type);
        }
        return list;
    }
    public static TextView getApiTextView(Activity activity, FrameBean frameBean, float x, float y,
                                          double width, double height, Font font, boolean isEditText) {
        TextView ed_myEdittext = new TextView(activity);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        ed_myEdittext.setLayoutParams(layoutParams);
        ed_myEdittext.setX(x);
        ed_myEdittext.setY(y);
        ed_myEdittext.getLayoutParams().width = (int) width;
        ed_myEdittext.getLayoutParams().height = (int) height;
        ed_myEdittext.setIncludeFontPadding(false);
        ed_myEdittext.setLineSpacing(0f, 0f);
        ed_myEdittext.setSingleLine(true);
        ed_myEdittext.setTextSize(font.getSize());
        ed_myEdittext.setTextColor(font.getColor());
        ed_myEdittext.setTypeface(font.getTypeface());
        ed_myEdittext.setGravity(Gravity.CENTER_VERTICAL);
        // ed_myEdittext.setBackground(activity.getResources().getDrawable(R.color.transparent_yellow));
        ed_myEdittext.setTextAlignment(frameBean.justification.equalsIgnoreCase("left") ? View.TEXT_ALIGNMENT_TEXT_START
                : (frameBean.justification.equalsIgnoreCase("center") ? View.TEXT_ALIGNMENT_CENTER : View.TEXT_ALIGNMENT_TEXT_END));
        if (frameBean.font_spacing.length() > 0) {
            ed_myEdittext.setLetterSpacing(parseFloat(frameBean.spacing) / 1000f);
        }
        ed_myEdittext.setText(frameBean.text);
        if (isEditText)
            ed_myEdittext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    TextEditorDialogFragment textEditorDialogFragment = TextEditorDialogFragment.show(activity, ed_myEdittext.getText().toString(), true, frameBean.text);
                    textEditorDialogFragment.setOnTextEditorListener(new TextEditorDialogFragment.TextEditor() {
                        @Override
                        public void onDone(String inputText, int colorCode) {
                            ed_myEdittext.setText(inputText);
                        }

                        @Override
                        public void onCancel(String s) {
                        }
                    });
                }
            });
        if (frameBean.rotation.length() > 0 && Integer.parseInt(frameBean.rotation) > 0) {
            ed_myEdittext.setX((float) (x - (height / 2) + (width / 2)));
            ed_myEdittext.setY((float) (y - (width / 2) + (height / 2)));
            ed_myEdittext.setRotation(Integer.parseInt(frameBean.rotation));
            ed_myEdittext.getLayoutParams().height = (int) width;
            ed_myEdittext.getLayoutParams().width = (int) height;
            ed_myEdittext.requestLayout();
        }
        return ed_myEdittext;
    }

    public static boolean isPurchase;

    public static boolean isPurchase(Activity activity, boolean isShowDialog) {
        isPurchase = FastSave.getInstance().getBoolean(PURCHASE, false);
        if (!isPurchase && isShowDialog)
            openPurchaseDialog(activity);
        return isPurchase;
    }
    public static Dialog dialog;

    public static void openPurchaseDialog(Activity activity) {
        dialog = new Dialog(activity, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.setContentView(R.layout.dialog_purchase);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND,
                WindowManager.LayoutParams.FLAG_BLUR_BEHIND);

        ((ImageView) dialog.findViewById(R.id.rlMain)).setImageBitmap(makeBlurBg(activity));
        /* dialog.findViewById(R.id.llTop).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.startActivity(new Intent(activity, SubscriptionTutorialActivity.class));
            }
        }); */
        dialog.findViewById(R.id.txtPurchase).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                openCountrySelectionDialog(activity);
                // activity.startActivityForResult(new Intent(activity, IndiaPurchaseActivity.class), PurchaseRequest);
            }
        });
        dialog.findViewById(R.id.rlMain).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.findViewById(R.id.txtCancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }
    public static void openCountrySelectionDialog(Activity activity) {
        dialog = new Dialog(activity, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.setContentView(R.layout.dialog_country_selection);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND,
                WindowManager.LayoutParams.FLAG_BLUR_BEHIND);

        ((ImageView) dialog.findViewById(R.id.rlMain)).setImageBitmap(makeBlurBg(activity));
        dialog.findViewById(R.id.llIndia).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
              //  activity.startActivityForResult(new Intent(activity, IndiaPurchaseActivity.class), PurchaseRequest);
            }
        });
        dialog.findViewById(R.id.llOutSideIndia).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
              //  activity.startActivityForResult(new Intent(activity, OutSideIndiaPaymentActivity.class), PurchaseRequest);
            }
        });
        dialog.findViewById(R.id.rlMain).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }




    public static Bitmap scaleDown(Bitmap realImage, float maxImageSize, boolean filter) {
        float ratio = Math.min((float) maxImageSize / realImage.getWidth(), (float) maxImageSize / realImage.getHeight());
        int width = Math.round((float) ratio * realImage.getWidth());
        int height = Math.round((float) ratio * realImage.getHeight());
        Bitmap newBitmap = Bitmap.createScaledBitmap(realImage, width, height, filter);
        return newBitmap;
    }

    public static boolean isNetworkAvailable(Activity mActivity) {
        ConnectivityManager connectivityManager = (ConnectivityManager) mActivity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetworkInfo != null && activeNetworkInfo.isConnected())
            return true;
        else {
            CommanUtilsApp.showToast(mActivity, mActivity.getResources().getString(R.string.check_network));
            return false;
        }
    }

    public static StrictMode.VmPolicy.Builder builder;
    public static StrictMode.ThreadPolicy policy;



    public void createProgressDialog(String message) {
        progressDialog = new ProgressDialog(mActivity, ProgressDialog.THEME_HOLO_LIGHT);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
    }


    public void showProgressDialog() {
        if (progressDialog != null) {
            if (!progressDialog.isShowing()) {
                progressDialog.show();
            }
        }
    }

    public void hideProgressDialog() {
        if (progressDialog != null) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
    }

    public static void showToast(Context activity, String message) {
        Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
    }

    public static SimpleDateFormat simpleDateFormat;
    public static String timeStamp, path;
    public static File file;

    public static File getNewFile(Activity activity) {
        simpleDateFormat = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US);
        timeStamp = simpleDateFormat.format(new Date());
        file = new File(Environment.getExternalStorageDirectory()
                + File.separator + activity.getResources().getString(R.string.app_name));
        if (!file.exists())
            file.mkdirs();
        path = file + File.separator + timeStamp + ".png";
        if (TextUtils.isEmpty(path)) {
            return null;
        }
        return new File(path);
    }


    public static int width, height;
    public static Bitmap inputBitmap, outputBitmap;
    public static RenderScript rs;
    public static ScriptIntrinsicBlur theIntrinsic;
    public static Allocation tmpIn, tmpOut;

    @SuppressLint("NewApi")
    public static Bitmap blur(Context context, Bitmap image) {
        float BITMAP_SCALE = 0.1f;
        float BLUR_RADIUS = 25f;
        width = Math.round(image.getWidth() * BITMAP_SCALE);
        height = Math.round(image.getHeight() * BITMAP_SCALE);
        inputBitmap = Bitmap.createScaledBitmap(image, width, height, true);
        outputBitmap = Bitmap.createBitmap(inputBitmap);
        rs = RenderScript.create(context);
        theIntrinsic = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
        tmpIn = Allocation.createFromBitmap(rs, inputBitmap);
        tmpOut = Allocation.createFromBitmap(rs, outputBitmap);
        theIntrinsic.setRadius(BLUR_RADIUS);
        theIntrinsic.setInput(tmpIn);
        theIntrinsic.forEach(tmpOut);
        tmpOut.copyTo(outputBitmap);
        return outputBitmap;
    }


    public static String getMaskImagePath(String maskUrl) {
        String downloadFolderPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath()
                + File.separator + ".Socially";
        File file = new File(downloadFolderPath);
        if (!file.exists())
            file.mkdirs();
        Log.d("---------", " substring : " + maskUrl.substring(maskUrl.lastIndexOf("/")));
        return file + File.separator + maskUrl.substring(maskUrl.lastIndexOf("/"));
    }
    public static List<String> list;
    public static SharedPreferences mPrefs;
    public static Gson gson;
    public static String json;
    public static Type type;
    public static SharedPreferences.Editor prefsEditor;

    public static List<String> getLogoList(Context context) {
        list = new ArrayList<String>();
        mPrefs = context.getSharedPreferences(LOGO_LIST, context.MODE_PRIVATE);
        gson = new Gson();
        json = mPrefs.getString("myJson", "");
        if (!json.isEmpty()) {
            type = new TypeToken<List<String>>() {
            }.getType();
            list = gson.fromJson(json, type);
        }
        return list;
    }

    public static void saveFontList(List<String> callLog, Context context) {
        mPrefs = context.getSharedPreferences(FONT_LIST, context.MODE_PRIVATE);
        prefsEditor = mPrefs.edit();
        gson = new Gson();
        json = gson.toJson(callLog);
        prefsEditor.putString("FontList", json);
        prefsEditor.commit();
    }

    public static Intent intent;

    public static void openYouTube(Activity activity, String s) {
        intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(s));
        intent.setPackage("com.google.android.youtube");
        activity.startActivity(intent);
    }

    public static String name, font;
    public static String[] fonts;

    public static String getFontFileName(Activity activity, String fname) {
        name = "Mont-Bold.ttf";
        fonts = activity.getResources().getStringArray(R.array.fonts);
        for (String s : fonts) {
            font = s;
            if (font.substring(0, font.indexOf(".")).equalsIgnoreCase(fname)) {
                name = font;
                break;
            }
        }
        return name;
    }

    public static Bitmap makeBlurBg(Activity activity) {
        View view = activity.getWindow().getDecorView();
        view.setDrawingCacheEnabled(true);
        view.buildDrawingCache();
        Bitmap b1 = view.getDrawingCache();
        Rect frame = new Rect();
        activity.getWindow().getDecorView().getWindowVisibleDisplayFrame(frame);
        int statusBarHeight = frame.top;
        int width = activity.getWindowManager().getDefaultDisplay().getWidth();
        int height = activity.getWindowManager().getDefaultDisplay().getHeight();

        Bitmap sentBitmap = Bitmap.createBitmap(b1, 0, statusBarHeight, width, height - statusBarHeight);
        view.destroyDrawingCache();
        Bitmap bitmap = sentBitmap.copy(sentBitmap.getConfig(), true);
        return blur(activity, bitmap);
    }
}
