package com.example.iosa.utils;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.Settings;

import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;


public class Permission {

    public static String[] permissions = new String[]{
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA,
            android.Manifest.permission.READ_EXTERNAL_STORAGE};
    public static boolean checkPermission(final Activity act) {
        int result;
        boolean isGranted = false;
        for (String p : permissions) {
            result = ContextCompat.checkSelfPermission(act, p);
            if (result == PackageManager.PERMISSION_GRANTED) {
                isGranted = true;
            } else {
                isGranted = false;
                requestPermission(act);
                break;
            }
        }
        return isGranted;
    }

    public static boolean onlyCheckPermision(final Activity act) {
        int result;
        boolean isGranted = false;
        for (String p : permissions) {
            result = ContextCompat.checkSelfPermission(act, p);
            if (result == PackageManager.PERMISSION_GRANTED) {
                isGranted = true;
            } else {
                isGranted = false;
                break;
            }
        }
        return isGranted;
    }

    public static void requestPermission(Activity act) {
        List<String> listPermissionsNeeded;
        int result;
        listPermissionsNeeded = new ArrayList<>();
        for (String p : permissions) {
            result = ContextCompat.checkSelfPermission(act, p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty())
            ActivityCompat.requestPermissions(act, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), 144);
    }

    public static void ShowPermissionDialog(final Activity act) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(act)
                //.setMessage(act.getString(R.string.permission_message))
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        Intent intent = new Intent();
                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", act.getPackageName(), null);
                        intent.setData(uri);
                        act.startActivity(intent);
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public static boolean isPermission(Activity act) {
        List<String> listPermissionsNeeded;
        int result;
        listPermissionsNeeded = new ArrayList<>();
        for (String p : permissions) {
            result = ContextCompat.checkSelfPermission(act, p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            return false;
        } else {
            return true;
        }
    }
}
