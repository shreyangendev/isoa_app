package com.example.iosa.bean;

import java.io.Serializable;

public class SubCatBean implements Serializable {
    public String id, name, parent_id, parent_name, image;
    public boolean isSelected;
}
