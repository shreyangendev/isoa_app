package com.example.iosa.bean;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CategoryApiResponse {

	@SerializedName("success")
	private String success;

	@SerializedName("response")
	private List<CategoryItemList> response;

	public String getSuccess(){
		return success;
	}

	public List<CategoryItemList> getResponse(){
		return response;
	}
}