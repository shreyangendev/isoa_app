package com.example.iosa.bean;

import com.google.gson.annotations.SerializedName;

public class LoginApiResponse{

	@SerializedName("success")
	private String success;

	@SerializedName("response")
	private LoginItem response;

	public String getSuccess(){
		return success;
	}

	public LoginItem getResponse(){
		return response;
	}
}