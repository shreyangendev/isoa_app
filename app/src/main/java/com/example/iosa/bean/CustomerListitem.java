package com.example.iosa.bean;

import com.google.gson.annotations.SerializedName;

public class CustomerListitem {

	@SerializedName("is_download")
	private String isDownload;

	@SerializedName("id")
	private String id;

	@SerializedName("customer_name")
	private String customerName;

	@SerializedName("shop_name")
	private String shopName;

	@SerializedName("is_subscribe")
	private String isSubscribe;

	@SerializedName("is_cash")
	private String isCash;

	@SerializedName("total_meeting")
	private String totalMeeting;

	public String getIsDownload(){
		return isDownload;
	}

	public String getId(){
		return id;
	}

	public String getCustomerName(){
		return customerName;
	}

	public String getShopName(){
		return shopName;
	}

	public String getIsSubscribe(){
		return isSubscribe;
	}

	public String getIsCash(){
		return isCash;
	}

	public String getTotalMeeting(){
		return totalMeeting;
	}
}