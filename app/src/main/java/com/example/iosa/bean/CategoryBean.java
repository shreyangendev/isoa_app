package com.example.iosa.bean;

import java.io.Serializable;
import java.util.ArrayList;

public class CategoryBean implements Serializable {
    public String catName, id, parent_id, image;
    public ArrayList<SubCatBean> subCatBeans;
}
