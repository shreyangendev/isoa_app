package com.example.iosa.bean;

import com.google.gson.annotations.SerializedName;

public class CustomerApiResponse{

	@SerializedName("success")
	private String success;

	@SerializedName("response")
	private String response;

	@SerializedName("error")
	private String error;

	public String getSuccess(){
		return success;
	}

	public String getResponse(){
		return response;
	}

	public String getError(){
		return error;
	}
}