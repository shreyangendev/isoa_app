package com.example.iosa.bean;

import com.google.gson.annotations.SerializedName;

public class SubCategoryItem {

	@SerializedName("image")
	private String image;

	public void setImage(String image) {
		this.image = image;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setId(String id) {
		this.id = id;
	}

	@SerializedName("parent_id")
	private String parentId;

	@SerializedName("name")
	private String name;

	@SerializedName("id")
	private String id;

	public String getImage(){
		return image;
	}

	public String getParentId(){
		return parentId;
	}

	public String getName(){
		return name;
	}

	public String getId(){
		return id;
	}
}