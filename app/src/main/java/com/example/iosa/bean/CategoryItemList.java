package com.example.iosa.bean;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CategoryItemList {

	@SerializedName("image")
	private String image;

	@SerializedName("sub_category")
	private List<SubCategoryItem> subCategory;

	public void setImage(String image) {
		this.image = image;
	}

	public void setSubCategory(List<SubCategoryItem> subCategory) {
		this.subCategory = subCategory;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setId(String id) {
		this.id = id;
	}

	@SerializedName("parent_id")
	private String parentId;

	@SerializedName("name")
	private String name;

	@SerializedName("id")
	private String id;

	public String getImage(){
		return image;
	}

	public List<SubCategoryItem> getSubCategory(){
		return subCategory;
	}

	public String getParentId(){
		return parentId;
	}

	public String getName(){
		return name;
	}

	public String getId(){
		return id;
	}
}