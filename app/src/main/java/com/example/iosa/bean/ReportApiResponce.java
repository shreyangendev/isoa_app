package com.example.iosa.bean;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ReportApiResponce {

    @SerializedName("success")
    private String success;

    @SerializedName("error")
    private String error;

    @SerializedName("response")
    private List<ReportBean> response;

    public String getSuccess() {
        return success;
    }

    public String getError() {
        return error;
    }


    public List<ReportBean> getResponse() {
        return response;
    }
}