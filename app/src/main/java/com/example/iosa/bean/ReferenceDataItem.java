package com.example.iosa.bean;

import com.google.gson.annotations.SerializedName;

public class ReferenceDataItem {

	public void setImage(String image) {
		this.image = image;
	}

	public void setId(String id) {
		this.id = id;
	}

	@SerializedName("image")
	private String image;

	@SerializedName("id")
	private String id;

	public String getImage(){
		return image;
	}

	public String getId(){
		return id;
	}
}