package com.example.iosa.bean;

import java.io.Serializable;

public class FrameBean implements Serializable {
    public String type, src, name, x, y, width, height, font, is_lock, is_color, border_color, border_size,
            is_hue, is_mask, font_spacing, spacing, justification, lineheight, color, size, text, rotation, position,
            font_name, is_shape, is_object, is_st, is_bt, is_wt, is_pt, is_logo, is_zoom;
    public boolean isDeleted = false;
}
