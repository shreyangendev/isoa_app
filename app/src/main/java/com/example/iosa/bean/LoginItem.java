package com.example.iosa.bean;

import com.google.gson.annotations.SerializedName;

public class LoginItem {

	@SerializedName("login_id")
	private int loginId;

	@SerializedName("total_subscribe")
	private String totalSubScription;

	public String getTotalPromo() {
		return totalPromo;
	}

	public void setTotalPromo(String totalPromo) {
		this.totalPromo = totalPromo;
	}

	@SerializedName("total_promo")
	private String totalPromo;

	public String getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(String totalPrice) {
		this.totalPrice = totalPrice;
	}

	@SerializedName("total_price")
	private String totalPrice;

	@SerializedName("apikey")
	private String apikey;

	@SerializedName("phone")
	private String phone;

	@SerializedName("name")
	private String name;

	@SerializedName("email")
	private String email;

	public LoginItem() {
	}

	public int getLoginId(){
		return loginId;
	}

	public String getApikey(){
		return apikey;
	}

	public String getPhone(){
		return phone;
	}

	public String getName(){
		return name;
	}

	public String getEmail(){
		return email;
	}

	public String getTotalSubScription() {
		return totalSubScription;
	}

}
