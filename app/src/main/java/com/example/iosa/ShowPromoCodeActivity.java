package com.example.iosa;

import android.os.Bundle;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;

import com.example.iosa.utils.UtilsStatusbar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ShowPromoCodeActivity extends BaseActivity {

    @BindView(R.id.imgShowPromoBack)
    ImageView imgShowPromoBack;
    @BindView(R.id.txtShowPromoNo)
    TextView txtShowPromoNo;
    @BindView(R.id.cardTop)
    CardView cardTop;
    @BindView(R.id.imgShowPromoCall)
    ImageView imgShowPromoCall;
    @BindView(R.id.txtShowPromoLink)
    TextView txtShowPromoLink;
    @BindView(R.id.txtShowPromoURL)
    TextView txtShowPromoURL;
    @BindView(R.id.txtDate)
    TextView txtDate;
    @BindView(R.id.llOne)
    LinearLayout llOne;
    @BindView(R.id.llTwo)
    LinearLayout llTwo;
    @BindView(R.id.llThree)
    LinearLayout llThree;
    @BindView(R.id.llfour)
    LinearLayout llfour;
    @BindView(R.id.imgDownload)
    ImageView imgDownload;
    @BindView(R.id.imgWhatsapp)
    ImageView imgWhatsapp;
    @BindView(R.id.llFive)
    LinearLayout llFive;
    @BindView(R.id.mainFrame)
    FrameLayout mainFrame;
    @BindView(R.id.rlMain)
    RelativeLayout rlMain;
    @BindView(R.id.cardVShowPromo)
    CardView cardVShowPromo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new UtilsStatusbar(ShowPromoCodeActivity.this).setStatuBar();
        setContentView(R.layout.activity_show_promo_code);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.imgShowPromoBack)
    public void onClick() {
        finish();
    }
}