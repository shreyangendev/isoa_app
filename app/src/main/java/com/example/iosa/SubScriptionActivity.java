package com.example.iosa;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.appizona.yehiahd.fastsave.FastSave;
import com.example.iosa.Adapter.SubscriptionBottomAdapter;
import com.example.iosa.Adapter.SubscriptionTopAdapter;
import com.example.iosa.Retrofitapi.APIInterface;
import com.example.iosa.Retrofitapi.ApiClient;
import com.example.iosa.VollyApi.NoConnectivityException;
import com.example.iosa.bean.WeekResponse;
import com.example.iosa.bean.WeekResponseItem;
import com.example.iosa.utils.CommanUtilsApp;
import com.example.iosa.utils.UtilsStatusbar;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.graphics.Color.BLACK;
import static android.graphics.Color.GRAY;
import static com.example.iosa.utils.ApiConstants.APIKEY;
import static com.example.iosa.utils.ApiConstants.BASE_URL;

public class SubScriptionActivity extends BaseActivity {

    @BindView(R.id.imgCodeBack)
    ImageView imgCodeBack;
    @BindView(R.id.rVSubscriptionBottomlist)
    RecyclerView rVSubscriptionBottomlist;
    SubscriptionTopAdapter subscriptionTopAdapter;
    SubscriptionBottomAdapter subscriptionBottomAdapter;
    CommanUtilsApp commanUtilsApp;
    @BindView(R.id.txtWeek)
    TextView txtWeek;
    @BindView(R.id.cardWeek)
    CardView cardWeek;
    @BindView(R.id.rlWeek)
    LinearLayout rlWeek;
    @BindView(R.id.txtMonth)
    TextView txtMonth;
    @BindView(R.id.cardMonth)
    CardView cardMonth;
    @BindView(R.id.rlMonth)
    LinearLayout rlMonth;
    @BindView(R.id.threeMonth)
    TextView threeMonth;
    @BindView(R.id.cardThreeMonth)
    CardView cardThreeMonth;
    @BindView(R.id.rlThreeMonth)
    LinearLayout rlThreeMonth;
    @BindView(R.id.txtTwelveMonth)
    TextView txtTwelveMonth;
    @BindView(R.id.cardTwelveMonth)
    CardView cardTwelveMonth;
    @BindView(R.id.rlTwelveMonth)
    LinearLayout rlTwelveMonth;
    @BindView(R.id.layoutTop)
    LinearLayout layoutTop;
    Call<JsonObject> call;
    List<WeekResponseItem> reportBeanList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new UtilsStatusbar(SubScriptionActivity.this).setStatuBar();
        setContentView(R.layout.activity_sub_scription);
        ButterKnife.bind(this);

       /* subscriptionBottomAdapter = new SubscriptionBottomAdapter(SubScriptionActivity.this);
        rVSubscriptionBottomlist.setAdapter(subscriptionBottomAdapter);
        rVSubscriptionBottomlist.setHasFixedSize(false);*/

        commanUtilsApp = new CommanUtilsApp(this);
        commanUtilsApp.createProgressDialog(getResources().getString(R.string.loading));

        getData(1);
    }

    private void getData(int i) {
        cardWeek.setVisibility(i == 1 ? View.VISIBLE : View.GONE);
        cardMonth.setVisibility(i == 2 ? View.VISIBLE : View.GONE);
        cardThreeMonth.setVisibility(i == 3 ? View.VISIBLE : View.GONE);
        cardTwelveMonth.setVisibility(i == 4 ? View.VISIBLE : View.GONE);

        txtWeek.setTextColor(i == 1 ? BLACK : GRAY);
        txtMonth.setTextColor(i == 2 ? BLACK : GRAY);
        threeMonth.setTextColor(i == 3 ? BLACK : GRAY);
        txtTwelveMonth.setTextColor(i == 4 ? BLACK : GRAY);

        commanUtilsApp.showProgressDialog();

        if (i == 1) {
            call = ApiClient.getClient(BASE_URL, getApplicationContext()).create(APIInterface.class)
                    .reportWeek(FastSave.getInstance().getString(APIKEY, ""));
        } else if (i == 2) {
            call = ApiClient.getClient(BASE_URL, getApplicationContext()).create(APIInterface.class)
                    .reportMonth(FastSave.getInstance().getString(APIKEY, ""));
        } else if (i == 3) {
            call = ApiClient.getClient(BASE_URL, getApplicationContext()).create(APIInterface.class)
                    .reportThreeMonth(FastSave.getInstance().getString(APIKEY, ""));
        } else if (i == 4) {
            call = ApiClient.getClient(BASE_URL, getApplicationContext()).create(APIInterface.class)
                    .reportThisYear(FastSave.getInstance().getString(APIKEY, ""));

        }
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> objectResponse) {
                if (objectResponse.body().get("success").toString().contains("1")) {
                    Gson gson = new Gson();
                    WeekResponse response = gson.fromJson(objectResponse.body().toString(), WeekResponse.class);
                    reportBeanList = response.getResponse();
                } else {
                    CommanUtilsApp.showToast(SubScriptionActivity.this, objectResponse.body().get("error").toString());
                }
                subscriptionBottomAdapter = new SubscriptionBottomAdapter(SubScriptionActivity.this,reportBeanList);
                rVSubscriptionBottomlist.setAdapter(subscriptionBottomAdapter);
                rVSubscriptionBottomlist.setHasFixedSize(false);
                commanUtilsApp.hideProgressDialog();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                t.printStackTrace();
                if (t instanceof NoConnectivityException) {
                    CommanUtilsApp.showToast(SubScriptionActivity.this, getResources().getString(R.string.check_network));
                }
                commanUtilsApp.hideProgressDialog();
            }
        });
    }

    @OnClick({R.id.imgCodeBack, R.id.rlWeek, R.id.rlMonth, R.id.rlThreeMonth, R.id.rlTwelveMonth})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgCodeBack:
                finish();
                break;
            case R.id.rlWeek:
                getData(1);
                break;
            case R.id.rlMonth:
                getData(2);
                break;
            case R.id.rlThreeMonth:
                getData(3);
                break;
            case R.id.rlTwelveMonth:
                getData(4);
                break;
        }
    }
}