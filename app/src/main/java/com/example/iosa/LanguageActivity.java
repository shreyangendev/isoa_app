package com.example.iosa;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.iosa.Adapter.LanguageBottomAdapter;
import com.example.iosa.Adapter.LanguageTopAdapter;
import com.example.iosa.utils.CommanUtilsApp;
import com.example.iosa.utils.UtilsStatusbar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LanguageActivity extends BaseActivity {

    @BindView(R.id.imgLanguageBack)
    ImageView imgLanguageBack;
    @BindView(R.id.editLanguageSearch)
    EditText editLanguageSearch;
    @BindView(R.id.rVLanguageToplist)
    RecyclerView rVLanguageToplist;
    @BindView(R.id.rVLanguageBottomlist)
    RecyclerView rVLanguageBottomlist;
    LanguageBottomAdapter languageBottomAdapter;
    LanguageTopAdapter languageTopAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new UtilsStatusbar(LanguageActivity.this).setStatuBar();
        setContentView(R.layout.activity_language);
        ButterKnife.bind(this);

        languageTopAdapter = new LanguageTopAdapter(LanguageActivity.this);
        rVLanguageToplist.setAdapter(languageTopAdapter);
        rVLanguageToplist.setHasFixedSize(false);

        languageBottomAdapter = new LanguageBottomAdapter(LanguageActivity.this);
        rVLanguageBottomlist.setAdapter(languageBottomAdapter);
        rVLanguageBottomlist.setHasFixedSize(false);
    }

    @OnClick(R.id.imgLanguageBack)
    public void onClick() {
        finish();
    }
}