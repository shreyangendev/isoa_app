package com.example.iosa.Adapter;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;


import com.example.iosa.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeFrameTypeListAdapter extends RecyclerView.Adapter<HomeFrameTypeListAdapter.ViewHolder> {
    Activity mActivity;
    ArrayList<Drawable> typeList;
    int pos, width = -1;
    FrameTypeClick frameTypeClick;

    public HomeFrameTypeListAdapter(Activity mActivity, ArrayList<Drawable> typeList, FrameTypeClick frameTypeClick, int pos) {
        this.mActivity = mActivity;
        this.typeList = typeList;
        this.frameTypeClick = frameTypeClick;
        this.pos = pos;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_home_frame_type_list_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        /* holder.txtTitle.setText(typeList.get(position).toUpperCase());
        holder.txtTitle.setTypeface(Typeface.createFromAsset(mActivity.getAssets(), position == pos
                ? "fonts/Mont-Bold.ttf" : "fonts/Mont-Book.ttf"));
        holder.txtTitle.setTextColor(position == pos ? mActivity.getResources().getColor(R.color.white)
                : mActivity.getResources().getColor(R.color.black)); */
        holder.imgSelected.setVisibility(position == pos ? View.VISIBLE : View.GONE);
        holder.img.setImageDrawable(typeList.get(position));
        /* holder.cardBg.setCardBackgroundColor(position == pos ? mActivity.getResources().getColor(R.color.purple)
                : mActivity.getResources().getColor(R.color.white)); */

        if (width == -1)
            holder.cardBg.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    holder.cardBg.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    width = holder.cardBg.getMeasuredWidth();
                }
            });
        holder.cardBg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pos = position;
                notifyDataSetChanged();
                frameTypeClick.frameTypeClick(position, width);
            }
        });
    }

    public int getWidth() {
        Log.d("---------- type : ", width + "");
        return width;
    }

    @Override
    public int getItemCount() {
        return typeList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.img)
        ImageView img;
        @BindView(R.id.imgSelected)
        ImageView imgSelected;
        @BindView(R.id.cardBg)
        CardView cardBg;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public interface FrameTypeClick {
        void frameTypeClick(int i, int width);
    }
}