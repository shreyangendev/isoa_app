package com.example.iosa.Adapter;

import android.app.Activity;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.appizona.yehiahd.fastsave.FastSave;
import com.example.iosa.R;
import com.example.iosa.bean.SubCatBean;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.example.iosa.utils.ApiConstants.SUB_CAT_POS;


public class HomeSubCatListAdapter extends RecyclerView.Adapter<HomeSubCatListAdapter.ViewHolder> {
    SubCatClick click;
    Activity activity;
    List<SubCatBean> subCatBeans;
    String selectedId;
    int width = -1;
    Typeface tf;

    public HomeSubCatListAdapter(Activity activity, SubCatClick click, List<SubCatBean> subCatBeans) {
        this.activity = activity;
        this.click = click;
        this.subCatBeans = subCatBeans;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_home_top_list_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        selectedId = FastSave.getInstance().getString(SUB_CAT_POS, "0");
        if (width == -1)
            holder.llMain.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    holder.llMain.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    width = holder.llMain.getMeasuredWidth();
                }
            });
        holder.txt.setText(subCatBeans.get(position).name);
        holder.txt.setTextColor(activity.getResources().getColor(
                subCatBeans.get(position).id.equalsIgnoreCase(selectedId) ? R.color.cyan : R.color.black));
        tf = Typeface.createFromAsset(activity.getAssets(),
                subCatBeans.get(position).id.equalsIgnoreCase(selectedId) ? "fonts/Mont-Bold.ttf" : "fonts/Mont-Book.ttf");
        holder.txt.setTypeface(tf);
        holder.llMain.setBackgroundColor(activity.getResources().getColor(
                subCatBeans.get(position).id.equalsIgnoreCase(selectedId) ? R.color.cyan : R.color.white));
        holder.llMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                click.subCatClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return subCatBeans.size();
    }

    public int getWidth() {
        Log.d("---------- sub cat : ", width + "");
        return width;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.llMain)
        LinearLayout llMain;
        @BindView(R.id.txt)
        TextView txt;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public interface SubCatClick {
        void subCatClick(int i);
    }
}