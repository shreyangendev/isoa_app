package com.example.iosa.Adapter;

import android.app.Activity;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.iosa.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SubscriptionTopAdapter extends RecyclerView.Adapter<SubscriptionTopAdapter.ViewHolder> {
    Activity activity;
    int ClickPosition = 0;

    public SubscriptionTopAdapter(Activity activity) {
        this.activity = activity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_subscription_top_list_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtTopName.setTextColor(ClickPosition == position ? Color.BLACK : Color.GRAY);
        holder.cardTop.setVisibility(ClickPosition == position ? View.VISIBLE : View.INVISIBLE);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ClickPosition = position;
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return 10;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtTopName)
        TextView txtTopName;
        @BindView(R.id.rlOne)
        RelativeLayout rlOne;
        @BindView(R.id.cardTop)
        CardView cardTop;
        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}
