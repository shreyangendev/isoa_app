package com.example.iosa.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.iosa.R;
import com.example.iosa.SearchDetailActivity;
import com.example.iosa.bean.ReportBean;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.ViewHolder> {
    Activity activity;
    int clickPostin = 0;
    List<ReportBean> response;

    public SearchAdapter(Activity activity, List<ReportBean> response) {
        this.activity = activity;
        this.response = response;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_search_list_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        clickPostin = position;

        holder.txtName.setText(response.get(position).getCustomerName());
        holder.txtNumber.setText(response.get(position).getPhone());
        holder.txtShopeName.setText(response.get(position).getShopName());

        holder.txtName.setVisibility(holder.txtName.length() <=0 ? View.GONE : View.VISIBLE);
        holder.txtNumber.setVisibility(holder.txtNumber.length() <=0 ? View.GONE : View.VISIBLE);
        holder.txtShopeName.setVisibility(holder.txtShopeName.length() <=0 ? View.GONE : View.VISIBLE);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.startActivity(new Intent(activity.getApplicationContext(), SearchDetailActivity.class).putExtra("data", response.get(position)));
            }
        });
    }

    @Override
    public int getItemCount() {
        return response.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtName)
        TextView txtName;
        @BindView(R.id.txtNumber)
        TextView txtNumber;
        @BindView(R.id.txtShopeName)
        TextView txtShopeName;
        @BindView(R.id.txtPromocode)
        TextView txtPromocode;
        @BindView(R.id.imgAttachmentShare)
        ImageView imgAttachmentShare;
        @BindView(R.id.imgAttachmentWA)
        ImageView imgAttachmentWA;
        @BindView(R.id.imgAttachmentLocation)
        ImageView imgAttachmentLocation;
        @BindView(R.id.llRight)
        LinearLayout llRight;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}