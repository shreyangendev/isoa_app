package com.example.iosa.Adapter;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.iosa.R;
import com.example.iosa.utils.CommanUtilsApp;
import com.google.gson.Gson;

import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.example.iosa.utils.ApiConstants.LOGO_LIST;
import static com.example.iosa.utils.CommanUtilsApp.getLogoList;


public class DesignWithFontListAdapter extends RecyclerView.Adapter<DesignWithFontListAdapter.ViewHolder> {
    Activity activity;
    List<String> list;
    String s;
    TextView txtWhite, txtBlack, cardWhite;
    CommanUtilsApp commanUtilsApp;
    Done done;
    List<String> logoList;

    public DesignWithFontListAdapter(Activity activity, List<String> listFont, String s, Done done) {
        this.activity = activity;
        this.list = listFont;
        this.done = done;
        this.s = s;
        commanUtilsApp = new CommanUtilsApp(activity);
        commanUtilsApp.createProgressDialog(activity.getResources().getString(R.string.loading));
        logoList = getLogoList(activity);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_design_with_font_list_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        if (!s.isEmpty()) {
            holder.txtWhite.setText(s);
            holder.txtBlack.setText(s);
        }
        holder.txtWhite.setTypeface(Typeface.createFromAsset(activity.getAssets(), "fonts/" + list.get(position)));
        holder.txtBlack.setTypeface(Typeface.createFromAsset(activity.getAssets(), "fonts/" + list.get(position)));
        holder.llConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtWhite = holder.txtWhite;
                txtBlack = holder.txtBlack;
                cardWhite = holder.txtWhite;
                commanUtilsApp.showProgressDialog();
                saveImage(0);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtWhite)
        TextView txtWhite;
        @BindView(R.id.txtBlack)
        TextView txtBlack;
        @BindView(R.id.llConfirm)
        LinearLayout llConfirm;
        @BindView(R.id.cardWhite)
        CardView cardWhite;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    private void saveImage(int i) {
        File file = null;
        try {
            Bitmap generatedBitmap = loadBitmapFromView(i == 0 ? txtWhite : txtBlack);
            file = CommanUtilsApp.getLogoImagePathWithName(i);
            if (file != null) {
                FileOutputStream output = new FileOutputStream(file.getAbsolutePath());
                generatedBitmap.compress(Bitmap.CompressFormat.PNG, 100, output);
                output.close();
                i++;
                logoList.add(file.getAbsolutePath());
                if (i < 2) {
                    saveImage(i);
                } else {
                    saveLogoList(logoList);
                    commanUtilsApp.hideProgressDialog();
                    done.completeLogoGenerate();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void saveLogoList(List<String> callLog) {
        SharedPreferences mPrefs = activity.getSharedPreferences(LOGO_LIST, activity.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(callLog);
        prefsEditor.putString("myJson", json);
        prefsEditor.commit();
    }

    public interface Done {
        void completeLogoGenerate();
    }

    public Bitmap loadBitmapFromView(View view) {
        int height = view.getHeight();
        int width = view.getWidth();
        view.measure(View.MeasureSpec.makeMeasureSpec(width, View.MeasureSpec.EXACTLY),
                View.MeasureSpec.makeMeasureSpec(height, View.MeasureSpec.EXACTLY));
        Bitmap bitmap = null;
        try {
            bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            canvas.translate(0.0f, 0.0f);
            canvas.drawColor(-1, PorterDuff.Mode.CLEAR);
            canvas.scale(1, 1);
            view.draw(canvas);
            return bitmap;
        } catch (Exception e) {
            e.printStackTrace();
            return bitmap;
        }
    }
}