package com.example.iosa.Adapter;

import android.app.Activity;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.iosa.R;
import com.example.iosa.bean.DateWiseResponseItem;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class AchivementScanAdapter extends RecyclerView.Adapter<AchivementScanAdapter.ViewHolder> {
    Activity activity;
    List<DateWiseResponseItem> dateWiseResponseItemList;

    public AchivementScanAdapter(Activity activity, List<DateWiseResponseItem> dateWiseResponseItemList) {
        this.activity = activity;
        this.dateWiseResponseItemList = dateWiseResponseItemList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_achivement_scan_list_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String date = dateWiseResponseItemList.get(position).getDate();
        try {
            Date newDate = new SimpleDateFormat("yyyy-MM-dd").parse(date);
            date = new SimpleDateFormat("dd MMM").format(newDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.txtDate.setText(date);
        holder.txtQR.setText(dateWiseResponseItemList.get(position).getQr());
        holder.txtSubScription.setText(dateWiseResponseItemList.get(position).getSubscription());


        holder.cardQr.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                holder.cardQr.getViewTreeObserver().removeOnGlobalLayoutListener(this);

                if (!dateWiseResponseItemList.get(position).getQr().equalsIgnoreCase("0")) {
                    holder.cardSubscription.getLayoutParams().height = Integer.parseInt(dateWiseResponseItemList.get(position).getSubscription()) * holder.cardQr.getMeasuredHeight()
                            / Integer.parseInt(dateWiseResponseItemList.get(position).getQr());
                    holder.cardSubscription.requestLayout();
                } else {
                    holder.cardQr.setVisibility(View.INVISIBLE);
                    holder.cardSubscription.setVisibility(View.INVISIBLE);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return dateWiseResponseItemList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.cardQr)
        CardView cardQr;
        @BindView(R.id.cardSubscription)
        CardView cardSubscription;
        @BindView(R.id.txtDate)
        TextView txtDate;
        @BindView(R.id.txtQR)
        TextView txtQR;
        @BindView(R.id.txtSubScription)
        TextView txtSubScription;
        @BindView(R.id.llRight)
        LinearLayout llRight;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}