package com.example.iosa.Adapter;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;


import com.example.iosa.CategoryActivity;
import com.example.iosa.HomeActivity;
import com.example.iosa.R;
import com.example.iosa.bean.CategoryItemList;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {

    CategoryActivity categoryActivity;
    List<CategoryItemList> categoryItemLists;
    String selectedID = "0";
    int width = -1;
    String instaImage;

    PackageClick packageClick;
    int clickPosition = 0;


    public CategoryAdapter(CategoryActivity categoryActivity, List<CategoryItemList> categoryItemLists,String instaImage,PackageClick packageClick) {
        this.categoryActivity = categoryActivity;
        this.categoryItemLists = categoryItemLists;
        this.instaImage = instaImage;
        this.packageClick = packageClick;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_category_list_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.aboutText.setText(categoryItemLists.get(position).getName());

        if (categoryItemLists.get(position).getImage().length() > 0)
            Picasso.get().load(categoryItemLists.get(position).getImage()).placeholder(R.drawable.placehoder).into(holder.aboutImg);
        else Picasso.get().load(R.drawable.placehoder).into(holder.aboutImg);


     //   selectedID = FastSave.getInstance().getString(MAIN_CAT_POS, "0");
       /* if (width == -1)
            holder.aboutMain.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    holder.aboutMain.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    width = holder.aboutMain.getMeasuredWidth();
                }
            });
        holder.aboutText.setText(categoryItemLists.get(position).getName());

        if (categoryItemLists.get(position).getImage().length() > 0)
            Picasso.get().load(categoryItemLists.get(position).getImage()).placeholder(R.drawable.placehoder).into(holder.aboutImg);
        else Picasso.get().load(R.drawable.placehoder).into(holder.aboutImg);

        holder.aboutText.setTypeface(Typeface.createFromAsset(aboutActivity.getAssets(),
                clickedPos==position
                        ? "fonts/Mont-Bold.ttf" : "fonts/Mont-Book.ttf"));*/

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                packageClick.packageClick(position);
                clickPosition = position;
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return categoryItemLists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.aboutMain)
        RelativeLayout aboutMain;
        @BindView(R.id.aboutText)
        TextView aboutText;
        @BindView(R.id.aboutCard)
        CardView aboutCard;
        @BindView(R.id.aboutImg)
        ImageView aboutImg;
        @BindView(R.id.linelayoutTop)
        LinearLayout linelayoutTop;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public interface PackageClick {
        void packageClick(int i);
    }
}

