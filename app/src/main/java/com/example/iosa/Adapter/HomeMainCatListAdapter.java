package com.example.iosa.Adapter;

import android.app.Activity;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.appizona.yehiahd.fastsave.FastSave;
import com.example.iosa.R;
import com.example.iosa.bean.CategoryBean;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.example.iosa.utils.ApiConstants.MAIN_CAT_POS;


public class HomeMainCatListAdapter extends RecyclerView.Adapter<HomeMainCatListAdapter.ViewHolder> {
    Activity activity;
    String selectedID = "0";
    int width = -1;
    List<CategoryBean> subCatBeans;
    MainCatItemClick itemClick;

    public HomeMainCatListAdapter(Activity activity, List<CategoryBean> subCatBeans, MainCatItemClick itemClick) {
        this.activity = activity;
        this.subCatBeans = subCatBeans;
        this.itemClick = itemClick;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_home_type_list_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        selectedID = FastSave.getInstance().getString(MAIN_CAT_POS,
                "0");
        if (width == -1)
            holder.llMain.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    holder.llMain.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    width = holder.llMain.getMeasuredWidth();
                }
            });
        holder.txt.setText(subCatBeans.get(position).catName);
        Log.d("-----name : ",subCatBeans.get(position).catName);
        Log.d("-----image : ",subCatBeans.get(position).catName);
        if (subCatBeans.get(position).image.length() > 0)
            Picasso.get().load(subCatBeans.get(position).image).placeholder(R.drawable.placehoder).into(holder.img);
        else Picasso.get().load(R.drawable.placehoder).into(holder.img);
        holder.txt.setTextColor(subCatBeans.get(position).id.equalsIgnoreCase(selectedID) ? activity.getResources().getColor(R.color.orange)
                : activity.getResources().getColor(R.color.black));
        holder.txt.setTypeface(Typeface.createFromAsset(activity.getAssets(), subCatBeans.get(position).id.equalsIgnoreCase(selectedID)
                ? "fonts/Mont-Bold.ttf" : "fonts/Mont-Book.ttf"));
        holder.card.setCardElevation(subCatBeans.get(position).id.equalsIgnoreCase(selectedID) ? activity.getResources().getDimension(R.dimen._3sdp) : 0);
        holder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedID = subCatBeans.get(position).id;
                itemClick.mainCatItemClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
      Log.d("---------- main count :", subCatBeans.size() + "");
        return subCatBeans.size();
    }

    public int getWidth() {
        Log.d("---------- main cat : ", width + "");
        return width;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.card)
        CardView card;
        @BindView(R.id.txt)
        TextView txt;
        @BindView(R.id.img)
        ImageView img;
        @BindView(R.id.llMain)
        LinearLayout llMain;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public interface MainCatItemClick {
        void mainCatItemClick(int i);
    }
}