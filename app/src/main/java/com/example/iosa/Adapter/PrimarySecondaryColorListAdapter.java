package com.example.iosa.Adapter;

import android.app.Activity;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.iosa.Coustomer.PrimarySecondaryTonePos;
import com.example.iosa.R;

import java.util.ArrayList;

public class PrimarySecondaryColorListAdapter extends RecyclerView.Adapter<PrimarySecondaryColorListAdapter.ViewHolder> {
    Activity activity;
    ArrayList<Integer> colors;
    int i;
    PrimarySecondaryTonePos primarySecondaryTonePos;
    boolean isPt;
    float width = -1;

    public PrimarySecondaryColorListAdapter(Activity mainActivity, ArrayList<Integer> colors, int i, PrimarySecondaryTonePos primarySecondaryTonePos, boolean isPt) {
        this.activity = mainActivity;
        this.colors = colors;
        this.i = i;
        this.isPt = isPt;
        this.primarySecondaryTonePos = primarySecondaryTonePos;
    }

    public PrimarySecondaryColorListAdapter(Activity mainActivity, ArrayList<Integer> colors, int i, PrimarySecondaryTonePos primarySecondaryTonePos,
                                            boolean isPt, int width) {
        this.activity = mainActivity;
        this.colors = colors;
        this.i = i;
        this.isPt = isPt;
        this.width = width;
        this.primarySecondaryTonePos = primarySecondaryTonePos;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_color_list, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (width != -1) {
            holder.rlMain.getLayoutParams().height = (int) (width / 3) - 16;
            holder.rlMain.requestLayout();
        }
        holder.cardColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                primarySecondaryTonePos.primarySecondaryTonePos(false, isPt, i, colors.get(position));
            }
        });
        holder.imgPicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                primarySecondaryTonePos.primarySecondaryTonePos(true, isPt, i, 0);
            }
        });
        if (colors.get(position) != null) {
            holder.cardColor.setCardBackgroundColor(colors.get(position));
            holder.imgPicker.setVisibility(View.GONE);
            if (colors.get(position) == Color.WHITE) {
                holder.cardBgColor.setBackground(activity.getResources().getDrawable(R.drawable.rounded_corner_black_border));
            } else
                holder.cardBgColor.setBackground(null);
        } else {
            holder.cardBgColor.setBackground(null);
            holder.imgPicker.setVisibility(View.VISIBLE);
            holder.cardColor.setCardBackgroundColor(Color.TRANSPARENT);
        }
    }

    @Override
    public int getItemCount() {
        return colors.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CardView cardColor;
        LinearLayout cardBgColor;
        ImageView imgPicker;
        RelativeLayout rlMain;

        public ViewHolder(View view) {
            super(view);
            rlMain = view.findViewById(R.id.rlMain);
            cardBgColor = view.findViewById(R.id.cardBgColor);
            cardColor = view.findViewById(R.id.cardColor);
            imgPicker = view.findViewById(R.id.imgPicker);
        }
    }
}