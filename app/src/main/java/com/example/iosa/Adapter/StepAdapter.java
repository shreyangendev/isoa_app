package com.example.iosa.Adapter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.iosa.R;
import com.squareup.picasso.Picasso;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;

import static java.security.AccessController.getContext;

public class StepAdapter extends RecyclerView.Adapter<StepAdapter.ViewHolder> {
    Activity activity;
    File[] allFiles;

    public StepAdapter(Activity activity, File[] allFiles) {
        this.activity = activity;
        this.allFiles = allFiles;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_step_list_item, parent, false);
        return new ViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

       Glide.with(activity)
                .load(allFiles[position])
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .dontAnimate()
                .into(holder.imgInsta);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // activity.startActivity(new Intent(activity.getApplicationContext(), SearchDetailActivity.class));

            }
        });
    }

    @Override
    public int getItemCount() {
         return allFiles.length;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imgInsta)
        ImageView imgInsta;
        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            imgInsta.findViewById(R.id.imgInsta);
        }
    }

}