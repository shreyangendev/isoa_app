package com.example.iosa.Adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.iosa.R;
import com.example.iosa.bean.WeekResponseItem;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SubscriptionBottomAdapter extends RecyclerView.Adapter<SubscriptionBottomAdapter.ViewHolder> {
    Activity activity;
    List<WeekResponseItem> reportBeanList;


    public SubscriptionBottomAdapter(Activity activity, List<WeekResponseItem> reportBeanList) {
        this.activity = activity;
        this.reportBeanList = reportBeanList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_subscription_bottom_list_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtPayableName.setText(reportBeanList.get(position).getProductLink());
        holder.txtSubscriptionNumber.setText(reportBeanList.get(position).getPhone());
        holder.txtRS.setText(reportBeanList.get(position).getPackageName());

        String date = reportBeanList.get(position).getAddedOn();
        try {
            Date newDate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(date);
            date = new SimpleDateFormat("dd MMM yyyy").format(newDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
          holder.txtDateSubscription.setText(date);

    }

    @Override
    public int getItemCount() {
        return reportBeanList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtPayableName)
        TextView txtPayableName;
        @BindView(R.id.txtRS)
        TextView txtRS;
        @BindView(R.id.txtSubscriptionNumber)
        TextView txtSubscriptionNumber;
        @BindView(R.id.txtDateSubscription)
        TextView txtDateSubscription;


        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}
