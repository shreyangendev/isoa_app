package com.example.iosa.Adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.iosa.R;

import butterknife.ButterKnife;

public class LanguageBottomAdapter extends RecyclerView.Adapter<LanguageBottomAdapter.ViewHolder> {
    Activity activity;

    public LanguageBottomAdapter(Activity activity) {
        this.activity = activity;
    }

    @Override
    public LanguageBottomAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_language_bottom_list_item, parent, false);
        return new LanguageBottomAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull LanguageBottomAdapter.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 10;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}
