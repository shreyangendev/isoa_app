package com.example.iosa.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.iosa.R;
import com.example.iosa.bean.CodeResponseItem;

import java.net.URLEncoder;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CodeBottomAdapter extends RecyclerView.Adapter<CodeBottomAdapter.ViewHolder> {
    Activity activity;
    List<CodeResponseItem> scanListItems;



    public CodeBottomAdapter(Activity activity, List<CodeResponseItem> scanListItems) {
        this.activity = activity;
        this.scanListItems = scanListItems;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_code_bottom_list_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtPayableName.setText(scanListItems.get(position).getProductLink());
        holder.txtPayableNumber.setText(scanListItems.get(position).getPhone());

    }

    @Override
    public int getItemCount() {
        return scanListItems.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtPayableName)
        TextView txtPayableName;
        @BindView(R.id.txtPayableNumber)
        TextView txtPayableNumber;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}
