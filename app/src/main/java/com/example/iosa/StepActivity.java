package com.example.iosa;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;
import com.example.iosa.Adapter.StepAdapter;
import com.example.iosa.utils.UtilsStatusbar;

import java.io.File;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class StepActivity extends BaseActivity {

    @BindView(R.id.txtStepNumber)
    TextView txtStepNumber;
    @BindView(R.id.llOne)
    LinearLayout llOne;
    @BindView(R.id.llTwo)
    LinearLayout llTwo;
    @BindView(R.id.llThree)
    LinearLayout llThree;
    @BindView(R.id.llFour)
    LinearLayout llFour;
    @BindView(R.id.imgWhatsapp)
    ImageView imgWhatsapp;
    @BindView(R.id.imgDownload)
    ImageView imgDownload;
    @BindView(R.id.lldownload)
    LinearLayout lldownload;

    StepAdapter stepAdapter;
    @BindView(R.id.imgStepBack)
    ImageView imgStepBack;
    @BindView(R.id.cardStepNext)
    CardView cardStepNext;
    String instaImage, getNumber, barcode, promo;
    @BindView(R.id.txtStartCommunication)
    TextView txtStartCommunication;

    @BindView(R.id.cardFour)
    CardView cardFour;

    String SCAN_PATH;
    @BindView(R.id.rVSteplist)
    RecyclerView rVSteplist;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new UtilsStatusbar(StepActivity.this).setStatuBar();
        setContentView(R.layout.activity_step);
        ButterKnife.bind(this);

        instaImage = getIntent().getExtras().getString("image");
        getNumber = getIntent().getExtras().getString("number");
        barcode = getIntent().getExtras().getString("barcode");
        promo = getIntent().getExtras().getString("promo");


        setImageData();
    }

    private void setImageData() {
        String downloadFolderPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
                .getAbsolutePath() + File.separator + "SociallySaveImage";
        File folder = new File(downloadFolderPath);
        File[] allFiles = folder.listFiles();

        if (allFiles != null) {
            stepAdapter = new StepAdapter(StepActivity.this, allFiles);
            rVSteplist.setAdapter(stepAdapter);
            rVSteplist.setHasFixedSize(false);
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        setImageData();
    }

    private boolean appInstalledOrNot(String uri) {
        PackageManager pm = getPackageManager();
        boolean app_installed;
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        } catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        }
        return app_installed;
    }

    private void whatsapp(String phone) {
        try {
            if (appInstalledOrNot("com.whatsapp")) {
                PackageManager packageManager = getPackageManager();
                Intent i = new Intent(Intent.ACTION_VIEW);
                String url = "https://api.whatsapp.com/send?phone=" + phone + "&text=" + URLEncoder.encode("Hello " + getNumber, "UTF-8");
                i.setPackage("com.whatsapp");
                i.setData(Uri.parse(url));
                startActivity(i);
            } else if (appInstalledOrNot("com.whatsapp.w4b")) {
                PackageManager packageManager = getPackageManager();
                Intent i = new Intent(Intent.ACTION_VIEW);
                String url = "https://api.whatsapp.com/send?phone=" + phone + "&text=" + URLEncoder.encode("Hello " + getNumber, "UTF-8");
                i.setPackage("com.whatsapp.w4b");
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        } catch (Exception e) {
        }
    }

    @OnClick({R.id.imgStepBack, R.id.addToNext, R.id.txtStepNumber, R.id.txtStartCommunication})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgStepBack:
                finish();
                break;
            case R.id.addToNext:
                startActivityForResult(new Intent(StepActivity.this, CategoryActivity.class)
                        .putExtra("image", instaImage), 30);
                break;
            case R.id.txtStepNumber:
                Intent intent = new Intent(ContactsContract.Intents.Insert.ACTION);
                intent.setType(ContactsContract.RawContacts.CONTENT_TYPE);
                intent.putExtra(ContactsContract.Intents.Insert.PHONE, getNumber)
                        .putExtra(ContactsContract.Intents.Insert.PHONE_TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_WORK);
                startActivity(intent);
                break;
            case R.id.txtStartCommunication:
                whatsapp(getNumber);
                break;
        }
    }
}