package com.example.iosa.texteditor;

import android.app.Activity;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


import com.example.iosa.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FontListAdapter extends RecyclerView.Adapter<FontListAdapter.ViewHolder> {
    Activity activity;
    List<String> fonts;
    FontClick fontClick;
    int selectedPos;

    public FontListAdapter(Activity activity, List<String> fonts, FontClick fontClick, int selectedPos) {
        this.activity = activity;
        this.fonts = fonts;
        this.fontClick = fontClick;
        this.selectedPos = selectedPos;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_font_list_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Typeface tf = Typeface.createFromAsset(activity.getAssets(), "fonts/" + fonts.get(position));
        holder.imgSelected.setVisibility(selectedPos == position ? View.VISIBLE : View.INVISIBLE);
        holder.txt.setTypeface(tf);
        holder.txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedPos = position;
                notifyDataSetChanged();
                fontClick.fontClick(tf, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return fonts.size();
    }

    public void setPosition(int selectedFontPos) {
        this.selectedPos = selectedFontPos;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt)
        TextView txt;
        @BindView(R.id.imgSelected)
        ImageView imgSelected;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public interface FontClick {
        void fontClick(Typeface tf, int pos);
    }
}