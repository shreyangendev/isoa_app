package com.example.iosa.texteditor;

import android.app.Activity;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;


import com.example.iosa.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ColorListAdapter extends RecyclerView.Adapter<ColorListAdapter.ViewHolder> {
    Activity activity;
    String[] colors;
    ColorClick colorClick;
    int selectedPos;

    public ColorListAdapter(Activity activity, String[] colors, ColorClick colorClick, int selectedPos) {
        this.activity = activity;
        this.colors = colors;
        this.colorClick = colorClick;
        this.selectedPos = selectedPos;
    }

    @Override
    public ColorListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_text_editor_color_list_item, parent, false);
        return new ColorListAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ColorListAdapter.ViewHolder holder, int position) {
        holder.imgSelected.setVisibility(selectedPos == position ? View.VISIBLE : View.GONE);
        holder.card.setCardBackgroundColor(Color.parseColor(colors[position]));
        holder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedPos = position;
                colorClick.colorClick(position);
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return colors.length;
    }

    public void setPosition(int selectedColorPos) {
        this.selectedPos = selectedColorPos;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.card)
        CardView card;
        @BindView(R.id.imgSelected)
        ImageView imgSelected;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public interface ColorClick {
        void colorClick(int i);
    }
}