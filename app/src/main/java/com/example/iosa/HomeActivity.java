package com.example.iosa;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.appizona.yehiahd.fastsave.FastSave;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.coolerfall.download.DownloadCallback;
import com.coolerfall.download.DownloadManager;
import com.coolerfall.download.DownloadRequest;
import com.coolerfall.download.Logger;
import com.coolerfall.download.OkHttpDownloader;
import com.coolerfall.download.Priority;
import com.example.iosa.Adapter.HomeFrameTypeListAdapter;
import com.example.iosa.Adapter.HomeMainCatListAdapter;
import com.example.iosa.Adapter.HomeSubCatListAdapter;
import com.example.iosa.VollyApi.ApiCall;
import com.example.iosa.VollyApi.VolleyTaskCompleteListener;
import com.example.iosa.bean.CategoryBean;
import com.example.iosa.bean.ListBean;
import com.example.iosa.bean.SubCatBean;
import com.example.iosa.utils.Permission;
import com.example.iosa.utils.UtilsStatusbar;
import com.zyyoona7.popup.EasyPopup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.Serializable;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.OkHttpClient;

import android.widget.Toast;
import com.example.iosa.utils.CommanUtilsApp;
import com.example.iosa.utils.Utils;
import com.facebook.AccessToken;
import static com.example.iosa.utils.ApiConstants.APIKEY;
import static com.example.iosa.utils.ApiConstants.CANVAS_ID;
import static com.example.iosa.utils.ApiConstants.CAT_GETLIST;
import static com.example.iosa.utils.ApiConstants.CAT_ID;
import static com.example.iosa.utils.ApiConstants.CAT_ID_1;
import static com.example.iosa.utils.ApiConstants.CAT_ID_2;
import static com.example.iosa.utils.ApiConstants.CAT_ID_3;
import static com.example.iosa.utils.ApiConstants.COVER_ID;
import static com.example.iosa.utils.ApiConstants.CURRENT_PAGE_NUMBER;
import static com.example.iosa.utils.ApiConstants.DATA;
import static com.example.iosa.utils.ApiConstants.ERROR;
import static com.example.iosa.utils.ApiConstants.FESTIVAL_ID;
import static com.example.iosa.utils.ApiConstants.GET_FONT_LIST;
import static com.example.iosa.utils.ApiConstants.GET_FRAME_LIST;
import static com.example.iosa.utils.ApiConstants.GET_TODAY_FESTIVAL_LIST;
import static com.example.iosa.utils.ApiConstants.GREETING_ID;
import static com.example.iosa.utils.ApiConstants.GRID_ID;
import static com.example.iosa.utils.ApiConstants.KEY_BEAN;
import static com.example.iosa.utils.ApiConstants.KEY_IMAGES;
import static com.example.iosa.utils.ApiConstants.KEY_MAIN_CAT_ID;
import static com.example.iosa.utils.ApiConstants.KEY_MASK_IMAGE;
import static com.example.iosa.utils.ApiConstants.KEY_SUB_CAT_ID;
import static com.example.iosa.utils.ApiConstants.KEY_TUTORIAL_LIST;
import static com.example.iosa.utils.ApiConstants.KEY_TYPE;
import static com.example.iosa.utils.ApiConstants.MAIN_CAT_POS;
import static com.example.iosa.utils.ApiConstants.OFFER_ID;
import static com.example.iosa.utils.ApiConstants.ONLY_LOGO_ID;
import static com.example.iosa.utils.ApiConstants.POSTER_ID;
import static com.example.iosa.utils.ApiConstants.PURCHASE;
import static com.example.iosa.utils.ApiConstants.RECORD_PER_PAGE;
import static com.example.iosa.utils.ApiConstants.RESPONSE;
import static com.example.iosa.utils.ApiConstants.SUB_CAT_POS;
import static com.example.iosa.utils.ApiConstants.SUCCESS;
import static com.example.iosa.utils.ApiConstants.UNIQUE_KEY;
import static com.example.iosa.utils.ApiConstants.UNIQUE_KEY_VALUE;

public class HomeActivity extends BaseActivity implements VolleyTaskCompleteListener {
    @BindView(R.id.listMain)
    RecyclerView listMain;
    @BindView(R.id.imgOneFeed)
    ImageView imgOneFeed;
    @BindView(R.id.imgDualFeed)
    ImageView imgDualFeed;
    @BindView(R.id.imgHomeBack)
    ImageView imgHomeBack;

    TextView txtDataNotAvailable;
    RecyclerView listFrames, listFrameType, listMainCat, listSubCategory;
    EasyPopup mCirclePop;
    // vars
    FrameType selectedFrameType = FrameType.Poster;
    public static boolean isFrameClick = false;
    boolean isSubCatMatch, isCatMatch;
    boolean isFontListGet = false, requestCheck, isLoadingData, promoClick;
    ApiCall apiCall;
    int frameTypePos = 0, mainPos, subPos,
            category_api_request = 6,
            frame_api_request = 9, clickFramePos,
            font_api_request = 2,
            current_page_number = 1;

    int POSTER_POS = 0, COVER_POS = 1,
            CANVAS_POS = 2, WISH_POS = 3, OFFER_POS = 4;

    String mainCatId, subCatId;
    String downloadImageUrl, total_records, catId;
    HashMap<String, String> hashMap;
    // arrays
    ArrayList<CategoryBean> categoryBeans;
    ArrayList<SubCatBean> subCatBeans;
    ArrayList<ListBean> frameBeans;
    List<String> logoList;
    GridLayoutManager mainListLayoutManger;
    LinearLayoutManager catListLinearLayoutManager, subCatListLinearLayoutManager, frameTypeLinearLayoutManager;
    // adapter
    HomeListAdapter homeListAdapter;
    HomeFrameTypeListAdapter frameTypeListAdapter;
    HomeMainCatListAdapter homeMainCatListAdapter;
    HomeSubCatListAdapter homeSubCatListAdapter;
    boolean isFromDeepLink;
    String promoCode = "", phone = "", bottomFestivalId = "", employee_id = "";
    String instaImage;

    @Override
    public void onTaskCompleted(int service_code, String response) {
        if (service_code == category_api_request) {
            try {
                categoryBeans.clear();
                subCatBeans.clear();
                object = new JSONObject(response);
                success = object.getString(SUCCESS);
                if (success.equals("1")) {
                    jsonArray = object.getJSONArray(RESPONSE);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        jsonObject = jsonArray.getJSONObject(i);
                        categoryBean = new CategoryBean();
                        categoryBean.id = jsonObject.getString("id");
                        categoryBean.image = jsonObject.getString("image");
                        if (catId.equalsIgnoreCase(FESTIVAL_ID)) {
                            categoryBean.catName = jsonObject.getString("festival_name");
                            categoryBean.subCatBeans = new ArrayList<>();
                        } else {
                            categoryBean.catName = jsonObject.getString("name");
                            categoryBean.parent_id = jsonObject.getString("parent_id");
                            categoryBean.subCatBeans = new ArrayList<>();
                            array = jsonObject.getJSONArray("sub_category");
                            for (int j = 0; j < array.length(); j++) {
                                jsonObject1 = array.getJSONObject(j);
                                subCatBean = new SubCatBean();
                                subCatBean.id = jsonObject1.getString("id");
                                subCatBean.name = jsonObject1.getString("name");
                                subCatBean.parent_id = jsonObject1.getString("parent_id");
                                subCatBean.image = jsonObject1.getString("image");
                                subCatBean.parent_name = categoryBean.catName;
                                subCatBean.isSelected = false;
                                categoryBean.subCatBeans.add(subCatBean);
                                if (!isSubCatMatch) {
                                    if (jsonObject1.getString("id").equalsIgnoreCase(subCatId)) {
                                        subPos = j;
                                        isSubCatMatch = true;
                                    }
                                }
                                if (categoryBean.id.equalsIgnoreCase(mainCatId)) {
                                    subCatBeans.add(subCatBean);
                                }
                            }
                        }
                        if (!isCatMatch)
                            if (jsonObject.getString("id").equalsIgnoreCase(mainCatId)) {
                                mainPos = i;
                                isCatMatch = true;
                            }
                        categoryBeans.add(categoryBean);
                    }
                    if (!isCatMatch) {
                        mainPos = 0;
                        if (categoryBeans.size() > 0)
                            mainCatId = categoryBeans.get(0).id;
                    }
                    if (!isSubCatMatch) {
                        subPos = 0;
                        if (categoryBeans.get(mainPos).subCatBeans.size() > 0) {
                            subCatId = categoryBeans.get(mainPos).subCatBeans.get(0).id;
                            subCatBeans.addAll(categoryBeans.get(0).subCatBeans);
                        } else {
                            subCatId = "0";
                        }
                    }
                    if (listMainCat != null)
                        listMainCat.setVisibility(categoryBeans.size() > 0 ? View.VISIBLE : View.GONE);
                    if (listSubCategory != null)
                        listSubCategory.setVisibility(subCatBeans.size() > 0 ? View.VISIBLE : View.GONE);
                    current_page_number = 1;
                    // setHeaderDataInMiddle();
                    getFrames();
                } else {
                    // llPoster.setVisibility(View.GONE);
                    CommanUtilsApp.showToast(HomeActivity.this, object.getString(ERROR));
                    if (listMainCat != null)
                        listMainCat.setVisibility(View.GONE);
                    if (listSubCategory != null)
                        listSubCategory.setVisibility(View.GONE);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (service_code == frame_api_request) {
            try {
                jsonObject = new JSONObject(response);
                success = jsonObject.getString(SUCCESS);
                if (success.equalsIgnoreCase("1")) {
                    object = jsonObject.getJSONObject(RESPONSE);
                    total_records = object.getString("total_records");
                    jsonArray = object.getJSONArray(DATA);

                    requestCheck = jsonArray.length() > 0;
                    isLoadingData = false;

                    for (int i = 0; i < jsonArray.length(); i++) {
                        mainFrameBean = new ListBean();
                        jsonObject1 = jsonArray.getJSONObject(i);
                        mainFrameBean.id = jsonObject1.getString("id");
                        mainFrameBean.thumb = jsonObject1.getString("thumb");
                        mainFrameBean.image = jsonObject1.getString("image");
                        // mainFrameBean.is_free = i > 1 ? "0" : "1";

                        mainFrameBean.subcategory_id = jsonObject1.getString("subcategory_id");
                        mainFrameBean.frame_name = jsonObject1.getString("frame_name");
                        mainFrameBean.mask_image = jsonObject1.getString("mask_image");
                        mainFrameBean.is_mask = jsonObject1.getString("is_mask");
                        mainFrameBean.maincategory_id = jsonObject1.getString("maincategory_id");

                        frameBeans.add(mainFrameBean);
                    }
                    if (txtDataNotAvailable != null)
                        txtDataNotAvailable.setVisibility(frameBeans.size() > 0 ? View.GONE : View.VISIBLE);
                    homeMainCatListAdapter.notifyDataSetChanged();
                    homeSubCatListAdapter.notifyDataSetChanged();
                    homeListAdapter.notifyDataSetChanged();
                    setHeaderDataInMiddle();
                } else {
                    CommanUtilsApp.showToast(HomeActivity.this, jsonObject.getString(ERROR));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (!isFontListGet)
                getFontList();
        } else if (service_code == font_api_request) {
            try {
                jsonObject = new JSONObject(response);
                if (jsonObject.getString(SUCCESS).equalsIgnoreCase("1")) {
                    jsonArray = jsonObject.getJSONArray(RESPONSE);
                    list = new ArrayList<>();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        list.add(jsonArray.getJSONObject(i).getString("font_name"));
                    }
                    isFontListGet = true;
                    CommanUtilsApp.saveFontList(list, HomeActivity.this);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public enum FrameType {
        Poster, COVER, Canvas, Offer, Wish, Festival;
    }

    public static boolean isHomeScreen;
    public static HomeActivity homeActivity;


    @Override
    protected void onResume() {
        super.onResume();
        homeActivity = this;
        isHomeScreen = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        isHomeScreen = false;
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new UtilsStatusbar(HomeActivity.this).setStatuBar();
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        instaImage = getIntent().getExtras().getString("image");
        Log.d("image", instaImage+" ");

/*        isFromDeepLink = getIntent().getBooleanExtra(IS_FROM_DEEP_LINK, false);
        promoCode = getIntent().getStringExtra(BARCODE);
        employee_id = getIntent().getStringExtra(EMPLOYEE_ID);*/

        deleteSociallyFolder();
        initData();
        setUpListData();

        if (catId.equalsIgnoreCase(OFFER_ID)) {
            mainCatId = "0";
            subCatId = "0";
            getFrames();
        } else
            getFrameData();

        changeView(2);

    }


    private void deleteSociallyFolder() {
        if (Permission.onlyCheckPermision(HomeActivity.this)) {
            String downloadFolderPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath()
                    + File.separator + ".Socially";
            File file = new File(downloadFolderPath);
            if (file.exists())
                if (file != null)
                    if (file.listFiles() != null)
                        for (File f : file.listFiles()) {
                            f.delete();
                        }
        }
    }

    private void initData() {
        frameBeans = new ArrayList<>();
        categoryBeans = new ArrayList<>();
        subCatBeans = new ArrayList<>();
        apiCall = new ApiCall(this, this);

        catId = FastSave.getInstance().getString(CAT_ID, POSTER_ID);
        mainCatId = FastSave.getInstance().getString(MAIN_CAT_POS, "0");
        subCatId = FastSave.getInstance().getString(SUB_CAT_POS, "0");

        selectedFrameType = catId.equalsIgnoreCase(POSTER_ID) ? FrameType.Poster :
                catId.equalsIgnoreCase(COVER_ID) ? FrameType.COVER :
                        catId.equalsIgnoreCase(CANVAS_ID) ? FrameType.Canvas :
                                catId.equalsIgnoreCase(GREETING_ID) ? FrameType.Wish :
                                        catId.equalsIgnoreCase(OFFER_ID) ? FrameType.Offer :
                                                catId.equalsIgnoreCase(FESTIVAL_ID) ? FrameType.Festival :
                                                        catId.equalsIgnoreCase(POSTER_ID) ? FrameType.Poster : FrameType.Poster;
        frameTypePos = catId.equalsIgnoreCase(POSTER_ID) ? POSTER_POS :
                catId.equalsIgnoreCase(COVER_ID) ? COVER_POS :
                        catId.equalsIgnoreCase(CANVAS_ID) ? CANVAS_POS :
                                catId.equalsIgnoreCase(GREETING_ID) ? WISH_POS :
                                        catId.equalsIgnoreCase(OFFER_ID) ? OFFER_POS
                                                : POSTER_POS;
    }

    private void getFrameData() {
        isCatMatch = false;
        isSubCatMatch = false;
        if (catId.equalsIgnoreCase(POSTER_ID) || catId.equalsIgnoreCase(GREETING_ID)
                || catId.equalsIgnoreCase(COVER_ID) || catId.equalsIgnoreCase(CANVAS_ID)) {
            if (CommanUtilsApp.isNetworkAvailable(HomeActivity.this)) {
                hashMap = new HashMap<String, String>();
                hashMap.put(APIKEY,"qa4sc6f2028dd6b29f91e0e656790c8c");
                hashMap.put("parent_category", catId);
                apiCall.GetData(Request.Method.POST, CAT_GETLIST, hashMap, category_api_request);
            }
        } else if (catId.equalsIgnoreCase(FESTIVAL_ID)) {
            if (CommanUtilsApp.isNetworkAvailable(HomeActivity.this)) {
                hashMap = new HashMap<String, String>();
                hashMap.put(UNIQUE_KEY, UNIQUE_KEY_VALUE);
                hashMap.put("country_id", "2");
                apiCall.GetData(Request.Method.POST, GET_TODAY_FESTIVAL_LIST, hashMap, category_api_request);
            }
        }
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private void setUpListData() {
        homeListAdapter = new HomeListAdapter(HomeActivity.this);
        mainListLayoutManger = new GridLayoutManager(this, 1);
        mainListLayoutManger.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return position == 0 ? 1 : 1;
            }
        });
        catListLinearLayoutManager = new LinearLayoutManager(HomeActivity.this, RecyclerView.HORIZONTAL, false);
        subCatListLinearLayoutManager = new LinearLayoutManager(HomeActivity.this, RecyclerView.HORIZONTAL, false);
        frameTypeLinearLayoutManager = new LinearLayoutManager(HomeActivity.this, RecyclerView.HORIZONTAL, false);

        ArrayList<Drawable> frameTypes = new ArrayList<>();
        frameTypes.add(getResources().getDrawable(R.drawable.ic_poster_banner));
        frameTypes.add(getResources().getDrawable(R.drawable.ic_cover_banner));
        frameTypes.add(getResources().getDrawable(R.drawable.ic_cover_banner));
        frameTypes.add(getResources().getDrawable(R.drawable.ic_wish_banner));
        frameTypes.add(getResources().getDrawable(R.drawable.ic_offer_banner));
        frameTypeListAdapter = new HomeFrameTypeListAdapter(HomeActivity.this,
                frameTypes, new HomeFrameTypeListAdapter.FrameTypeClick() {
            @Override
            public void frameTypeClick(int i, int width) {
                if (frameTypePos != i) {
                    current_page_number = 1;
                    frameBeans.clear();
                    frameTypePos = i;
                    frameTypeClickData(i);
                }
            }
        }, frameTypePos);
        listMain.setLayoutManager(mainListLayoutManger);
        listMain.setAdapter(homeListAdapter);

        homeMainCatListAdapter = new HomeMainCatListAdapter(this, categoryBeans, new HomeMainCatListAdapter.MainCatItemClick() {
            @Override
            public void mainCatItemClick(int i) {
                if (mainPos != i) {
                    mainCatId = categoryBeans.get(i).id;
                    subCatId = categoryBeans.get(i).subCatBeans.size() > 0 ?
                            categoryBeans.get(i).subCatBeans.get(0).id : "0";
                    mainPos = i;
                    subPos = 0;
                    subCatBeans.clear();
                    subCatBeans.addAll(categoryBeans.get(mainPos).subCatBeans);
                    homeSubCatListAdapter.notifyDataSetChanged();
                    current_page_number = 1;
                    frameBeans.clear();
                    getFrames();
                }
            }
        });
        homeSubCatListAdapter = new HomeSubCatListAdapter(this, new HomeSubCatListAdapter.SubCatClick() {
            @Override
            public void subCatClick(int i) {
                if (subPos != i) {
                    subCatId = categoryBeans.get(mainPos).subCatBeans.size() > i ?
                            categoryBeans.get(mainPos).subCatBeans.get(i).id : "0";
                    current_page_number = 1;
                    frameBeans.clear();
                    // FastSave.getInstance().saveString(SUB_CAT_POS, subCatId);
                    subPos = i;
                    homeSubCatListAdapter.notifyDataSetChanged();
                    getFrames();
                }
            }
        }, subCatBeans);
        homeListAdapter.notifyDataSetChanged();
    }

    private void getFrames() {
        hashMap = new HashMap<>();
        hashMap.put(UNIQUE_KEY, UNIQUE_KEY_VALUE);

        if (selectedFrameType == FrameType.Festival) {
            hashMap.put("festivalid", mainCatId);
        } else {
            hashMap.put(CAT_ID_1, catId);
            hashMap.put(CAT_ID_2, mainCatId);
            hashMap.put(CAT_ID_3, subCatId);
        }
        FastSave.getInstance().saveString(MAIN_CAT_POS, mainCatId);
        FastSave.getInstance().saveString(SUB_CAT_POS, subCatId);
        hashMap.put(RECORD_PER_PAGE, "50");
        hashMap.put(CURRENT_PAGE_NUMBER, current_page_number + "");
        hashMap.put("is_pro", FastSave.getInstance().getBoolean(PURCHASE, false) ? "1" : "0");

        if (CommanUtilsApp.isNetworkAvailable(HomeActivity.this)) {
            if (selectedFrameType != FrameType.Festival) {
                if (current_page_number == 1)
                    apiCall.GetData(Request.Method.POST, GET_FRAME_LIST, hashMap, frame_api_request);
                else
                    apiCall.GetDataPagination(Request.Method.POST, GET_FRAME_LIST, hashMap, frame_api_request);
            }
        }

    }

    private void frameTypeClickData(int i) {
        current_page_number = 1;
        catId = i == POSTER_POS ? POSTER_ID :
                i == COVER_POS ? COVER_ID :
                        i == CANVAS_POS ? CANVAS_ID :
                                i == WISH_POS ? GREETING_ID :
                                        i == OFFER_POS ? OFFER_ID : POSTER_ID;
        FastSave.getInstance().saveString(CAT_ID, catId);
        selectedFrameType = (i == POSTER_POS) ? FrameType.Poster :
                i == COVER_POS ? FrameType.COVER :
                        i == CANVAS_POS ? FrameType.Canvas :
                                i == WISH_POS ? FrameType.Wish :
                                        i == OFFER_POS ? FrameType.Offer : FrameType.Poster;
        if (selectedFrameType == FrameType.Offer) {
            mainCatId = "0";
            subCatId = "0";
        }
        if (i == POSTER_POS || i == COVER_POS || i == CANVAS_POS || i == WISH_POS) {
            getFrameData();
        } else if (i == OFFER_POS) {
            getFrames();
            if (listMainCat != null)
                listMainCat.setVisibility( View.GONE);
            if (listSubCategory != null)
                listSubCategory.setVisibility( View.GONE);
        } else {
            if (txtDataNotAvailable != null)
                txtDataNotAvailable.setVisibility(View.GONE);
            homeListAdapter.notifyDataSetChanged();
        }
        setHeaderDataInMiddle();
    }

    private void setHeaderDataInMiddle() {
        if (frameTypeLinearLayoutManager != null) {
            frameTypeLinearLayoutManager.setSmoothScrollbarEnabled(true);
            frameTypeLinearLayoutManager.scrollToPositionWithOffset(frameTypePos,
                    (int) (Utils.getScreenWH(HomeActivity.this).widthPixels / 2 -
                            (frameTypeListAdapter.getWidth() / 2) -
                            (18 * Resources.getSystem().getDisplayMetrics().density)));
        }
        if (catListLinearLayoutManager != null) {
            catListLinearLayoutManager.setSmoothScrollbarEnabled(true);
            catListLinearLayoutManager.scrollToPositionWithOffset(mainPos,
                    (int) (Utils.getScreenWH(HomeActivity.this).widthPixels / 2 -
                            (homeMainCatListAdapter.getWidth() / 2) -
                            (18 * Resources.getSystem().getDisplayMetrics().density)));
        }
        if (subCatListLinearLayoutManager != null) {
            subCatListLinearLayoutManager.setSmoothScrollbarEnabled(true);
            subCatListLinearLayoutManager.scrollToPositionWithOffset(subPos,
                    (int) (Utils.getScreenWH(HomeActivity.this).widthPixels / 2 -
                            (homeSubCatListAdapter.getWidth() / 2) -
                            (18 * Resources.getSystem().getDisplayMetrics().density)));
        }
    }

    private void changeView(int i) {
        imgOneFeed.setColorFilter(ContextCompat.getColor(HomeActivity.this, i == 1 ? R.color.black
                : R.color.gray));
        imgDualFeed.setColorFilter(ContextCompat.getColor(HomeActivity.this, i == 2 ? R.color.black
                : R.color.gray));
        mainListLayoutManger.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return position == 0 ? i : 1;
            }
        });
        mainListLayoutManger.setSpanCount(i);
        listMain.setLayoutManager(mainListLayoutManger);
        homeListAdapter.notifyDataSetChanged();
    }

    public class HomeListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        public Activity activity;
        String thumbimage;
        int h;
        int TYPE_TOP_HEADER = 1, TYPE_ITEM = 3;

        public HomeListAdapter(Activity activity) {
            this.activity = activity;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
            View view;
            if (viewType == TYPE_TOP_HEADER) {
                view = LayoutInflater.from(activity).inflate(R.layout.home_header_item, viewGroup, false);
                return new HeaderViewHolder(view);
            } else {
                view = LayoutInflater.from(activity).inflate(R.layout.adapter_home_frame_list_item, viewGroup, false);
                return new PosterItemViewHolder(view);
            }
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
            if (getItemViewType(position) == TYPE_TOP_HEADER) {
                listFrameType.setAdapter(frameTypeListAdapter);
                listMainCat.setAdapter(homeMainCatListAdapter);
                listSubCategory.setAdapter(homeSubCatListAdapter);
                setHeaderDataInMiddle();
            } else {
                ((PosterItemViewHolder) holder).imgPoster.getViewTreeObserver().addOnGlobalLayoutListener(
                        new ViewTreeObserver.OnGlobalLayoutListener() {
                            @Override
                            public void onGlobalLayout() {
                                ((PosterItemViewHolder) holder).imgPoster.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                                // if (selectedFrameType != Festival) {
                                h = (((PosterItemViewHolder) holder).imgPoster.getWidth() * 1350) / 1080;
                                    /* } else {
                                        h = ((PosterItemViewHolder) holder).imgPoster.getWidth();
                                    } */
                                ((PosterItemViewHolder) holder).imgPoster.getLayoutParams().height = h;
                                (((PosterItemViewHolder) holder).imgPoster).setScaleType(ImageView.ScaleType.FIT_CENTER);
                                ((PosterItemViewHolder) holder).imgPoster.requestLayout();

                            }
                        });
                thumbimage = frameBeans.get(position - 1).thumb;
                if (thumbimage != null && thumbimage.length() > 0) {
                    Glide.with(activity.getApplicationContext())
                            .load(thumbimage)
                            .placeholder(R.drawable.placehoder)
                            .apply(new RequestOptions())
                            .into(((PosterItemViewHolder) holder).imgPoster);
                } else
                    Glide.with(activity.getApplicationContext())
                            .load(R.drawable.placehoder)
                            .apply(new RequestOptions())
                            .into(((PosterItemViewHolder) holder).imgPoster);
                ((PosterItemViewHolder) holder).imgPoster.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        clickFramePos = position - 1;
                        openNextScreen();
                    }
                });

                listMain.addOnScrollListener(new RecyclerView.OnScrollListener() {
                    @Override
                    public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                        super.onScrollStateChanged(recyclerView, newState);
                    }

                    @Override
                    public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                        super.onScrolled(recyclerView, dx, dy);

                        if (requestCheck) {
                            if (mainListLayoutManger.findLastCompletelyVisibleItemPosition() >= (frameBeans.size() * 0.50)) {
                                if (!isLoadingData && frameBeans.size() > 0) {
                                    isLoadingData = true;
                                    current_page_number++;
                                    getFrames();
                                }
                            }
                        }
                    }
                });
            }
        }

        @Override
        public int getItemViewType(int position) {
            if (position == 0) {
                return TYPE_TOP_HEADER;
            } else {
                return TYPE_ITEM;
            }
        }

        @Override
        public int getItemCount() {
            return frameBeans.size() + 1;
        }
    }

    public class HeaderViewHolder extends RecyclerView.ViewHolder {
        HeaderViewHolder(View itemView) {
            super(itemView);
            listFrameType = itemView.findViewById(R.id.listFrameType);
            listFrameType.setLayoutManager(frameTypeLinearLayoutManager);

            listMainCat = itemView.findViewById(R.id.listMainCat);
            listMainCat.setLayoutManager(catListLinearLayoutManager);

            listSubCategory = itemView.findViewById(R.id.listSubCategory);
            listSubCategory.setLayoutManager(subCatListLinearLayoutManager);

            txtDataNotAvailable = itemView.findViewById(R.id.txtDataNotAvailable);
        }
    }

    public class PosterItemViewHolder extends RecyclerView.ViewHolder {
        ImageView imgPoster;

        PosterItemViewHolder(View itemView) {
            super(itemView);
            imgPoster = itemView.findViewById(R.id.img);
        }
    }

    // Parsing Data
    JSONObject object, jsonObject, jsonObject1;
    String success;
    CategoryBean categoryBean;
    JSONArray jsonArray, array;
    SubCatBean subCatBean;
    ListBean mainFrameBean;
    List<String> list;


    @OnClick({R.id.llOneFeed, R.id.llDualFeed,R.id.imgHomeBack})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.llOneFeed:
                changeView(1);
                break;
            case R.id.llDualFeed:
                changeView(2);
                break;
            case R.id.imgHomeBack:
                finish();
                break;
        }
    }

    private void downloadImage(ArrayList<String> images, String destPath) {
        if (!CommanUtilsApp.isNetworkAvailable(HomeActivity.this))
            return;
        CommanUtilsApp commanUtilsApp = new CommanUtilsApp(HomeActivity.this);
        commanUtilsApp.createProgressDialog(getResources().getString(R.string.loading));
        commanUtilsApp.showProgressDialog();
        subCatId = categoryBeans.get(categoryBeans.size() > mainPos ? mainPos : 0).subCatBeans.size() > 0
                ? categoryBeans.get(categoryBeans.size() > mainPos ? mainPos : 0).subCatBeans.get(subCatBeans.size() > subPos ? subPos : 0).id : "0";

        OkHttpClient client = new OkHttpClient.Builder().build();
        DownloadManager manager = new DownloadManager.Builder().context(this)
                .downloader(OkHttpDownloader.create(client))
                .threadPoolSize(3)
                .logger(new Logger() {
                    @Override
                    public void log(String message) {
                    }
                })
                .build();
        DownloadRequest request =
                new DownloadRequest.Builder()
                        .url(downloadImageUrl)
                        .retryTime(5)
                        .retryInterval(2, TimeUnit.SECONDS)
                        .progressInterval(1, TimeUnit.SECONDS)
                        .priority(Priority.HIGH)
                        .destinationFilePath(destPath)
                        .downloadCallback(new DownloadCallback() {
                            @Override
                            public void onStart(int downloadId, long totalBytes) {
                            }

                            @Override
                            public void onRetry(int downloadId) {
                            }

                            @Override
                            public void onProgress(int downloadId, long bytesWritten, long totalBytes) {
                            }

                            @Override
                            public void onSuccess(int downloadId, String filePath) {
                                commanUtilsApp.hideProgressDialog();
                                openNextScreen();
                            }

                            @Override
                            public void onFailure(int downloadId, int statusCode, String errMsg) {
                                commanUtilsApp.hideProgressDialog();
                            }
                        })
                        .build();
        manager.add(request);
    }

    private void openNextScreen() {
        startActivityForResult(new Intent(HomeActivity.this, FrameEditorNewDesign.class)
                .putExtra(KEY_BEAN, frameBeans.get(clickFramePos))
                .putExtra(KEY_IMAGES, new ArrayList<>())
                .putExtra(KEY_TYPE, catId)
                .putExtra(KEY_SUB_CAT_ID, subCatId)
                .putExtra(KEY_TUTORIAL_LIST, (Serializable) new ArrayList<>())
                .putExtra(KEY_MASK_IMAGE, frameBeans.get(clickFramePos).mask_image)
                .putExtra(KEY_MAIN_CAT_ID, frameBeans.get(clickFramePos).maincategory_id)
                .putExtra("image", instaImage), 30);
    }


    void getFontList() {
        hashMap = new HashMap<>();
        hashMap.put(UNIQUE_KEY, UNIQUE_KEY_VALUE);
        if (CommanUtilsApp.isNetworkAvailable(HomeActivity.this))
            apiCall.GetData(Request.Method.POST, GET_FONT_LIST, hashMap, font_api_request);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (Permission.onlyCheckPermision(HomeActivity.this))
            if (CommanUtilsApp.isNetworkAvailable(HomeActivity.this)) {
                if (frameBeans.size() > clickFramePos) {
                    openNextScreen();
                }
            }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        setResult(30,intent);
        finish();
    }
}
