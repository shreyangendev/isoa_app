package com.example.iosa;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Layout;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.iosa.customTextSticker.Font;
import com.example.iosa.texteditor.TextStickerViewNew1;
import com.example.iosa.utils.CommanUtilsApp;

import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;


public class BaseActivity extends AppCompatActivity {
    CommanUtilsApp commanUtilsApp;
    File file = null;
    Bitmap generatedBitmap;
    FileOutputStream output;
    Intent shareIntent;
    int height, width;
    Bitmap bitmap = null;
    Canvas canvas;
    StringBuilder ret;

  public   List<String> fonts;
   public String[] colors;

    @Override
    protected void attachBaseContext(Context newBase) {
        final Configuration override = new Configuration(newBase.getResources().getConfiguration());
        override.fontScale = 1.0f;
        applyOverrideConfiguration(override);
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        commanUtilsApp = new CommanUtilsApp(BaseActivity.this);
        commanUtilsApp.createProgressDialog("Loading...");

        fonts = CommanUtilsApp.getFontList(BaseActivity.this);
        colors = getResources().getStringArray(R.array.colors_list);
    }

    class ShareImage extends AsyncTask<View, Boolean, File> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            commanUtilsApp.showProgressDialog();
        }

        @Override
        protected File doInBackground(View... integers) {
            try {
                generatedBitmap = loadBitmapFromView(integers[0]);
                file = CommanUtilsApp.getNewHidenFile();
                if (file != null) {
                    output = new FileOutputStream(file.getAbsolutePath());
                    generatedBitmap.compress(Bitmap.CompressFormat.PNG, 100, output);
                    output.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return file;
        }

        @Override
        protected void onPostExecute(File file) {
            commanUtilsApp.hideProgressDialog();
            CommanUtilsApp.enableStrictMode();
            if (!isFinishing())
                shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("image/*");
            shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
            startActivity(Intent.createChooser(shareIntent, "Share image using"));
        }
    }


    public String toCamelCase(String init) {
        ret = new StringBuilder(init.length());
        for (String word : init.split(" ")) {
            if (!word.isEmpty()) {
                ret.append(Character.toUpperCase(word.charAt(0)));
                ret.append(word.substring(1).toLowerCase());
            }
            if (!(ret.length() == init.length()))
                ret.append(" ");
        }
        return ret.toString();
    }

    public Bitmap loadBitmapFromView(View view) {
        height = view.getHeight() * 2;
        width = view.getWidth() * 2;
        view.measure(View.MeasureSpec.makeMeasureSpec(width, View.MeasureSpec.EXACTLY),
                View.MeasureSpec.makeMeasureSpec(height, View.MeasureSpec.EXACTLY));
        try {
            bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            canvas = new Canvas(bitmap);
            canvas.translate(0.0f, 0.0f);
            canvas.drawColor(-1, PorterDuff.Mode.CLEAR);
            canvas.scale(2, 2);
            view.draw(canvas);
            return bitmap;
        } catch (Exception e) {
            e.printStackTrace();
            return bitmap;
        }
    }

    public Font getFont(Context activity, String fontName) {
        Font font = new Font();
        font.setColor(Color.BLACK);
        int size = 20;
        font.setSize(size);
        Typeface tf = Typeface.createFromAsset(activity.getAssets(), "fonts/" + fontName);
        font.setTypeface(tf);
        return font;
    }

    public RelativeLayout.LayoutParams layoutParams;

    public TextStickerViewNew1 getTextSticker(Context activity, ScrollView scroll, int screenWidth, int screenHeight, String fontName) {
        TextStickerViewNew1 textSticker = new TextStickerViewNew1(activity,
                scroll, screenWidth / 2, screenHeight / 2);
        layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.addRule(10);
        textSticker.setLayoutParams(layoutParams);
        textSticker.setFont(getFont(activity, fontName));
        textSticker.setText("");
        textSticker.setLayoutX(screenWidth / 2);
        textSticker.setLayoutY(screenHeight / 2);
        textSticker.setScale(5.0f);
        textSticker.setPaddingLeft(10);
        textSticker.setPaddingRight(10);
        textSticker.setAlign(Layout.Alignment.ALIGN_CENTER);
        textSticker.setOpacity(255);
        textSticker.setUnderLine(false);
        textSticker.setStrikethrough(false);
        textSticker.setLetterSpacing(0.0f);
        textSticker.setLineSpacing(10.0f);
        textSticker.setTag(1);
        return textSticker;
    }


    public class ColorFilterGenerator {
        public ColorFilter adjustHue(float value) {
            ColorMatrix cm = new ColorMatrix();
            adjustHue(cm, value);
            return new ColorMatrixColorFilter(cm);
        }

        public void adjustHue(ColorMatrix cm, float value) {
            value = cleanValue(value, 360f) / 180f * (float) Math.PI;
            if (value == 0) {
                return;
            }
            float cosVal = (float) Math.cos(value);
            float sinVal = (float) Math.sin(value);
            float lumR = 0.213f;
            float lumG = 0.715f;
            float lumB = 0.072f;
            float[] mat = new float[]{
                    lumR + cosVal * (1 - lumR) + sinVal * (-lumR), lumG + cosVal * (-lumG) + sinVal * (-lumG), lumB + cosVal * (-lumB) + sinVal * (1 - lumB), 0, 0,
                    lumR + cosVal * (-lumR) + sinVal * (0.143f), lumG + cosVal * (1 - lumG) + sinVal * (0.140f), lumB + cosVal * (-lumB) + sinVal * (-0.283f), 0, 0,
                    lumR + cosVal * (-lumR) + sinVal * (-(1 - lumR)), lumG + cosVal * (-lumG) + sinVal * (lumG), lumB + cosVal * (1 - lumB) + sinVal * (lumB), 0, 0,
                    0f, 0f, 0f, 1f, 0f,
                    0f, 0f, 0f, 0f, 1f};
            cm.postConcat(new ColorMatrix(mat));
        }

        public float cleanValue(float p_val, float p_limit) {
            return Math.min(p_limit, Math.max(-p_limit, p_val));
        }
    }

    @Override
    protected void onDestroy() {
        if (commanUtilsApp != null)
            commanUtilsApp.hideProgressDialog();
        super.onDestroy();
        // Glide.with(getApplicationContext()).pauseRequests();
    }
}
