package com.example.iosa;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.appizona.yehiahd.fastsave.FastSave;
import com.example.iosa.Adapter.CodeBottomAdapter;
import com.example.iosa.Retrofitapi.APIInterface;
import com.example.iosa.Retrofitapi.ApiClient;
import com.example.iosa.VollyApi.NoConnectivityException;
import com.example.iosa.bean.CodeAPIResponse;
import com.example.iosa.bean.CodeResponseItem;
import com.example.iosa.utils.CommanUtilsApp;
import com.example.iosa.utils.UtilsStatusbar;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.iosa.utils.ApiConstants.APIKEY;
import static com.example.iosa.utils.ApiConstants.BASE_URL;

public class CodeActivity extends BaseActivity {

    @BindView(R.id.imgCodeBack)
    ImageView imgCodeBack;
    @BindView(R.id.rVCodeBottomlist)
    RecyclerView rVCodeBottomlist;
    CodeBottomAdapter codeBottomAdapter;
    @BindView(R.id.btnCreatedCode)
    TextView btnCreatedCode;
    @BindView(R.id.cardCreate)
    CardView cardCreate;
    @BindView(R.id.btnLiveCode)
    TextView btnLiveCode;
    @BindView(R.id.cardLive)
    CardView cardLive;
    @BindView(R.id.btnExpireCode)
    TextView btnExpireCode;
    @BindView(R.id.cardExpire)
    CardView cardExpire;
    @BindView(R.id.btnSubScription)
    TextView btnSubScription;
    @BindView(R.id.cardSubScription)
    CardView cardSubScription;
    @BindView(R.id.btnExpireSubscription)
    TextView btnExpireSubscription;
    @BindView(R.id.cardExpireSubscription)
    CardView cardExpireSubscription;
    @BindView(R.id.rVCodeToplist)
    HorizontalScrollView rVCodeToplist;
    CommanUtilsApp commanUtilsApp;
    List<CodeResponseItem> reportBeanList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new UtilsStatusbar(CodeActivity.this).setStatuBar();
        setContentView(R.layout.activity_code);
        ButterKnife.bind(this);

        commanUtilsApp = new CommanUtilsApp(this);
        commanUtilsApp.createProgressDialog(getResources().getString(R.string.loading));

        createdCodeList(1);
    }

    Call<JsonObject> call;

    private void createdCodeList(int i) {
        btnExpireCode.setTextColor(i == 3 ? Color.BLACK : Color.GRAY);
        btnCreatedCode.setTextColor(i == 1 ? Color.BLACK : Color.GRAY);
        btnLiveCode.setTextColor(i == 2 ? Color.BLACK : Color.GRAY);
        btnSubScription.setTextColor(i == 4 ? Color.BLACK : Color.GRAY);
        btnExpireSubscription.setTextColor(i == 5 ? Color.BLACK : Color.GRAY);

        cardExpire.setVisibility(i == 3 ? View.VISIBLE : View.GONE);
        cardLive.setVisibility(i == 2 ? View.VISIBLE : View.GONE);
        cardCreate.setVisibility(i == 1 ? View.VISIBLE : View.GONE);
        cardSubScription.setVisibility(i == 4 ? View.VISIBLE : View.GONE);
        cardExpireSubscription.setVisibility(i == 5 ? View.VISIBLE : View.GONE);

        commanUtilsApp.showProgressDialog();
        reportBeanList.clear();
        if (i == 1) {
            call = ApiClient.getClient(BASE_URL, getApplicationContext()).create(APIInterface.class)
                    .createReport(FastSave.getInstance().getString(APIKEY, ""));
        } else if (i == 2) {
            call = ApiClient.getClient(BASE_URL, getApplicationContext()).create(APIInterface.class)
                    .liveReport(FastSave.getInstance().getString(APIKEY, ""));
        } else if (i == 3) {
            call = ApiClient.getClient(BASE_URL, getApplicationContext()).create(APIInterface.class)
                    .expireReport(FastSave.getInstance().getString(APIKEY, ""));
        } else if (i == 4) {
            call = ApiClient.getClient(BASE_URL, getApplicationContext()).create(APIInterface.class)
                    .subscriptionReport(FastSave.getInstance().getString(APIKEY, ""));
        } else if (i == 5) {
            call = ApiClient.getClient(BASE_URL, getApplicationContext()).create(APIInterface.class)
                    .expireSubscriptionReport(FastSave.getInstance().getString(APIKEY, ""));
        }
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> objectResponse) {
                if (objectResponse.body().get("success").toString().contains("1")) {
                    Gson gson = new Gson();
                    CodeAPIResponse response = gson.fromJson(objectResponse.body().toString(), CodeAPIResponse.class);
                    reportBeanList = response.getResponse();
                    Log.d("=== responcesize",reportBeanList.size()+"" );
                } else {
                    CommanUtilsApp.showToast(CodeActivity.this, objectResponse.body().get("error").toString());
                }
                codeBottomAdapter = new CodeBottomAdapter(CodeActivity.this,reportBeanList);
                rVCodeBottomlist.setAdapter(codeBottomAdapter);
                rVCodeBottomlist.setHasFixedSize(false);
                commanUtilsApp.hideProgressDialog();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                t.printStackTrace();
                Log.d("=== errormsg", "onFailure: "+t.getMessage());
                if (t instanceof NoConnectivityException) {
                    CommanUtilsApp.showToast(CodeActivity.this, getResources().getString(R.string.check_network));
                }
                commanUtilsApp.hideProgressDialog();
            }
        });
    }

    @OnClick({R.id.btnCreatedCode, R.id.btnLiveCode, R.id.btnExpireCode, R.id.btnSubScription, R.id.btnExpireSubscription, R.id.imgCodeBack})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnCreatedCode:
                createdCodeList(1);
                break;
            case R.id.btnLiveCode:
                createdCodeList(2);
                break;
            case R.id.btnExpireCode:
                createdCodeList(3);
                break;
            case R.id.btnSubScription:
                createdCodeList(4);
                break;
            case R.id.btnExpireSubscription:
                createdCodeList(5);
                break;
            case R.id.imgCodeBack:
                finish();
                break;
        }
    }
}