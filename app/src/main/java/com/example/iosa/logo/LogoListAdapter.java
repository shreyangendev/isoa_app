package com.example.iosa.logo;

import android.app.Activity;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.iosa.R;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;


import java.util.List;

public class LogoListAdapter extends RecyclerView.Adapter<LogoListAdapter.MyViewHolder> {

    // implements ItemMoveCallback.ItemTouchHelperContract {
    // private final StartDragListener mStartDragListener;
    SelectDeleteClick selectDeleteClick;
    List<String> list;
    Activity activity;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        View rowView;
        ImageView imgDelete, img;
        CardView card;
        TextView txtMakeDefault, txtDefaultLogo;

        public MyViewHolder(View itemView) {
            super(itemView);
            rowView = itemView;
            img = itemView.findViewById(R.id.img);
            imgDelete = itemView.findViewById(R.id.imgDelete);
            card = itemView.findViewById(R.id.card);
            txtMakeDefault = itemView.findViewById(R.id.txtMakeDefault);
            txtDefaultLogo = itemView.findViewById(R.id.txtDefaultLogo);
        }
    }

    public LogoListAdapter(Activity activity, List<String> list, SelectDeleteClick selectDeleteClick) {
        // mStartDragListener = startDragListener;
        this.activity = activity;
        this.selectDeleteClick = selectDeleteClick;
        this.list = list;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.logo_list_adapter_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        holder.img.setImageURI(Uri.parse(list.get(position)));
        holder.img.setTag(list.get(position));
        holder.card.setCardBackgroundColor(list.get(position).contains("Socially_Logo/0_")
                ? activity.getResources().getColor(R.color.black)
                : activity.getResources().getColor(R.color.white));
        holder.txtDefaultLogo.setVisibility(position == 0 ? View.VISIBLE : View.GONE);
        holder.txtMakeDefault.setVisibility(position == 0 ? View.GONE : View.VISIBLE);
        holder.txtDefaultLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
        holder.imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectDeleteClick.selectDeleteClick(list.get(position), true);
            }
        });
        holder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectDeleteClick.selectDeleteClick(holder.img.getTag().toString(), false);
            }
        });
        /* holder.card.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                mStartDragListener.requestDrag(holder);
                return false;
            }
        }); */
        holder.txtMakeDefault.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // notifyItemMoved(position, 0);˛
                selectDeleteClick.moveLocation(position, 0);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    /* @Override
    public void onRowMoved(int fromPosition, int toPosition) {
        notifyItemMoved(fromPosition, toPosition);
        selectDeleteClick.moveLocation(fromPosition, toPosition);
    }

    @Override
    public void onRowSelected(MyViewHolder myViewHolder) {
    }

    @Override
    public void onRowClear(MyViewHolder myViewHolder) {
    } */

    interface SelectDeleteClick {
        void selectDeleteClick(String i, boolean isDelete);

        void moveLocation(int source, int dest);
    }
}

