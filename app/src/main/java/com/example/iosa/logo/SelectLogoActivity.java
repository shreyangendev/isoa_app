package com.example.iosa.logo;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.features.ReturnMode;
import com.example.iosa.BaseActivity;
import com.example.iosa.Coustomer.DesignLogoWithFontActivity;
import com.example.iosa.R;
import com.example.iosa.utils.CommanUtilsApp;
import com.google.gson.Gson;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.example.iosa.utils.ApiConstants.LOGO_LIST;
import static com.example.iosa.utils.ApiConstants.SelectLogoRequest;
import static com.example.iosa.utils.CommanUtilsApp.getLogoList;
import static com.example.iosa.utils.KeyParameterConst.KEY_FROM_HOME;
import static com.example.iosa.utils.KeyParameterConst.KEY_IMAGE;
import static com.example.iosa.utils.KeyParameterConst.KEY_IS_REFRESH;
import static com.example.iosa.utils.KeyParameterConst.KEY_PATH;


public class SelectLogoActivity extends BaseActivity implements LogoListAdapter.SelectDeleteClick {
    @BindView(R.id.imgBlur)
    ImageView imgBlur;
    @BindView(R.id.listLogo)
    RecyclerView listLogo;
    @BindView(R.id.rlMain)
    RelativeLayout rlMain;
    @BindView(R.id.llMain)
    LinearLayout llMain;
    @BindView(R.id.rlList)
    RelativeLayout rlList;

    LogoListAdapter mAdapter;
    // ItemTouchHelper touchHelper;
    List<String> logoList;
    boolean isLogoSet = false;
    // StartDragListener startDragListener;
    LogoListAdapter.SelectDeleteClick selectDeleteClick;
    CommanUtilsApp commanUtilsApp;
    boolean fromHome;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND,
                WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
        setContentView(R.layout.select_logo_dialog);
        ButterKnife.bind(this);

        fromHome = getIntent().getBooleanExtra(KEY_FROM_HOME, false);
        byte[] byteArray = getIntent().getByteArrayExtra(KEY_IMAGE);
        if (byteArray != null) {
            Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
            imgBlur.setImageBitmap(bmp);
        }

        selectDeleteClick = this;
        new GetLogoData().execute();
    }

    @Override
    public void moveLocation(int fromPosition, int toPosition) {
        Collections.swap(logoList, fromPosition, toPosition);
        saveLogoList(logoList);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void selectDeleteClick(String path, boolean isDelete) {
        if (isDelete) {
            new File(path).delete();
            logoList.remove(path);
            saveLogoList(logoList);
            mAdapter.notifyDataSetChanged();
        } else {
            passLogo(path);
        }
    }

    private void passLogo(String s) {
        Intent intent = new Intent();
        intent.putExtra(KEY_PATH, s);
        setResult(SelectLogoRequest, intent);
        finish();
    }

    @OnClick({R.id.llMain, R.id.cardDesignWithFont, R.id.imgBlur, R.id.imgSelecteLogo, R.id.imgFolderLogo, R.id.rlList})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.llMain:
                finish();
                break;
            case R.id.cardDesignWithFont:
                startActivityForResult(new Intent(SelectLogoActivity.this, DesignLogoWithFontActivity.class), 90);
                break;
            case R.id.rlList:
                finish();
                break;
            case R.id.imgBlur:
                finish();
                break;
            case R.id.imgFolderLogo:
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                String[] mimeTypes = {"image/jpeg", "image/png"};
                intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
                startActivityForResult(intent, 160);
                break;
            case R.id.imgSelecteLogo:
                ImagePicker.create(SelectLogoActivity.this)
                        .returnMode(ReturnMode.ALL)
                        .folderMode(true)
                        .toolbarFolderTitle("Folder")
                        .toolbarImageTitle("Tap to select")
                        .toolbarArrowColor(Color.BLACK)
                        .single()
                        .showCamera(false)
                        .enableLog(false)
                        .start();
                break;
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 160) {
            if (data != null) {
                Uri selectedImage = data.getData();
                try {
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                    cursor.moveToFirst();
                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String imgDecodableString = cursor.getString(columnIndex);
                    cursor.close();

                    saveImageInLogoFolder(new File(imgDecodableString), CommanUtilsApp.getLogoImagePath());
                } catch (Exception e) {
                   e.printStackTrace();
                }
            }
        } else if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            com.esafirm.imagepicker.model.Image image = ImagePicker.getFirstImageOrNull(data);
            saveImageInLogoFolder(new File(image.getPath()), CommanUtilsApp.getLogoImagePath());
        } else if (resultCode == 90) {
            if (data != null)
                if (data.getBooleanExtra(KEY_IS_REFRESH, false))
                    new GetLogoData().execute();
        }
    }

    public void saveLogoList(List<String> callLog) {
        SharedPreferences mPrefs = getApplicationContext().getSharedPreferences(LOGO_LIST, getApplicationContext().MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(callLog);
        prefsEditor.putString("myJson", json);
        prefsEditor.commit();
        rlList.setVisibility(logoList.size() > 0 ? View.VISIBLE : View.GONE);
    }

    public void saveImageInLogoFolder(File sourceLocation, File targetLocation) {
        try {
            InputStream in = new FileInputStream(sourceLocation);
            OutputStream out = new FileOutputStream(targetLocation);
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            in.close();
            out.close();
            logoList.add(targetLocation.getPath());
            saveLogoList(logoList);
            mAdapter.notifyDataSetChanged();
            if (isLogoSet && !fromHome) {
                passLogo(logoList.get(0));
            }
            if (fromHome){
                Intent intent = new Intent();
                setResult(SelectLogoRequest, intent);
                finish();
            }
        } catch (Exception e) {
           e.printStackTrace();
        }
    }

    private class GetLogoData extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            /*commanUtilsApp.createProgressDialog(getResources().getString(R.string.loading));
            commanUtilsApp.showProgressDialog();*/
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            rlList.setVisibility(logoList.size() > 0 ? View.VISIBLE : View.GONE);

            mAdapter = new LogoListAdapter(SelectLogoActivity.this, logoList, selectDeleteClick);
            listLogo.setAdapter(mAdapter);

            /* ItemTouchHelper.Callback callback = new ItemMoveCallback(mAdapter);
            touchHelper = new ItemTouchHelper(callback);
            touchHelper.attachToRecyclerView(listLogo); */
            llMain.setVisibility(View.VISIBLE);
            // commanUtilsApp.hideProgressDialog();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            logoList = getLogoList(SelectLogoActivity.this);
            isLogoSet = logoList.size() == 0;
            return null;
        }
    }
}
