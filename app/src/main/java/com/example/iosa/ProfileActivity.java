package com.example.iosa;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.appizona.yehiahd.fastsave.FastSave;
import com.coolerfall.download.DownloadCallback;
import com.coolerfall.download.DownloadManager;
import com.coolerfall.download.DownloadRequest;
import com.coolerfall.download.Logger;
import com.coolerfall.download.OkHttpDownloader;
import com.coolerfall.download.Priority;
import com.example.iosa.Adapter.AchivementScanAdapter;
import com.example.iosa.Retrofitapi.APIInterface;
import com.example.iosa.Retrofitapi.ApiClient;
import com.example.iosa.VollyApi.NoConnectivityException;
import com.example.iosa.bean.DateWiseResponse;
import com.example.iosa.bean.DateWiseResponseItem;
import com.example.iosa.bean.GetPackageApiResponse;
import com.example.iosa.bean.LoginApiResponse;
import com.example.iosa.bean.NewCustmerResponse;
import com.example.iosa.bean.PackageResponseItem;
import com.example.iosa.utils.CommanUtilsApp;
import com.example.iosa.utils.Permission;
import com.example.iosa.utils.Source;
import com.example.iosa.utils.UtilsStatusbar;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;

import static com.example.iosa.utils.ApiConstants.APIKEY;
import static com.example.iosa.utils.ApiConstants.BASE_URL;
import static com.example.iosa.utils.ApiConstants.BASE_URL_API_2;
import static com.example.iosa.utils.ApiConstants.LOGIN_ID;
import static com.example.iosa.utils.ApiConstants.NAME;
import static com.example.iosa.utils.ApiConstants.PASSWORD;
import static com.example.iosa.utils.ApiConstants.PRICE;
import static com.example.iosa.utils.ApiConstants.PROMO;
import static com.example.iosa.utils.ApiConstants.SUBSCRIPTION;

public class ProfileActivity extends BaseActivity {

    @BindView(R.id.imgMenu)
    ImageView imgMenu;
    @BindView(R.id.imgAppName)
    ImageView imgAppName;
    @BindView(R.id.imgMessage)
    ImageView imgMessage;
    @BindView(R.id.imgAttachment)
    ImageView imgAttachment;
    @BindView(R.id.toolbarLayout)
    RelativeLayout toolbarLayout;
    @BindView(R.id.editSearch)
    EditText editSearch;
    @BindView(R.id.cardPayable)
    CardView cardPayable;
    @BindView(R.id.mainLayout)
    LinearLayout mainLayout;
    @BindView(R.id.edtProductLin)
    EditText edtProductLin;
    @BindView(R.id.edtProfileLink)
    EditText edtProfileLink;
    @BindView(R.id.edtProfileNumber)
    EditText edtProfileNumber;
    @BindView(R.id.txtPackageId)
    TextView txtPackageId;
    @BindView(R.id.cardPackage)
    CardView cardPackage;
    @BindView(R.id.cardNext)
    CardView cardNext;
    @BindView(R.id.txtTopName)
    TextView txtTopName;
    @BindView(R.id.imgAchievement)
    ImageView imgAchievement;
    @BindView(R.id.cardSecond)
    CardView cardSecond;
    @BindView(R.id.cardTop)
    CardView cardTop;
    @BindView(R.id.cardCode)
    CardView cardCode;
    @BindView(R.id.CardSubScription)
    CardView CardSubScription;
    String url;
    RequestQueue queue;

    @BindView(R.id.imgProfile)
    CircularImageView imgProfile;
    @BindView(R.id.txtSubScription)
    TextView txtSubScription;
    @BindView(R.id.txtCode)
    TextView txtCode;
    @BindView(R.id.txtPaybleAmount)
    TextView txtPaybleAmount;
    @BindView(R.id.numberBlock)
    ImageView numberBlock;
    String packageId = "";
    CommanUtilsApp commanUtilsApp;
    List<PackageResponseItem> mainScanList = new ArrayList<>();
    @BindView(R.id.rVAchivementScanlist)
    RecyclerView rVAchivementScanlist;
    AchivementScanAdapter achivementScanAdapter;
    List<DateWiseResponseItem> dateWiseResponseItemList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new UtilsStatusbar(ProfileActivity.this).setStatuBar();
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);

        commanUtilsApp = new CommanUtilsApp(this);
        commanUtilsApp.createProgressDialog(getResources().getString(R.string.loading));

        if (getIntent().getBooleanExtra("isApiCall", false)) {
            setLoginData();
        } else {
            getLogInData();
        }
        getDateWiseQr();
        getPackageList();
    }

    private void getDateWiseQr() {
        commanUtilsApp.showProgressDialog();
        Call<DateWiseResponse> call = ApiClient.getClient(BASE_URL, getApplicationContext()).create(APIInterface.class)
                .getDateWiseList(FastSave.getInstance().getString(APIKEY, ""));
        call.enqueue(new Callback<DateWiseResponse>() {
            @Override
            public void onResponse(Call<DateWiseResponse> call, retrofit2.Response<DateWiseResponse> response) {
                if (response.body().getSuccess().equalsIgnoreCase("1")) {
                    dateWiseResponseItemList = response.body().getResponse();
                    achivementScanAdapter = new AchivementScanAdapter(ProfileActivity.this, dateWiseResponseItemList);
                    rVAchivementScanlist.setAdapter(achivementScanAdapter);
                    rVAchivementScanlist.setHasFixedSize(false);
                } else {
                    CommanUtilsApp.showToast(ProfileActivity.this, "something went wrong");
                }
                commanUtilsApp.hideProgressDialog();
            }

            @Override
            public void onFailure(Call<DateWiseResponse> call, Throwable t) {
                t.printStackTrace();
                if (t instanceof NoConnectivityException) {
                    CommanUtilsApp.showToast(ProfileActivity.this, getResources().getString(R.string.check_network));
                }
                commanUtilsApp.showProgressDialog();
            }
        });
    }

    private void getPackageList() {
        commanUtilsApp.showProgressDialog();
        Call<GetPackageApiResponse> call = ApiClient.getClient(BASE_URL_API_2, getApplicationContext()).create(APIInterface.class)
                .getPackageList(FastSave.getInstance().getString(APIKEY, ""));
        call.enqueue(new Callback<GetPackageApiResponse>() {
            @Override
            public void onResponse(Call<GetPackageApiResponse> call, retrofit2.Response<GetPackageApiResponse> response) {
                if (response.body().getSuccess().equalsIgnoreCase("1")) {
                    mainScanList = response.body().getResponse();
                    if (mainScanList.size() > 0) {
                        packageId = mainScanList.get(0).getId();
                        txtPackageId.setText(mainScanList.get(0).getName());

                    }
                    //setAdapter();
                } else {
                    CommanUtilsApp.showToast(ProfileActivity.this, "Something Went Wrong");

                }
                commanUtilsApp.hideProgressDialog();
            }

            @Override
            public void onFailure(Call<GetPackageApiResponse> call, Throwable t) {
                t.printStackTrace();
                if (t instanceof NoConnectivityException) {
                    CommanUtilsApp.showToast(ProfileActivity.this, getResources().getString(R.string.check_network));
                }
                commanUtilsApp.showProgressDialog();
            }
        });
    }

    private void getLogInData() {
        Call<JsonObject> call = ApiClient.getClient(BASE_URL, getApplicationContext()).create(APIInterface.class)
                .getLogin(FastSave.getInstance().getString(LOGIN_ID, ""),
                        FastSave.getInstance().getString(PASSWORD, ""),
                        Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID) + "",
                        "A");
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> objectResponse) {
                if (objectResponse.body().get("success").toString().contains("1")) {
                    Gson gson = new Gson();
                    LoginApiResponse response = gson.fromJson(objectResponse.body().toString(), LoginApiResponse.class);

                    if (response.getSuccess().equalsIgnoreCase("1")) {
                        FastSave.getInstance().saveString(APIKEY, response.getResponse().getApikey());
                        FastSave.getInstance().saveString(NAME, response.getResponse().getName());
                        FastSave.getInstance().saveString(SUBSCRIPTION, response.getResponse().getTotalSubScription());
                        FastSave.getInstance().saveString(PROMO, response.getResponse().getTotalPromo());
                        FastSave.getInstance().saveString(PRICE, response.getResponse().getTotalPrice());

                        setLoginData();
                    }
                } else {
                    CommanUtilsApp.showToast(ProfileActivity.this, objectResponse.body().get("error").toString());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                t.printStackTrace();
                if (t instanceof NoConnectivityException) {
                    CommanUtilsApp.showToast(ProfileActivity.this, getResources().getString(R.string.check_network));
                }
            }
        });
    }


    private void setLoginData() {
        txtSubScription.setText(FastSave.getInstance().getString(SUBSCRIPTION, ""));
        txtCode.setText(FastSave.getInstance().getString(PROMO, ""));
        txtPaybleAmount.setText(FastSave.getInstance().getString(PRICE, ""));
    }


    @OnClick({R.id.cardCode, R.id.cardNext, R.id.CardSubScription, R.id.imgMenu, R.id.imgAchievement, R.id.editSearch,R.id.imgMessage})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cardCode:
                startActivity(new Intent(ProfileActivity.this, CodeActivity.class));
                break;
            case R.id.cardNext:
               /* startActivity(new Intent(ProfileActivity.this, StepActivity.class)
                        .putExtra("number", edtProfileNumber.getText().toString())
                        .putExtra("barcode", "JDFGKJ")
                        .putExtra("number", edtProfileNumber.getText().toString())
                        .putExtra("image", "https://storage.googleapis.com/sociallyapp/frames/Socially-114-92949911599722905.png")
                        .putExtra("promo", "HJDXKJHGKJS"));*/
               if (Permission.checkPermission(ProfileActivity.this))
                SetValidation();
                break;
            case R.id.CardSubScription:
                startActivity(new Intent(ProfileActivity.this, SubScriptionActivity.class));
                break;
            case R.id.imgMenu:
                FastSave.getInstance().saveString(APIKEY, "");
                startActivity(new Intent(ProfileActivity.this, LoginActivity.class));
                finish();
                break;
            case R.id.imgAchievement:
                startActivity(new Intent(ProfileActivity.this, ShowPromoCodeActivity.class));
                break;
            case R.id.editSearch:
                startActivity(new Intent(ProfileActivity.this, SearchActivity.class));
                break;
            case R.id.imgMessage:
                startActivity(new Intent(ProfileActivity.this, LanguageActivity.class));
                break;
        }
    }

    private void SetValidation() {
        if (edtProductLin.length() == 0) {
            edtProductLin.requestFocus();
            edtProductLin.setError(edtProductLin.getHint().toString());
        } else if (edtProfileLink.length() == 0) {
            edtProfileLink.requestFocus();
            edtProfileLink.setError(edtProfileLink.getHint().toString());
        } else if (!(edtProfileNumber.length() > 9)) {
            edtProfileNumber.requestFocus();
            edtProfileNumber.setError("Enter number with at least 10 digit");
        } else {
            String obj = edtProductLin.getText().toString();
            if (obj.contains("instagram.com")) {
                if (!obj.contains("http")) {
                    url = "http://" + obj;
                } else {
                    url = obj;
                }
                url = Source.getURL(url, "(http(s)?:\\/\\/(.+?\\.)?[^\\s\\.]+\\.[^\\s\\/]{1,9}(\\/[^\\s]+)?)");
                viewPageSource(url);
            }
        }
    }

    private void getNewCustomer(String imageUrl) {
        commanUtilsApp.showProgressDialog();
        Call<JsonObject> call = ApiClient.getClient(BASE_URL, getApplicationContext()).create(APIInterface.class)
                .addCustomer(FastSave.getInstance().getString(APIKEY, ""),
                        edtProductLin.getText().toString(),
                        edtProfileLink.getText().toString(),
                        edtProfileNumber.getText().toString(),
                        packageId);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> objectResponse) {
                if (objectResponse.body() != null)
                    if (objectResponse.body().get("success").toString().contains("1")) {
                        Gson gson = new Gson();
                        NewCustmerResponse response = gson.fromJson(objectResponse.body().toString(), NewCustmerResponse.class);
                        if (response.getSuccess().equalsIgnoreCase("1")) {
                     /*       edtProductLin.setText("");
                            edtProfileLink.setText("");
                            edtProfileNumber.setText("");*/
                            downloadImage(imageUrl,response.getResponse().getBarcode(),response.getResponse().getPromo());
                        }
                    } else {
                        CommanUtilsApp.showToast(ProfileActivity.this, objectResponse.body().get("error").toString());
                    }
                else {
                    CommanUtilsApp.showToast(ProfileActivity.this, "Something Went Wrong");
                }
                commanUtilsApp.hideProgressDialog();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                t.printStackTrace();
                if (t instanceof NoConnectivityException) {
                    CommanUtilsApp.showToast(ProfileActivity.this, getResources().getString(R.string.check_network));
                } else {
                    CommanUtilsApp.showToast(ProfileActivity.this, t.getMessage());
                }
                commanUtilsApp.hideProgressDialog();
            }
        });
    }

    private void downloadImage(String imgUrl,String barCode, String promoCode) {
        if (!CommanUtilsApp.isNetworkAvailable(ProfileActivity.this))
            return;
        CommanUtilsApp commanUtilsApp = new CommanUtilsApp(ProfileActivity.this);
        commanUtilsApp.createProgressDialog(getResources().getString(R.string.loading));
        commanUtilsApp.showProgressDialog();

        OkHttpClient client = new OkHttpClient.Builder().build();
        DownloadManager manager = new DownloadManager.Builder().context(this)
                .downloader(OkHttpDownloader.create(client))
                .threadPoolSize(3)
                .logger(new Logger() {
                    @Override
                    public void log(String message) {
                    }
                })
                .build();


        Log.d("=== imgUrl ", imgUrl+" ");
        DownloadRequest request =
                new DownloadRequest.Builder()
                        .url(imgUrl)
                        .retryTime(5)
                        .retryInterval(2, TimeUnit.SECONDS)
                        .progressInterval(1, TimeUnit.SECONDS)
                        .priority(Priority.HIGH)
                        .destinationFilePath(CommanUtilsApp.getNewHidenFile().getAbsolutePath())
                        .downloadCallback(new DownloadCallback() {
                            @Override
                            public void onStart(int downloadId, long totalBytes) {
                            }

                            @Override
                            public void onRetry(int downloadId) {
                            }

                            @Override
                            public void onProgress(int downloadId, long bytesWritten, long totalBytes) {
                            }

                            @Override
                            public void onSuccess(int downloadId, String filePath) {
                                commanUtilsApp.hideProgressDialog();

                                startActivity(new Intent(ProfileActivity.this, StepActivity.class)
                                        .putExtra("number", edtProfileNumber.getText().toString())
                                        .putExtra("barcode",barCode )
                                        .putExtra("number", edtProfileNumber.getText().toString())
                                        .putExtra("image", filePath)
                                        .putExtra("promo", promoCode));


                                edtProductLin.setText("");
                                edtProfileLink.setText("");
                                edtProfileNumber.setText("");
                            }

                            @Override
                            public void onFailure(int downloadId, int statusCode, String errMsg) {
                                commanUtilsApp.hideProgressDialog();
                            }
                        })
                        .build();
        manager.add(request);
    }

    public void viewPageSource(final String url) {
        queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    String imageUrl = Source.getURL(response.toString(), "property=\"og:image\" content=\"([^\"]+)\"");
                    //imageList = Source.getArrayURL(htmlSource);
                    if (imageUrl.length() > 0)
                        getNewCustomer(imageUrl);
                    else
                        Toast.makeText(ProfileActivity.this, "Can't get Product Image", Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(ProfileActivity.this, "Failed to Fetch Data", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(ProfileActivity.this, "Please Check If Your Url is Correct!", Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(stringRequest);
    }

}