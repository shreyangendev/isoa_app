package com.example.iosa.Coustomer;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.RelativeLayout;

public class MagnifierView extends View {
    private float locationX = 0, locationY = 0;
    private float downX = 0, downY = 0;
    private Bitmap bm;
    private Activity activity;
    private ViewTreeObserver viewTreeObserver = null;
    private BitmapShader bitmapShader;
    private ViewGroup rootVg;
    private int initLeft, initTop;
    private int viewW, viewH;
    private float scaleX, scaleY;
    private int magnifierColor;
    private int magnifierAlpha;
    private float magnifierLen;
    public static RelativeLayout rlMainView;

    public MagnifierView(Builder builder, Context context) {
        super(context);
        activity = (Activity) context;
        this.rootVg = builder.rootVg;
        if (rootVg == null)
            rootVg = (ViewGroup) (activity.findViewById(android.R.id.content));
        this.viewH = builder.viewH;
        this.viewW = builder.viewW;
        this.scaleX = builder.scaleX;
        this.scaleY = builder.scaleY;
        this.magnifierColor = builder.magnifierColor;
        this.magnifierAlpha = builder.magnifierAlpha;
        this.initLeft = builder.initLeft;
        this.initTop = builder.initTop;

        ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(viewW + 30, viewH + 30);
        this.setLayoutParams(lp);

        magnifierLen = viewH > viewW ? viewW : viewH;
    }

    public MagnifierView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MagnifierView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void startViewToRoot() {
        if (activity != null && rootVg != null && this.getParent() == null) {
            rootVg.addView(this);
            viewTreeObserver = rootVg.getViewTreeObserver();
            viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    if (bm == null) {
                        MagnifierView.this.setX(initLeft);
                        MagnifierView.this.setY(initTop);
                        bm = getScreenBm(rootVg);
                        bitmapShader = new BitmapShader(bm, Shader.TileMode.MIRROR, Shader.TileMode.MIRROR);
                        invalidate();
                    }
                }
            });
        }
    }

    public void closeViewToRoot() {
        if (rootVg != null && this.getParent() != null) {
            rootVg.removeView(this);
        }
    }

    public void resetXY() {
        this.setX(initLeft);
        this.setY(initTop);
        invalidate();
    }

    public static class Builder {
        private Context context;
        private int initLeft = 0, initTop = 0;
        private int viewW = 100, viewH = 100;
        private float scaleX = 1.5f, scaleY = 1.5f;
        private int magnifierColor = 16777215;
        private int magnifierAlpha = 100;

        private ViewGroup rootVg;

        public Builder(Context context, RelativeLayout relativeLayout) {
            this.context = context;
            rlMainView = relativeLayout;
        }

        public Builder intiLT(int initLeft, int initTop) {
            if (initLeft > 0)
                this.initLeft = initLeft;
            if (initTop > 0)
                this.initTop = initTop;
            return this;
        }

        public Builder viewWH(int viewW, int viewH) {
            this.viewW = viewW;
            this.viewH = viewH;
            return this;
        }

        public Builder rootVg(ViewGroup rootVg) {
            this.rootVg = rootVg;
            return this;
        }

        public Builder scale(float scale) {
            this.scaleX = scale;
            this.scaleY = scale;
            return this;
        }

        public Builder color(int color) {
            this.magnifierColor = color;
            return this;
        }

        public Builder alpha(int alpha) {
            if (alpha >= 200) {
                this.magnifierAlpha = 200;
            } else if (alpha < 0) {
                this.magnifierAlpha = 0;
            } else {
                this.magnifierAlpha = alpha;
            }
            return this;
        }

        public MagnifierView build() {
            return new MagnifierView(this, context);
        }
    }

    Bitmap imgbmp;

    public void setMagnifierColor(int color) {
        this.magnifierColor = color;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                locationX = getX();
                locationY = getY();
                downX = event.getRawX();
                downY = event.getRawY();
                break;
            case MotionEvent.ACTION_MOVE:
                setX(locationX + (event.getRawX() - downX));
                setY(locationY + (event.getRawY() - downY));
                /* int pxl = bm.getPixel((int) locationX, (int) locationY);
                setMagnifierColor(pxl); */
                // listner.onTouchList(event.getX(), event.getY());
                invalidate();
                break;
            case MotionEvent.ACTION_UP:
                listner.onTouchList(pxl);
                /* try {
                    int pxl = imgbmp.getPixel((int) getX() + viewW / 2, (int) getY() + viewH / 2);
                    listner.onTouchList(pxl);
                } catch (Exception ignore) {
                    Log.d("===== ignore ", ignore.getMessage());
                }*/
                closeViewToRoot();
                break;
        }
        return true;
    }

    OnTouchList listner;

    public void setListner(OnTouchList listner) {
        this.listner = listner;
    }

    public interface OnTouchList {
        void onTouchList(float color);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {//一般viewgroup中用于确定子view的位置
        super.onLayout(changed, left, top, right, bottom);
    }

    int pxl;

    @Override
    protected void onDraw(Canvas canvas) {
        if (bm != null) {
            Paint paintBg = new Paint();
            paintBg.setAntiAlias(true);
            paintBg.setColor(Color.parseColor("#ffffff"));
            canvas.drawCircle(magnifierLen / 2 + 15f, magnifierLen / 2 + 15f, magnifierLen / 2, paintBg);

            Paint paint = new Paint();
            paint.setAntiAlias(true);
            paint.setShader(bitmapShader);// bitmapShader

            Matrix matrix = new Matrix();
            matrix.setScale(scaleX, scaleY);
            matrix.postTranslate(-(scaleX * getX() + (scaleX - 1) * magnifierLen / 2)
                    , -(scaleY * getY() + (scaleY - 1) * magnifierLen / 2));
            bitmapShader.setLocalMatrix(matrix);
            canvas.drawCircle(magnifierLen / 2 + 15f, magnifierLen / 2 + 15f, magnifierLen / 2, paint);

            Paint paintShade = new Paint();
            paintShade.setAntiAlias(true);
            paintShade.setColor(magnifierColor);
            paintShade.setAlpha(magnifierAlpha);
            canvas.drawCircle(magnifierLen / 2 + 15f, magnifierLen / 2 + 15f, magnifierLen / 2, paintShade);

            Paint paints = new Paint();
            paints.setStyle(Paint.Style.STROKE);
            paints.setStrokeWidth(30); // set stroke width

            if (imgbmp == null) {
                rlMainView.setDrawingCacheEnabled(true);
                imgbmp = Bitmap.createBitmap(rlMainView.getDrawingCache());
                rlMainView.setDrawingCacheEnabled(false);
            }

            Point centerOfCanvas1 = new Point(canvas.getWidth() / 2, canvas.getHeight() / 2);
            int left1 = centerOfCanvas1.x - (33 / 2);
            int top1 = centerOfCanvas1.y - (33 / 2);
            int right1 = centerOfCanvas1.x + (33 / 2);
            int bottom1 = centerOfCanvas1.y + (33 / 2);
            Rect rectBorder = new Rect(left1, top1, right1, bottom1);

            Point centerOfCanvas = new Point(canvas.getWidth() / 2, canvas.getHeight() / 2);
            int rectW = 30;
            int rectH = 30;
            int left = centerOfCanvas.x - (rectW / 2);
            int top = centerOfCanvas.y - (rectH / 2);
            int right = centerOfCanvas.x + (rectW / 2);
            int bottom = centerOfCanvas.y + (rectH / 2);
            Rect rect = new Rect(left, top, right, bottom);

            try {
                pxl = imgbmp.getPixel((int) ((getX() + magnifierLen / 2) + scaleX / 2),
                        (int) ((getY() + magnifierLen / 2) + scaleY / 2));

                paints.setColor(pxl);
                canvas.drawCircle(magnifierLen / 2 + 15f, magnifierLen / 2 + 15f, magnifierLen / 2 + 0.2f, paints);

                Paint paint2 = new Paint();
                paint2.setStyle(Paint.Style.STROKE);
                paint2.setStrokeWidth(2);
                paint2.setAntiAlias(true);
                paint2.setColor(getResources().getColor(android.R.color.black));
                canvas.drawRect(rectBorder, paint2);

                Paint paint1 = new Paint();
                paint1.setAntiAlias(true);
                paint1.setColor(pxl);
                canvas.drawRect(rect, paint1);
                // listner.onTouchList(pxl);
            } catch (Exception ignore) {
            }
            // listner.onTouchList(pxl, 0);
        }
    }

    private Bitmap getScreenBm(View contentView) {
        Bitmap bm;
        contentView.setDrawingCacheEnabled(true);
        contentView.buildDrawingCache();
        bm = contentView.getDrawingCache();
        // contentView.setDrawingCacheEnabled(false);
        // contentView.destroyDrawingCache();// 释放缓存
        return bm;
    }
}
