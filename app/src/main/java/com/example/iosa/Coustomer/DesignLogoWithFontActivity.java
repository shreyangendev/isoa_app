package com.example.iosa.Coustomer;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.example.iosa.Adapter.DesignWithFontListAdapter;
import com.example.iosa.BaseActivity;
import com.example.iosa.R;
import com.example.iosa.utils.CommanUtilsApp;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.example.iosa.utils.KeyParameterConst.KEY_IS_REFRESH;


public class DesignLogoWithFontActivity extends BaseActivity {
    @BindView(R.id.edtBusinessName)
    EditText edtBusinessName;
    @BindView(R.id.listFontLogo)
    RecyclerView listFontLogo;
    DesignWithFontListAdapter designWithFontListAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND,
                WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
        setContentView(R.layout.activity_design_with_font);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.llBack, R.id.cardSubmit})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.llBack:
                finish();
                break;
            case R.id.cardSubmit:
                if (!edtBusinessName.getText().toString().isEmpty()) {
                    CommanUtilsApp.hideKeyboard(DesignLogoWithFontActivity.this, edtBusinessName);
                    designWithFontListAdapter = new DesignWithFontListAdapter(DesignLogoWithFontActivity.this,fonts,
                            edtBusinessName.getText().toString(), new DesignWithFontListAdapter.Done() {
                        @Override
                        public void completeLogoGenerate() {
                            Intent intent = new Intent();
                            intent.putExtra(KEY_IS_REFRESH, true);
                            setResult(90, intent);
                            finish();
                        }
                    });
                    listFontLogo.setAdapter(designWithFontListAdapter);
                } else
                    edtBusinessName.setError(getResources().getString(R.string.enter_businessName));
                break;
        }
    }

}
