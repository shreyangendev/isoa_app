package com.example.iosa.VollyApi;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.iosa.R;
import com.example.iosa.VollyApi.VolleyTaskCompleteListener;
import com.example.iosa.utils.CommanUtilsApp;


import java.util.Map;


public class ApiCall {
    private VolleyTaskCompleteListener listener;
    Context context;
    CommanUtilsApp commanUtilsApp;

    public ApiCall(Context context, VolleyTaskCompleteListener completeListener) {
        listener = completeListener;
        this.context = context;
        commanUtilsApp = new CommanUtilsApp(context);
        commanUtilsApp.createProgressDialog(context.getResources().getString(R.string.loading));
    }

    public void GetData(int post, String Url, final Map<String, String> params, final int service_code) {
        Log.e("--------- Url :- ", Url + "");
        Log.e("--------- params :- ", params.toString() + "");
        commanUtilsApp.showProgressDialog();
        final RequestQueue requestQueue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(post, Url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("-------- response : ", response);
                listener.onTaskCompleted(service_code, response);
                 commanUtilsApp.hideProgressDialog();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                commanUtilsApp.hideProgressDialog();
                Log.e("------ Volly Error : ", volleyError.toString());

                String message = "";
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                Log.e("------ Volly message : ", message);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                return params;
            }
        };
        stringRequest.setRetryPolicy(new RetryPolicy() {
                                         @Override
                                         public int getCurrentTimeout() {
                                             return 50000;
                                         }

                                         @Override
                                         public int getCurrentRetryCount() {
                                             return 5;
                                         }

                                         @Override
                                         public void retry(VolleyError error) throws VolleyError {
                                             Log.e("-----VollyRetryPolicy: ", error.getMessage());
                                         }
                                     }
        );
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);
    }

    public void GetDataPagination(int post, String Url, final Map<String, String> params, final int service_code) {
       Log.e("--------- Url :- ", Url + "");
       Log.e("--------- params :- ", params.toString() + "");
        final RequestQueue requestQueue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(post, Url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("-------- response : ", response);
                listener.onTaskCompleted(service_code, response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("------ Volly Error : ", volleyError.toString());

                String message = "";
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                Log.e("------ Volly message : ", message);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                return params;
            }
        };
        stringRequest.setRetryPolicy(new RetryPolicy() {
                                         @Override
                                         public int getCurrentTimeout() {
                                             return 50000;
                                         }

                                         @Override
                                         public int getCurrentRetryCount() {
                                             return 5;
                                         }

                                         @Override
                                         public void retry(VolleyError error) throws VolleyError {
                                             Log.e("-----VollyRetryPolicy: ", error.getMessage());
                                         }
                                     }
        );
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);
    }
}
