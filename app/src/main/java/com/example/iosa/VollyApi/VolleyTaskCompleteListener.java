package com.example.iosa.VollyApi;

public interface VolleyTaskCompleteListener {
    public void onTaskCompleted(int service_code, String response);
}
